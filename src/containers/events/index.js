import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { init, getData } from './actions';
// components
import InfiniteScroll from 'react-infinite-scroll-component';
import SpecialEvent from 'components/specialEvent';

class EventsContainer extends Component {
    //
    // CONSTRUCTOR FUNCTION
    // this function is the component's init
    //
    // @param props: inherit its props
    //
    //
    constructor(props) {
        super(props);

        this.state = {
            currentPage: 1
        };
    }

    //
    // DIDMOUNT FUNCTION
    // this function is execute when the component finished
    // to load the content
    //
    componentDidMount() {
        this.props.init(this.state.currentPage);
    }

    //
    // getIcon function
    // this function returns a string/color
    //
    getBackground = event => {
        const OPEN = 0;
        const CLOSE = 1;

        switch (event.tipoevento) {
            case OPEN:
                return '#4caf50';
            case CLOSE:
                return '#c0091c';
            default:
                return '#ccc';
        }
    };

    //
    // getIcon function
    // this function returns a string/icon
    //
    getIcon = event => {
        const OPEN = 0;
        const CLOSE = 1;

        switch (event.tipoevento) {
            case OPEN:
                return 'lock';
            case CLOSE:
                return 'unlock';
            default:
                return '';
        }
    };

    //
    // RENDER FUNCTION
    // this function is the charge to render
    // the main component
    //
    render = () => (
        <div>
            <InfiniteScroll
                dataLength={this.props.events.length}
                next={this.nextPage}
                hasMore={true}
                loader={this.getLoadingView()}
            >
                {
                    this.props.events.map((event, index) => (
                        <SpecialEvent
                            key={index}
                            event={event}
                            background={this.getBackground(event)}
                            icon={this.getIcon(event)}
                        />
                    ))
                }
            </InfiniteScroll>
        </div>
    )

    //
    // loadingview function
    // this function returns a view
    // spinner
    //
    getLoadingView = () => (
        <div style={{ display: 'flex' }}>

            <div className="__row_center">
                <div className="__tube_timeline" />
                <div className="__circle_timeline">
                    <span className="fa fa-refresh fa-spin" />
                </div>
                <div className="__tube_timeline" />
            </div>
        </div>
    )

    //
    // NextPage FUNCTION
    // this function is the charge to
    // fetch the next items
    //
    nextPage = () => {
        const ONE = 1;
        const timeout = 1500;
        const newCurrentPage = this.state.currentPage + ONE;
        setTimeout(() => {
            this.setState({
                currentPage: newCurrentPage
            }, this.props.getData(newCurrentPage));
        }, timeout);
    }
}

EventsContainer.propTypes = {
    events: PropTypes.array,
    getData: PropTypes.func,
    init: PropTypes.func
};

EventsContainer.defaultProps = {

};

const mapStateToProps = ({ eventsReducer }) => ({
    ...eventsReducer
});

const mapDispatchToProps = {
    init,
    getData
};

export default connect(mapStateToProps, mapDispatchToProps)(EventsContainer);
