import { ON_GET_EVENTS } from './types';
import { PATH_EVENTS } from 'source/constants';
import Service from 'helpers/service';

export const init = (page) => dispatch => dispatch(getData(page));

export const getData = (page) => dispatch => {
    Service({ page }, PATH_EVENTS, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_EVENTS, payload: data });
        })
        .catch(() => {
            //
        });
};
