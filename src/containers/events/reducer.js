import { ON_GET_EVENTS } from './types';

const initialState = {
    events: []
};

const reducerEvents = (state = initialState, action) => {
    switch (action.type) {
        case ON_GET_EVENTS:
            return {
                ...state,
                events: [
                    ...state.events,
                    ...action.payload
                ]
            };
        default:
            return { ...state };
    }
};

export default reducerEvents;
