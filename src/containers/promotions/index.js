import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import {
    getData,
    addQuery,
    setFilters,
    goProfile,
    downloadFile,
    notification,
    deletePromotions,
    goCreate
} from './actions';

class Promotions extends Component {
    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Nombre', field: 'name'},
            {name: 'fecha inicio', field: 'start_date', isDate: true},
            {name: 'Fecha fin', field: 'final_date', isDate: true},
            {name: 'descripción', field: 'description'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        return [];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addUser = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    onDelete = ids => {
        this.props.deletePromotions(ids);
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.onDelete, tooltip: 'Eliminar promociones'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'change-status',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'emailing',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'distribute',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'pass-treasurer',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    download = id => {
        const found = this.__getDataRows().find(d => d.id == id);

        if (found && found.file_url !== '') this.props.downloadFile(found.file_url);
        else this.props.notification('No existe archivo para descargar.');
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: id => this.onDelete([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: this.download
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: () => {}
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

Promotions.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    addQuery: PropTypes.func,
    goCreate: PropTypes.func,
    deletePromotions: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

Promotions.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ promotions }) => ({
    ...promotions
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    downloadFile,
    deletePromotions,
    notification,
    goCreate,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Promotions);
