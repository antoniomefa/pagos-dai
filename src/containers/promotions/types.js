
export const ON_FETCH_PROMOTIONS = 'App/Promotions/ON_FETCH_PROMOTIONS';
export const ON_GET_PROMOTIONS_SUCCESS = 'App/Promotions/ON_GET_PROMOTIONS_SUCCESS';
export const ON_GET_PROMOTIONS_FAIL = 'App/Promotions/ON_GET_PROMOTIONS_FAIL';
export const ON_SET_FILTERS = 'App/Promotions/ON_SET_FILTERS';
export const ON_DELETE_PROMOTION = 'App/Promotions/ON_DELETE_PROMOTION';
