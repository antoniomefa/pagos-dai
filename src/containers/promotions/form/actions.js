import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_PROMOTION } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createPromotion = data => dispatch => {
    Service(JSON.stringify(data), 'api/promotions', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Promoción creada correctamente',
                color: 'primary',
                alias: 'created-promotion',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear la promoción',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/promotions/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PROMOTION, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Promoción no encontrada',
                color: 'danger',
                alias: 'get-promotion',
                time: 3
            }));
            dispatch(push('/promotions'));
        });
};

export const updatePromotion = data => dispatch => {
    Service(JSON.stringify(data), 'api/promotions', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
