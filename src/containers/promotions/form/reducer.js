import { ON_GET_PROMOTION } from './types';

const initialState = {
    promotion: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_PROMOTION:
            return {
                ...state,
                promotion: action.data
            };
        default:
            return { ...state };
    }
}
