import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import {
    goEdit,
    createPromotion,
    updatePromotion,
    getProfile,
    cancelForm
} from './actions';
import {
    notification
} from '../actions';
import UploadFile from 'components/uploadFile';
import moment from 'moment';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 1,
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            file_url: '',
            formValues: {
                name: '',
                description: '',
                start_date: '',
                final_date: ''
            },
            roles: []
        };
    }

    componentDidMount() {
        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.promotion.id) {

            formValues.name = nextProps.promotion.name;
            formValues.description = nextProps.promotion.description;
            formValues.start_date = moment(nextProps.promotion.start_date).format('YYYY-MM-DD');
            formValues.final_date = moment(nextProps.promotion.final_date).format('YYYY-MM-DD');

            this.setState({
                formValues,
                file_url: nextProps.promotion.file_url
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return ['name', 'start_date', 'final_date'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [
            /./,
            /./,
            /./,
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        const ZERO = 0;
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const initial_date = moment(this.state.formValues.start_date);
        const final_date = moment(this.state.formValues.final_date);

        if (initial_date.diff(final_date) >= ZERO) {
            this.props.notification('La fecha final debe ser mayor a la inicial');
            return;
        }

        const form = {
            name: this.state.formValues['name'],
            description: this.state.formValues['description'],
            start_date: this.state.formValues['start_date'],
            final_date: this.state.formValues['final_date'],
            file_url: this.state.file_url
        };

        if (this.__isEditing()) {
            this.props.updatePromotion({ ...form, id: this.props.promotion.id });
        } else {
            this.props.createPromotion(form);
        }

    }

    getOptionsRol = () => {
        return this.state.roles.map(r => ({ value: r.id, label: r.name }));
    }

    onFileUploaded = file_url => {
        this.setState({ file_url });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Nombre"
                        htmlFor="name"
                        icon="code"
                        requested={formRequested}
                        id="name"
                        onChange={this.onChange.bind(this, 'name')}
                        onBlur={this.onBlur.bind(this, 'name')}
                        value={formValues['name']}
                        placeholder="Escribe el nombre de la promoción..."
                        aria="name"
                        hasError={!!errorMessages['name']}
                        invalidText={errorMessages['name']}
                        required
                        isGroup
                        hasIcon
                    />
                    <UploadFile
                        onFileUploaded={this.onFileUploaded}
                        previousName={this.state.file_url}
                        types="image/x-png,image/gif,image/jpeg,application/pdf"
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Descripción"
                        htmlFor="description"
                        icon="code"
                        parentClass="col-12"
                        requested={formRequested}
                        id="description"
                        onChange={this.onChange.bind(this, 'description')}
                        onBlur={this.onBlur.bind(this, 'description')}
                        value={formValues['description']}
                        placeholder="Escribe una descripcion de la promoción..."
                        aria="description"
                        hasError={!!errorMessages['description']}
                        invalidText={errorMessages['description']}
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Fecha de inicio"
                        htmlFor="start_date"
                        icon="calendar"
                        requested={formRequested}
                        id="start_date"
                        type="date"
                        onChange={this.onChange.bind(this, 'start_date')}
                        onBlur={this.onBlur.bind(this, 'start_date')}
                        value={formValues['start_date']}
                        placeholder="Cual es la fecha de inicio"
                        aria="start_date"
                        hasError={!!errorMessages['start_date']}
                        invalidText={errorMessages['start_date']}
                        isGroup
                        hasIcon
                        required
                    />
                    <Input
                        hasLabel
                        label="Fecha de fin"
                        htmlFor="final_date"
                        icon="calendar"
                        type="date"
                        requested={formRequested}
                        id="final_date"
                        onChange={this.onChange.bind(this, 'final_date')}
                        onBlur={this.onBlur.bind(this, 'final_date')}
                        value={formValues['final_date']}
                        placeholder="Cual es la fecha de fin"
                        aria="final_date"
                        hasError={!!errorMessages['final_date']}
                        invalidText={errorMessages['final_date']}
                        isGroup
                        required
                        hasIcon
                    />
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    createPromotion: PropTypes.func,
    notification: PropTypes.func,
    promotion: PropTypes.object,
    cancelForm: PropTypes.func,
    updatePromotion: PropTypes.func
};

const mapStateToProps = ({ promotionForm }) => ({
    ...promotionForm
});

const mapDispatchToProps = {
    goEdit,
    createPromotion,
    getProfile,
    notification,
    updatePromotion,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
