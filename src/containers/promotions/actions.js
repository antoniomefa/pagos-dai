import {
    ON_GET_PROMOTIONS_SUCCESS,
    ON_FETCH_PROMOTIONS, ON_SET_FILTERS,
    ON_DELETE_PROMOTION
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { downloadB64 } from 'helpers';
import { sendNotification } from 'actions';

export function downloadFile(url) {
    return dispatch => {

        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                downloadB64(mime, name, data);
            })
            .catch(() => {
                dispatch(sendNotification({
                    message: 'No fe posible descargar el archivo',
                    level: 'warning'
                }));
            });


    };
}

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_PROMOTIONS });

        Service(query, 'api/promociones', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_PROMOTIONS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/promotions/create'));
    };
}

export const deletePromotions = (users, withBack = false) => dispatch => {
    users.forEach(id => {
        Service({}, `api/promotions/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_PROMOTION, id: data.id });
                if (withBack) {
                    dispatch(push('/promotions'));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/promotions/${id}`));
    };
}
