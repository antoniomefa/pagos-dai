import {
    ON_GET_PROMOTIONS_SUCCESS,
    ON_GET_PROMOTIONS_FAIL,
    ON_FETCH_PROMOTIONS,
    ON_SET_FILTERS,
    ON_DELETE_PROMOTION
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    loading: false
};
const ONE = 1;
const ZERO = 0;

export default function providersReducer(state = initialState, action) {
    var index = state.rows.findIndex(r => r.id === action.id);

    switch (action.type) {
        case ON_FETCH_PROMOTIONS:
            return {
                ...state,
                loading: true
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_DELETE_PROMOTION:
            return {
                ...state,
                rows: [
                    ...state.rows.slice(ZERO, index),
                    ...state.rows.slice(index + ONE)
                ]
            };
        case ON_GET_PROMOTIONS_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_PROMOTIONS_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
