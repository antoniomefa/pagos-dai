import { ON_GET_PROMOTION } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/promotions/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PROMOTION, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Promocion no encontrada',
                color: 'danger',
                alias: 'get-promotion',
                time: 3
            }));
            dispatch(push('/promotions'));
        });
};
