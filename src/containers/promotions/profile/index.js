import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import { deletePromotions } from '../actions';
import { downloadFile, notification } from 'containers/promotions/actions';
import moment from 'moment';

class ProfileWarehouse extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deletePromotions([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    onDownload = () => {
        const { promotion, downloadFile, notification } = this.props;
        if (promotion.file_url !== '') downloadFile(promotion.file_url);
        else notification('No existe archivo para descargar.');
    }

    _getStartDate = () => moment(this.props.promotion.start_date).format('DD [de] MMMM [de] YYYY')
    _getEndDate = () => moment(this.props.promotion.final_date).format('DD [de] MMMM [de] YYYY')

    render() {
        const { promotion } = this.props;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h5>{promotion.name}</h5>
                    </div>
                    <div className="__card__body">
                        <small>{promotion.description}</small>
                        <p>
                            <small>
                                {`del ${this._getStartDate()} al ${this._getEndDate()}`}
                            </small>
                        </p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Eliminar"
                            icon="trash-o"
                            isIcon
                            type="danger"
                            outline
                            onButtonPress={this.onDelete}
                            permission={'delete'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Descargar"
                            type="primary"
                            icon="download"
                            isIcon
                            onButtonPress={this.onDownload}
                            permission={'download'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ProfileWarehouse.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    downloadFile: PropTypes.func,
    promotion: PropTypes.object,
    notification: PropTypes.func,
    deletePromotions: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ promotionProfile }) => ({
    ...promotionProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    deletePromotions,
    downloadFile,
    notification
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileWarehouse);
