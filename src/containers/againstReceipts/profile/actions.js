import { ON_GET_AGAINST } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/againstreceipts/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_AGAINST, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Contrarecibo no encontrado',
                color: 'danger',
                alias: 'get-against',
                time: 3
            }));
            dispatch(push('/against-receipts'));
        });
};
