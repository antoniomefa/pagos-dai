import { ON_GET_AGAINST } from './types';

const initialState = {
    against: null
};

export default function userProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_AGAINST:
            return {
                ...state,
                against: action.data
            };
        default:
            return { ...state };
    }
}
