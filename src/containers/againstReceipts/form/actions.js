import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';
import { ON_GET_AGAINST } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/againstreceipts/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_AGAINST, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Contrarecibo no encontrada',
                color: 'danger',
                alias: 'get-against',
                time: 3
            }));
            dispatch(push('/against-receipts'));
        });
};

export const updateAgainst = data => dispatch => {
    Service(JSON.stringify(data), 'api/againstreceipts', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const createAgainst = data => dispatch => {
    Service(JSON.stringify(data), 'api/againstreceipts', 'post')
        .then(() => {
            dispatch(goBack());
            dispatch(showAlert({
                text: 'Contrarecibo creado',
                color: 'success',
                alias: 'create-against',
                time: 3
            }));
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
