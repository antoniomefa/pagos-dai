import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import Select from 'components/select';
import PropTypes from 'prop-types';
import { goEdit, updateAgainst, createAgainst, getProfile, cancelForm } from './actions';
import UploadFile from 'components/uploadFile';
import { downloadFile, notification } from '../actions';
import {
    getProviders,
    getPurchaseOrders,
    getTaxDocumentsByOrder
} from 'utils/catalogs';

const ONE = 1;
const ZERO = 0;

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            pdf_url: '',
            modalIsOpen: null,
            tax_document: {},
            dropdownOpen: null,
            referral_note: {},
            formValues: {
                tax_document_id: '',
                provider_id: '',
                pdf_url: '',
                purchase_order_id: ''
            },
            providers: [],
            taxdocuments: [],
            purchaseorders: []
        };
    }

    componentDidMount() {
        this.getCatalogs();
        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    getCatalogs = () => {
        getProviders().then(providers => this.setState({ providers }));
    }

    toggle = modalIsOpen => {
        this.setState({
            modalIsOpen
        });
    }

    onUploadTax = (type, url) => {
        const tax_document = this.state.tax_document;

        if (type === 'pdf') {
            tax_document.pdf_url = url;
        } else {
            tax_document.xml_url = url;
        }

        this.setState({ tax_document });
    }

    onUploadReferral = (url) => {
        const referral_note = this.state.referral_note;

        referral_note.pdf_url = url;

        this.setState({ referral_note });
    }

    saveTax = () => {
        if (!this.state.tax_document.pdf_url || !this.state.tax_document.xml_url) {
            this.props.notification('Debes agregar el xml y el pdf');
            return;
        }

        const data = {
            id: -1,
            ...this.state.tax_document
        };

        const formValues = this.state.formValues;

        formValues.tax_documents = [
            ...formValues.tax_documents,
            ...[data]
        ];

        this.setState({
            modalIsOpen: null,
            tax_document: {},
            formValues
        });
    }

    saveReferral = () => {
        const amount = this.state.formValues.amount;
        if (!this.state.referral_note.pdf_url || amount === '' || isNaN(parseFloat(amount))) {
            if (isNaN(parseFloat(amount))) {
                this.props.notification('El valor del monto debe ser numerico');
            } else {
                this.props.notification('Debes completar todos los campos');
            }
            return;
        }

        const data = {
            id: -1,
            amount: this.state.formValues.amount,
            ...this.state.referral_note
        };

        const formValues = this.state.formValues;

        formValues.referral_notes = [
            ...formValues.referral_notes,
            ...[data]
        ];
        formValues.amount = '';

        this.setState({
            modalIsOpen: null,
            referral_note: {},
            formValues
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.against.id) {

            formValues.tax_document_id = nextProps.against.tax_document.id;
            formValues.provider_id = nextProps.against.tax_document.provider.id;
            formValues.purchase_order_id = nextProps.against.tax_document.purchase_order.id;
            formValues.pdf_url = nextProps.against.pdf_url;

            getPurchaseOrders(nextProps.against.tax_document.provider.id).then(
                purchaseorders => this.setState({ purchaseorders })
            );

            getTaxDocumentsByOrder(nextProps.against.tax_document.purchase_order.id).then(
                taxdocuments => this.setState({ taxdocuments })
            );

            this.setState({
                formValues
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;

        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;

                if (field === 'provider_id') {
                    getPurchaseOrders(select.value).then(
                        purchaseorders => this.setState({ purchaseorders })
                    );
                } else if (field === 'purchase_order_id') {
                    getTaxDocumentsByOrder(select.value).then(
                        taxdocuments => this.setState({ taxdocuments })
                    );
                }
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return ['pdf_url', 'provider_id', 'purchase_order_id', 'tax_document_id'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [
            /./,
            /./,
            /./,
            /./
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            if (this.state.formValues.pdf_url === '' || !this.state.formValues.pdf_url) {
                this.props.notification('El pdf es requerido');
            }
            return this.setState({ invalidForm: true });
        }

        const form = {
            tax_document_id: this.state.formValues.tax_document_id,
            pdf_url: this.state.formValues.pdf_url
        };

        if (this.__isEditing()) {
            this.props.updateAgainst({ ...form, id: this.props.against.id });
        } else {
            this.props.createAgainst(form);
        }

    }

    getOptionsProviders = () => {
        return this.state.providers.map(r => ({ value: r.id, label: r.bussiness_name }));
    }

    getOptionsTax = () => {
        return this.state.taxdocuments.map(r => ({ value: r.id, label: r.folio }));
    }

    getOptionsPurchases = () => {
        return this.state.purchaseorders.map(r => ({ value: r.id, label: `OC ID: ${r.id}` }));
    }

    onFileUploaded = pdf_url => {
        const formValues = this.state.formValues;

        formValues.pdf_url = pdf_url;

        this.setState({ formValues });
    }

    toggleDropdown = dropdown => {
        if (dropdown !== this.state.dropdownOpen) {
            this.setState({
                dropdownOpen: dropdown
            });
        } else {
            this.setState({
                dropdownOpen: null
            });
        }
    }

    downloadFile = url => {
        this.props.downloadFile(url, false);
    }

    deleteInvoice(index) {
        const formValues = this.state.formValues;

        formValues.tax_documents = [
            ...formValues.tax_documents.slice(ZERO, index),
            ...formValues.tax_documents.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    deleteReferral(index) {
        const formValues = this.state.formValues;

        formValues.referral_notes = [
            ...formValues.referral_notes.slice(ZERO, index),
            ...formValues.referral_notes.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <UploadFile
                        onFileUploaded={this.onFileUploaded}
                        previousName={this.state.formValues.pdf_url}
                        types="application/pdf"
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Proveedor"
                        htmlFor="provider_id"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'provider_id')}
                        onBlur={this.onBlur.bind(this, 'provider_id')}
                        id="provider_id"
                        value={formValues['provider_id']}
                        placeholder="selecciona un proveedor..."
                        options={this.getOptionsProviders()}
                        hasError={!!errorMessages['provider_id']}
                        invalidText={errorMessages['provider_id']}
                        required
                        hasIcon
                    />
                    <Select
                        hasLabel
                        label="Orden de compra"
                        htmlFor="purchase_order_id"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'purchase_order_id')}
                        onBlur={this.onBlur.bind(this, 'purchase_order_id')}
                        id="purchase_order_id"
                        value={formValues['purchase_order_id']}
                        placeholder="selecciona una orden de compra..."
                        options={this.getOptionsPurchases()}
                        hasError={!!errorMessages['purchase_order_id']}
                        invalidText={errorMessages['purchase_order_id']}
                        required
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Documento fiscal"
                        htmlFor="tax_document_id"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'tax_document_id')}
                        onBlur={this.onBlur.bind(this, 'tax_document_id')}
                        id="tax_document_id"
                        value={formValues['tax_document_id']}
                        placeholder="selecciona un documento fiscal..."
                        options={this.getOptionsTax()}
                        hasError={!!errorMessages['tax_document_id']}
                        invalidText={errorMessages['tax_document_id']}
                        required
                        hasIcon
                    />
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateAgainst: PropTypes.func,
    createAgainst: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    against: PropTypes.object,
    cancelForm: PropTypes.func
};

const mapStateToProps = ({ againstReceiptForm }) => ({
    ...againstReceiptForm
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    notification,
    updateAgainst,
    createAgainst,
    downloadFile,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
