import {
    ON_GET_AGAINST_RECEIPTS_SUCCESS, ON_DELETE_AGAINST,
    ON_FETCH_AGAINST_RECEIPTS, ON_SET_FILTERS, ON_REFRESH
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';

import Service from 'helpers/service';
import { sendNotification } from 'actions';
import { downloadB64 } from 'helpers';

export function downloadFile(url) {
    return dispatch => {
        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                dispatch({ type: ON_FETCH_AGAINST_RECEIPTS });
                downloadB64(mime, name, data);
            })
            .catch(() => {
                //
            });


    };
}

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_AGAINST_RECEIPTS });

        Service(query, 'api/againstreceipts', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_AGAINST_RECEIPTS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/against-receipts/${id}`));
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/against-receipts/create'));
    };
}

export const deleteAgainst = (ids, withBack = false) => dispatch => {
    const ONE = 1;
    ids.forEach((id, index) => {
        Service({}, `api/againstreceipts/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_AGAINST, id: data.id });
                if (withBack) {
                    dispatch(push('/payment-relationships'));
                }

                if (index === ids.length - ONE) {
                    dispatch({ type: ON_REFRESH });
                    dispatch(sendNotification({
                        message: 'Relaciones de pago eliminadas',
                        level: 'success'
                    }));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};
