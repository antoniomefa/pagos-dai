import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import {
    getData,
    addQuery,
    setFilters,
    goProfile,
    goCreate,
    deleteAgainst,
    downloadFile,
    notification
} from './actions';

/* CATALOGS */
import {
    getProviders,
    getOrdersCatalog
} from 'utils/catalogs';

class AgainstReceipts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            providersCatalog: [],
            ordersCatalog: []
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getProvidersCatalog();
        this.getOrdersCatalog();

    }

    getProvidersCatalog = () => {
        getProviders().then(providersCatalog => this.setState({ providersCatalog }));
    }

    getOrdersCatalog = () => {
        getOrdersCatalog().then(ordersCatalog => this.setState({ ordersCatalog }));
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.refresh) {
            this.props.getData(this.props.query);
        }
    }


    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Proveedor', field: 'tax_document.provider.bussiness_name'},
            {name: 'Factura', field: 'tax_document.folio'},
            {name: 'Orden de compra id', field: 'tax_document.purchase_order.id'},
            {name: 'Monto', field: 'tax_document.total'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { providersCatalog } = this.state;
        const { ordersCatalog } = this.state;

        var providersOptions = providersCatalog.map(p => (
            { label: p.bussiness_name, value: p.id}
        ));
        var ordersOptions = ordersCatalog.map(o => (
            { label: o.id, value: o.id }
        ));
        return [
            { name: 'proveedor', field: 'provider_id', options: providersOptions },
            { name: 'OC id', field: 'purchase_order_id', options: ordersOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    download = id => {
        const found = this.__getDataRows().find(d => d.id == id);

        if (found && found.pdf_url !== '') this.props.downloadFile(found.pdf_url);
        else this.props.notification('No existe archivo para descargar.');
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    onDelete = ids => {
        this.props.deleteAgainst(ids);
    }

    __addAgainst = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addAgainst, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.onDelete, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: id => this.onDelete([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: this.download
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: () => {}
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

AgainstReceipts.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    goCreate: PropTypes.func,
    notification: PropTypes.func,
    downloadFile: PropTypes.func,
    setFilters: PropTypes.func,
    deleteAgainst: PropTypes.func,
    goProfile: PropTypes.func
};

AgainstReceipts.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ againstReceipts }) => ({
    ...againstReceipts
});

const mapDispatchToProps = {
    getData,
    addQuery,
    deleteAgainst,
    setFilters,
    goCreate,
    downloadFile,
    notification,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(AgainstReceipts);
