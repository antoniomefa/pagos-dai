
export const data = [
    {
        id: 1,
        factura: 'F-S00212',
        provider: 'Jonathan Sa de CV',
        monto: 12000
    },
    {
        id: 2,
        factura: 'F-S002567',
        provider: 'Luis Angel Sa de CV',
        monto: 12000
    },
    {
        id: 3,
        factura: 'F-S00234',
        provider: 'Marco SA de CV',
        monto: 12000
    },
    {
        id: 4,
        factura: 'F-S00243',
        provider: 'Edmundo SA DE CV',
        monto: 12000
    },
    {
        id: 5,
        factura: 'F-S002424',
        provider: 'Jonathan Sa de CV',
        monto: 12000
    },
    {
        id: 6,
        factura: 'F-S0022345',
        provider: 'Luis Angel Sa de CV',
        monto: 12000
    },
    {
        id: 7,
        factura: 'F-S002',
        provider: 'Marco SA de CV',
        monto: 12000
    },
    {
        id: 8,
        factura: 'F-S002',
        provider: 'Edmundo SA DE CV',
        monto: 12000
    },
    {
        id: 9,
        factura: 'F-S002',
        provider: 'Jonathan Sa de CV',
        monto: 12000
    },
    {
        id: 10,
        factura: 'F-S002',
        provider: 'Luis Angel Sa de CV'
    }
];

export const data2 = [
    {
        id: 11,
        factura: 'F-S002343',
        provider: 'Jonathan Sa de CV',
        monto: 12000
    },
    {
        id: 12,
        factura: 'F-S0025456',
        provider: 'Luis Angel Sa de CV',
        monto: 12000
    },
    {
        id: 13,
        factura: 'F-S002234',
        provider: 'Marco SA de CV',
        monto: 12000
    },
    {
        id: 14,
        factura: 'F-S00247',
        provider: 'Edmundo SA DE CV',
        monto: 12000
    },
    {
        id: 15,
        factura: 'F-S002678',
        provider: 'Jonathan Sa de CV',
        monto: 12000
    },
    {
        id: 16,
        factura: 'F-S002768',
        provider: 'Luis Angel Sa de CV',
        monto: 12000
    },
    {
        id: 17,
        factura: 'F-S00223',
        provider: 'Marco SA de CV',
        monto: 12000
    },
    {
        id: 18,
        factura: 'F-S00298765',
        provider: 'Edmundo SA DE CV',
        monto: 12000
    },
    {
        id: 19,
        factura: 'F-S00224',
        provider: 'Jonathan Sa de CV',
        monto: 12000
    },
    {
        id: 20,
        factura: 'F-S002',
        provider: 'Luis Angel Sa de CV',
        monto: 12000
    }
];
