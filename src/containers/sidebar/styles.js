
const styles = {
    bmBurgerButton: {
        position: 'fixed',
        width: '36px',
        height: '30px',
        left: '10px',
        top: '10px'
    },
    bmBurgerBars: {
        background: '#fff'
    },
    bmCrossButton: {
        height: '24px',
        width: '24px'
    },
    bmCross: {
        background: '#bdc3c7'
    },
    bmMenu: {
        background: '#477cb5',
        padding: '2.5em 0',
        fontSize: '1.15em'
    },
    bmMorphShape: {
        fill: '#373a47'
    },
    bmItemList: {
        color: '#b8b7ad',
        padding: '0.8em',
        display: 'flex',
        flexDirection: 'column'
    },
    bmItem: {
        padding: 7,
        color: 'white'
    },
    bmOverlay: {
        background: 'rgba(0, 0, 0, 0.3)'
    }
};

export default styles;
