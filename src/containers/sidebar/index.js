import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import jQuery from 'jquery';
import { logout } from 'helpers';
import SweetAlert from 'react-bootstrap-sweetalert';
import { connect } from 'react-redux';
import { renewSession, me } from 'containers/wrapper/actions';
import { goSignIn } from 'redux-actions/auth';

class Sidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAlert: false,
            currentMenu: 'slide',
            side: 'left',
            active: false
        };
    }

    setActive = item => {
        if (!item.children || !item.children.length) {
            this.setState({ active: item.name });
        }
    }

    getIconName = (item) => {
        if (this.moduleActive(item)) return 'fa fa-minus-square';

        return 'fa fa-plus-square';
    }

    submoduleActive = item => {
        const { module } = this.props;

        return item.name == module || (item.children && item.children.some(i => i.alias == module));
    }

    moduleActive = item => {
        var activeSubmodule = this.submoduleActive(item);
        var sidebarMenu = jQuery('.sidebar-dropdown.active .sidebar-submenu');
        // sidebar-submenu
        if (activeSubmodule) {
            sidebarMenu.css('display', 'block');
        }

        return item.name == this.state.active || activeSubmodule ? 'active' : '';
    }

    componentDidMount() {
        // var exp = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;
        const TWO_HUNDRED = 200;
        var bgs = 'bg1 bg2 bg3 bg4';
        var themes = 'chiller-theme ice-theme cool-theme light-theme';

        jQuery(function($) {
            // Dropdown menu
            $('.sidebar-dropdown > a').click(function() {
                $('.sidebar-submenu').slideUp(TWO_HUNDRED);
                if ($(this).parent().hasClass('active')) {
                    $('.sidebar-dropdown').removeClass('active');
                    $(this).parent().removeClass('active');
                } else {
                    $('.sidebar-dropdown').removeClass('active');
                    $(this).next('.sidebar-submenu').slideDown(TWO_HUNDRED);
                    $(this).parent().addClass('active');
                }

            });

            if(window.screen.width < 768){ /// <-- Oculta el menu automaticamente
                $('.page-wrapper').removeClass('toggled');
                $('.page-wrapper').removeClass('withPadding');
            }

            // close sidebar
            $('.fas.fa-times').click(function() {
                $('.page-wrapper').removeClass('toggled');
                $('.page-wrapper').removeClass('withPadding');
            });

            // show sidebar
            $('#show-sidebar').click(function() {
                $('.page-wrapper').addClass('toggled');
                $('.page-wrapper').addClass('withPadding');
            });

            // switch between themes
            $('[data-theme]').click(function() {
                $('[data-theme]').removeClass('selected');
                $(this).addClass('selected');
                $('.page-wrapper').removeClass(themes);
                $('.page-wrapper').addClass($(this).attr('data-theme'));
            });

            // switch between background images
            $('[data-bg]').click(function() {
                $('[data-bg]').removeClass('selected');
                $(this).addClass('selected');
                $('.page-wrapper').removeClass(bgs);
                $('.page-wrapper').addClass($(this).attr('data-bg'));
            });

            // toggle background image
            $('#toggle-bg').change(function(e) {
                e.preventDefault();
                $('.page-wrapper').toggleClass('sidebar-bg');
            });
        });
    }

    showAlert = () => {
        if (!this.state.showAlert) return null;

        return (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Si, cerrar mi sesión"
                confirmBtnBsStyle="danger"
                cancelBtnBsStyle="default"
                title="Espera"
                onConfirm={this.onLogoutConfirm}
                onCancel={() => this.setState({ showAlert: false })}
            >
                ¿ Estas se guro de querer cerrar tu sesión ?
            </SweetAlert>
        );
    }

    __renderMenu = (item, index) => {
        const { module } = this.props;
        return (
            <li
                className={`sidebar-dropdown ${this.moduleActive(item) ? 'active' : ''}`}
                key={index}
                onClick={() => this.setActive(item)}
            >
                {
                    item.children && item.children.length ? (
                        <a href="#">
                            <span>{item.name}</span>
                        </a>
                    ) : (
                        this.props.module == item.alias ? (
                            <Link className="active">
                                <span>{item.name}</span>
                            </Link>
                        ) : (
                            <Link to={item.path}>
                                <span>{item.name}</span>
                            </Link>
                        )
                    )
                }
                {
                    item.children && item.children.length && (
                        <div
                            className="sidebar-submenu"
                            style={ this.moduleActive(item) ? { display: 'block' } : {}}
                        >
                            <ul>
                                {
                                    item.children && item.children.map((c, indx) => (
                                        <li key={indx}>
                                            <Link
                                                to={c.path}
                                                className={`${module == c.alias ? 'active' : ''}`}
                                            >
                                                <span>{c.name}</span>
                                            </Link>
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                    )
                }
            </li>
        );
    }

    onLogoutConfirm = () => {
        this.setState({
            showAlert: false
        }, () => {
            logout();
            this.props.renewSession();
            this.props.goSignIn();
        });
    }

    logout = () => {
        this.setState({
            showAlert: true
        });
    }

    getUserName = () => {
        //debugger
        const { user } = this.props.auth;
        localStorage.setItem("username", user.name)
        localStorage.setItem("useremail", user.email)
        return user.name;
    }

    getUserRol = () => {
        const ZERO = 0;
        const { user } = this.props.auth;

        if (!user.roles.length) return 'Sin rol';

        return user.roles[ZERO].name.toUpperCase();
    }

    doubleClick = () => {
        this.props.me();
    }

    clearCache = () => {
        location.reload(true);
    }

    render() {
        const { items } = this.props;
        return (
            <nav id="sidebar" className="sidebar-wrapper">
                <div className="sidebar-content content">
                    <div className="sidebar-brand">
                        <a href="#">
                            {'DAI ALARMAS'}
                        </a>
                        {/* <div id="close-sidebar">
                            <span onClick={this.clearCache} className="fa fa-refresh"/>
                        </div> */}
                        <div id="close-sidebar">
                            <i className="fas fa-times" style={{ marginLeft: 5 }}></i>
                        </div>
                    </div>
                    <div className="sidebar-header">
                        <div className="user-pic">
                            <img
                                className="img-responsive img-rounded"
                                onDoubleClick={this.doubleClick}
                                src="/assets/images/profile.png"
                                alt="User picture"
                            />
                        </div>
                        <div className="user-info">
                            <span className="user-name">
                                <strong>{this.getUserName()}</strong>
                            </span>
                            <span className="user-status">
                                <span onClick={this.logout}>cerrar sesión</span>
                            </span>
                        </div>
                    </div>
                    <div className="sidebar-menu">
                        <ul>
                            {
                                items && items.filter(i => i).map((item, index) =>
                                    this.__renderMenu(item, index)
                                )
                            }
                        </ul>
                    </div>
                </div>
                {this.showAlert()}
            </nav>



        );
    }
}

Sidebar.propTypes = {
    items: PropTypes.array,
    module: PropTypes.string,
    auth: PropTypes.object,
    renewSession: PropTypes.func,
    goSignIn: PropTypes.func,
    me: PropTypes.func,
    submodule: PropTypes.string
};

Sidebar.defaultOptions = {

};

const mapStateToProps = ({ auth }) => ({ auth });

const mapDispatchToProps = { renewSession, goSignIn, me };

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);

/*
<Menu
    id={currentMenu}
    pageWrapId={'page-wrap'}
    outerContainerId={'outer-container'}
    styles={styles}
>
    {items && items.filter(i => i).map((item, index) => this.__renderMenu(item, index))}
</Menu>
*/
