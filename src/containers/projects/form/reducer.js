import { ON_GET_PROJECT } from './types';

const initialState = {
    project: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_PROJECT:
            return {
                ...state,
                project: action.data
            };
        default:
            return { ...state };
    }
}
