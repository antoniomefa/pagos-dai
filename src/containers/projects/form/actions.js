import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_PROJECT } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createUser = data => dispatch => {
    Service(JSON.stringify(data), 'api/projects', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Usuario creado correctamente',
                color: 'primary',
                alias: 'created-user',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear el proyecto',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/projects/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PROJECT, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Proyecto no encontrado',
                color: 'danger',
                alias: 'get-project',
                time: 3
            }));
            dispatch(push('/projects'));
        });
};

export const updateProject = data => dispatch => {
    Service(JSON.stringify(data), 'api/projects', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
