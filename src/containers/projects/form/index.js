import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Select from 'components/select';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, updateProject, getProfile, cancelForm } from './actions';
import { getWarehouses } from 'utils/catalogs';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            formValues: {
                area: '0',
                latitude: '',
                longitude: '',
                description: '',
                percentage_fees: '0',
                project_id: ''
            },
            warehouses: []
        };
    }

    componentDidMount() {
        this.__getWarehouses();

        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.project.id) {

            const { project } = nextProps;

            formValues.area = project.area + '';
            formValues.description = project.description;
            formValues.latitude = project.latitude;
            formValues.longitude = project.longitude;
            formValues.percentage_fees = project.percentage_fees + '';
            formValues.warehouse_id = project.warehouse ? project.warehouse.id : '';

            this.setState({
                formValues
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    __getWarehouses = () => {
        getWarehouses().then(response => {
            this.setState({
                warehouses: response
            });
        });
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;
        const MAX = 100;
        const MIN = 0;

        if (field == 'percentage_fees') {
            if (parseInt(value) >= MIN && parseInt(value) <= MAX) {
                formValues[field] = value;
            } else formValues[field] = '0';
        } else {
            formValues[field] = value;
        }

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return [];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        const NOT_FOUND = -1;
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const { formValues } = this.state;

        const form = {
            id: this.props.project.id,
            area: parseFloat(formValues['area']),
            latitude: formValues['latitude'],
            longitude: formValues['longitude'],
            description: formValues['description'],
            percentage_fees: parseInt(formValues['percentage_fees']),
            warehouse_id: formValues['warehouse_id'] == '' ? NOT_FOUND : formValues['warehouse_id']
        };

        this.props.updateProject({ ...form, id: this.props.project.id });
    }

    getOptionsWarehouse = () => {
        return this.state.warehouses.map(r => ({ value: r.id, label: r.title }));
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Area"
                        htmlFor="area"
                        icon="chart-pie"
                        requested={formRequested}
                        id="area"
                        onChange={this.onChange.bind(this, 'area')}
                        onBlur={this.onBlur.bind(this, 'area')}
                        value={formValues['area']}
                        placeholder="area..."
                        aria="area"
                        hasError={!!errorMessages['area']}
                        invalidText={errorMessages['area']}
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        type="description"
                        label="Descripción del proyecto"
                        htmlFor="description"
                        icon="code"
                        requested={formRequested}
                        onChange={this.onChange.bind(this, 'description')}
                        onBlur={this.onBlur.bind(this, 'description')}
                        id="description"
                        value={formValues['description']}
                        placeholder="descripción..."
                        aria="description"
                        hasError={!!errorMessages['description']}
                        invalidText={errorMessages['description']}
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Almacen"
                        htmlFor="warehouse_id"
                        icon="gear"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'warehouse_id')}
                        onBlur={this.onBlur.bind(this, 'warehouse_id')}
                        id="warehouse_id"
                        value={formValues['warehouse_id']}
                        placeholder="selecciona un almacen..."
                        options={this.getOptionsWarehouse()}
                        hasError={!!errorMessages['warehouse_id']}
                        invalidText={errorMessages['warehouse_id']}
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        label="Honorarios"
                        htmlFor="percentage_fees"
                        icon="money"
                        id="percentage_fees"
                        hasError={!!errorMessages['percentage_fees']}
                        invalidText={errorMessages['percentage_fees']}
                        requested={formRequested}
                        placeholder="porcentaje honorarios..."
                        aria="percentage_fees"
                        type="number"
                        value={formValues['percentage_fees']}
                        onChange={this.onChange.bind(this, 'percentage_fees')}
                        onBlur={this.onBlur.bind(this, 'percentage_fees')}
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="latitud"
                        htmlFor="latitude"
                        icon="map-pin"
                        type="number"
                        requested={formRequested}
                        id="latitude"
                        onChange={this.onChange.bind(this, 'latitude')}
                        onBlur={this.onBlur.bind(this, 'latitude')}
                        value={formValues['latitude']}
                        placeholder="latitude..."
                        aria="latitude"
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        label="longitud"
                        htmlFor="longitude"
                        icon="map-pin"
                        type="number"
                        requested={formRequested}
                        id="longitude"
                        onChange={this.onChange.bind(this, 'longitude')}
                        onBlur={this.onBlur.bind(this, 'longitude')}
                        value={formValues['longitude']}
                        placeholder="longitud..."
                        aria="longitude"
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateProject: PropTypes.func,
    project: PropTypes.object,
    cancelForm: PropTypes.func
};

const mapStateToProps = ({ projectForm }) => ({
    ...projectForm
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    updateProject,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
