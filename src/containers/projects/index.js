import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import { getData, addQuery, setFilters, goProfile, sync } from './actions';

/* CATALOGS */
import { getCustomers, getProjectStatus } from 'utils/catalogs';

class Projects extends Component {
    constructor(props) {
        super(props);

        this.state = {
            customersCatalog: [],
            statusCatalog: []
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getStatusCatalog();
        this.getCustomersCatalog();
    }

    getStatusCatalog = () => {
        getProjectStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    getCustomersCatalog = () => {
        getCustomers().then(customersCatalog => this.setState({ customersCatalog }));
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.isSync) {
            this.props.getData(this.props.query);
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Nombre', field: 'name'},
            {name: 'Cliente', field: 'client.bussiness_name'},
            {name: 'Estatus', field: 'status'},
            {name: 'Almacen', field: 'warehouse.name'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { statusCatalog } = this.state;
        const { customersCatalog } = this.state;

        var statusOptions = statusCatalog.map(status => ({ label: status, value: status}));
        var customersOptions = customersCatalog.map(c => (
            { label: c.bussiness_name, value: c.id }
        ));
        return [
            { name: 'cliente', field: 'client_id', options: customersOptions },
            { name: 'estatus', field: 'project_status', options: statusOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __syncProjects = () => {
        this.props.sync();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'sync', label: 'sincronizar', style: 'outline-secondary',
                permission: 'sync',
                action: this.__syncProjects, tooltip: 'sincronizar proyectos'
            }
        ];
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'change-status',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'emailing',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'distribute',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'pass-treasurer',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: () => {}
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: () => {}
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

Projects.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    sync: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

Projects.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ projects }) => ({
    ...projects
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goProfile,
    sync
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
