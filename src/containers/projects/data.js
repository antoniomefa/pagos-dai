
export const data = [
    {
        id: 1, name: 'San isidro',
        customer: 'Jonathan Sa de CV', status: 'active'
    },
    {
        id: 2, name: 'San rafael',
        customer: 'Luis Angel Sa de CV', status: 'inactive'
    },
    {
        id: 3, name: '209176456',
        customer: 'Marco SA de CV', status: 'active'
    },
    {
        id: 4, name: '209897111',
        customer: 'Edmundo SA DE CV', status: 'active'
    },
    {
        id: 5, name: 'San isidro',
        customer: 'Jonathan Sa de CV', status: 'active'
    },
    {
        id: 6, name: 'San rafael',
        customer: 'Luis Angel Sa de CV', status: 'active'
    },
    {
        id: 7, name: '209176456',
        customer: 'Marco SA de CV', status: 'active'
    },
    {
        id: 8, name: '209897111',
        customer: 'Edmundo SA DE CV', status: 'active'
    },
    {
        id: 9, name: 'San isidro',
        customer: 'Jonathan Sa de CV', status: 'active'
    },
    {
        id: 10, name: 'San rafael',
        customer: 'Luis Angel Sa de CV'
    }
];

export const data2 = [
    {
        id: 11, name: 'San isidro',
        customer: 'Jonathan Sa de CV', status: 'active'
    },
    {
        id: 12, name: 'San rafael',
        customer: 'Luis Angel Sa de CV', status: 'active'
    },
    {
        id: 13, name: 'Bosques acueducto',
        customer: 'Marco SA de CV', status: 'active'
    },
    {
        id: 14, name: 'Colinas del bosque',
        customer: 'Edmundo SA DE CV', status: 'active'
    },
    {
        id: 15, name: 'San isidro',
        customer: 'Jonathan Sa de CV', status: 'active'
    },
    {
        id: 16, name: 'San rafael',
        customer: 'Luis Angel Sa de CV', status: 'active'
    },
    {
        id: 17, name: 'Bosques acueducto',
        customer: 'Marco SA de CV', status: 'active'
    },
    {
        id: 18, name: 'Colinas del bosque',
        customer: 'Edmundo SA DE CV', status: 'active'
    },
    {
        id: 19, name: 'San isidro',
        customer: 'Jonathan Sa de CV', status: 'active'
    },
    {
        id: 20, name: 'San rafael',
        customer: 'Luis Angel Sa de CV', status: 'active'
    }
];
