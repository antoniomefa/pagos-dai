import { ON_GET_PROJECT } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/projects/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PROJECT, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Usuario no encontrado',
                color: 'danger',
                alias: 'get-user',
                time: 3
            }));
            dispatch(push('/projects'));
        });
};
