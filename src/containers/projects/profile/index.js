import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import { deleteUsers } from '../actions';

class ProfileProject extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteUsers([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    getLocalitation = () => {
        const { project } = this.props;

        if (project.latitude == '' && project.longitude == '') return 'Sin datos de localización';

        return `${project.latitude},${project.longitude}`;
    }

    getWarehouse = () => {
        const { project } = this.props;

        if (!project.warehouse || !project.warehouse.id) return 'Sin almacen asignado';

        return project.warehouse.title;
    }

    render() {
        const { project } = this.props;

        if (!project) return null;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h5>{project.name}</h5>
                    </div>
                    <div className="__card__body">
                        <p>
                            <span className="fa fa-map-pin" />
                            {this.getLocalitation()}
                        </p>
                        <p><span className="fa fa-chart-pie" />  {project.area || 'Sin area'}</p>
                        <p>
                            <span className="fa fa-clipboard" />
                            {this.getWarehouse()}
                        </p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ProfileProject.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    project: PropTypes.object,
    deleteUsers: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ projectProfile }) => ({
    ...projectProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    deleteUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileProject);
