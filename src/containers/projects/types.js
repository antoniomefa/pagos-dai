
export const ON_FETCH_PROJECTS = 'App/Projects/ON_FETCH_PROJECTS';
export const ON_GET_PROJECTS_SUCCESS = 'App/Projects/ON_GET_PROJECTS_SUCCESS';
export const ON_GET_PROJECTS_FAIL = 'App/Projects/ON_GET_PROJECTS_FAIL';
export const ON_SET_FILTERS = 'App/Projects/ON_SET_FILTERS';
export const PROJECTS_SYNC_SUCCESS = 'App/Projects/PROJECTS_SYNC_SUCCESS';
