import {
    ON_GET_PROJECTS_SUCCESS,
    ON_FETCH_PROJECTS,
    ON_SET_FILTERS,
    PROJECTS_SYNC_SUCCESS
} from './types';
import Service from 'helpers/service';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import { sendNotification } from 'actions';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_PROJECTS });

        Service(query, 'api/projects', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_PROJECTS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });
    };
}

export function sync() {
    return dispatch => {
        Service({}, 'api/projects/sync', 'put')
            .then(() => {
                dispatch({ type: PROJECTS_SYNC_SUCCESS });
                dispatch(sendNotification({
                    message: 'proyectos sincronizados',
                    level: 'success'
                }));
            })
            .catch(() => {
                //
            });
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/projects/${id}`));
    };
}
