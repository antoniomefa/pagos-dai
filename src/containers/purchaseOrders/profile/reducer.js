import { ON_GET_OC } from './types';

const initialState = {
    order: null
};

export default function userProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_OC:
            return {
                ...state,
                order: action.data
            };
        default:
            return { ...state };
    }
}
