import { ON_GET_OC } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/purchaseorders/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_OC, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Orden de compra no encontrado',
                color: 'danger',
                alias: 'get-OC',
                time: 3
            }));
            dispatch(push('/purchase-orders'));
        });
};
