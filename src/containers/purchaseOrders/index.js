import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';
import ModalMessage from 'components/modalMessage';

/* ACTOONS */
import {
    getData,
    addQuery,
    setFilters,
    goProfile,
    sync,
    downloadFile,
    notification,
    notify
} from './actions';

/* CATALOGS */
import {
    getProviders,
    getProjects,
    getPurchaseOrderStatus
} from 'utils/catalogs';

class PurchaseOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            purchaseOrderId: null,
            providersCatalog:[],
            projectsCatalog:[],
            statusCatalog:[]
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getProvidersCatalog();
        this.getProjectsCatalog();
        this.getStatusCatalog();
    }

    getStatusCatalog = () => {
        getPurchaseOrderStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    getProvidersCatalog = () => {
        getProviders().then(providersCatalog => this.setState({ providersCatalog }));
    }

    getProjectsCatalog = () => {
        getProjects().then(projectsCatalog => this.setState({ projectsCatalog }));
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.isSync) {
            this.props.getData(this.props.query);
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Comprador', field: 'created_by'},
            {name: 'Proveedor', field: 'provider.bussiness_name'},
            {name: 'Proyecto', field: 'project.name'},
            {
                name: 'Fecha de envio',
                field: 'sent_at',
                isDate: true,
                format: 'DD [] MMMM [del] YYYY [a las] HH:mm'
            },
            {name: 'Estatus', field: 'status'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { providersCatalog } = this.state;
        const { projectsCatalog } = this.state;
        const { statusCatalog } = this.state;

        var statusOptions = statusCatalog.map(s => ({ label: s, value: s}));
        var providersOptions = providersCatalog.map(p => (
            { label: p.bussiness_name, value: p.bussiness_name }
        ));
        var projectsOptions = projectsCatalog.map(p => (
            { label: p.name, value: p.name }
        ));
        return [
            { name: 'proveedor', field: 'provider', options: providersOptions },
            { name: 'proyecto', field: 'project', options: projectsOptions },
            { name: 'estatus', field: 'status', options: statusOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __syncProviders = () => {
        this.props.sync();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'sync', label: 'sincronizar', style: 'outline-secondary',
                permission: 'sync',
                action: this.__syncProviders, tooltip: 'sincronizar ordenes de compra'
            }
        ];
    }

    __getMasiveActions = () => {
        return [];
    }

    sendMessage = id => {
        this.setState({
            modalIsOpen: true,
            purchaseOrderId: id
        });
    }

    download = id => {
        const found = this.__getDataRows().find(d => d.id == id);

        if (found && found.pdf_url !== '') this.props.downloadFile(found.pdf_url, id);
        else this.props.notification('No existe archivo para descargar.');
    }

    renderModal = () => {
        if (!this.state.purchaseOrderId) return null;

        return (
            <ModalMessage
                isOpen={this.state.modalIsOpen}
                toggle={this.toggle}
                notification={this.props.notification}
                title="Reenviar orden de compra"
                onBtnAction={this.send}
                purchaseOrderId={this.state.purchaseOrderId}
            />
        );
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: () => {}
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: this.download
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: this.sendMessage
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    toggle = () => {
        this.setState({
            modalIsOpen: !this.state.modalIsOpen,
            purchaseOrderId: null
        });
    }

    send = (ids, description) => {
        this.props.notify(this.state.purchaseOrderId, ids.map(c => c.value), description);

        this.setState({
            modalIsOpen: false,
            purchaseOrderId: null
        });
    }

    render() {
        const { query, total } = this.props;

        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
                {this.renderModal()}
             </div>
        );
    }
}

PurchaseOrders.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    sync: PropTypes.func,
    notify: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

PurchaseOrders.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ purchaseOrders }) => ({
    ...purchaseOrders
});

const mapDispatchToProps = {
    getData,
    addQuery,
    sync,
    setFilters,
    notify,
    downloadFile,
    notification,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseOrders);
