import {
    ON_GET_PURCHASE_ORDERS_SUCCESS,
    ON_FETCH_PURCHASE_ORDERS,
    ON_SET_FILTERS,
    PURCHASE_ORDERS_SYNC_SUCCESS
} from './types';
import Service from 'helpers/service';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import { sendNotification } from 'actions';
import { downloadB64 } from 'helpers';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_PURCHASE_ORDERS });

        Service(query, 'api/purchaseorders', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_PURCHASE_ORDERS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });
    };
}

export function downloadFile(url, id) {
    return dispatch => {
        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        const baseUrl = id ? `api/purchaseorders/${id}/download`: 'api/files';

        Service(query, baseUrl, 'get')
            .then(({ data }) => {
                dispatch({ type: PURCHASE_ORDERS_SYNC_SUCCESS });
                downloadB64(mime, name, data);
            })
            .catch(() => {
                //
            });


    };
}

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export function sync() {
    return dispatch => {
        Service({}, 'api/purchaseorders/sync', 'put')
            .then(() => {
                dispatch({ type: PURCHASE_ORDERS_SYNC_SUCCESS });
                dispatch(sendNotification({
                    message: 'ordenes de compra sincronizadas',
                    level: 'success'
                }));
            })
            .catch(() => {
                //
            });
    };
}

export function notify(purchaseOrder, contacts, observations) {
    return dispatch => {
        Service(JSON.stringify({
            contacts,
            observations
        }), `api/purchaseorders/${purchaseOrder}/send`, 'put')
            .then(() => {
                dispatch({ type: PURCHASE_ORDERS_SYNC_SUCCESS });
                dispatch(sendNotification({
                    message: 'Orden de compra notificada',
                    level: 'success'
                }));
            })
            .catch(() => {
                dispatch(sendNotification({
                    message: 'Hubo un error al enviar la orden de compra',
                    level: 'warning'
                }));
            });
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/purchase-orders/${id}`));
    };
}
