import {
    ON_GET_PURCHASE_ORDERS_SUCCESS,
    ON_GET_PURCHASE_ORDERS_FAIL,
    ON_FETCH_PURCHASE_ORDERS,
    ON_SET_FILTERS,
    PURCHASE_ORDERS_SYNC_SUCCESS
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    isSync: false,
    loading: false
};

export default function providersReducer(state = initialState, action) {
    switch (action.type) {
        case ON_FETCH_PURCHASE_ORDERS:
            return {
                ...state,
                isSync: false,
                loading: true
            };
        case PURCHASE_ORDERS_SYNC_SUCCESS:
            return {
                ...state,
                isSync: true
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_GET_PURCHASE_ORDERS_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_PURCHASE_ORDERS_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
