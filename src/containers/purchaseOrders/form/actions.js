import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';
import { ON_GET_OC } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/purchaseorders/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_OC, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'OC no encontrada',
                color: 'danger',
                alias: 'get-user',
                time: 3
            }));
            dispatch(push('/purchase-orders'));
        });
};

export const updateOC = data => dispatch => {
    Service(JSON.stringify(data), 'api/purchaseorders', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
