import { ON_GET_OC } from './types';

const initialState = {
    order: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_OC:
            return {
                ...state,
                order: action.data
            };
        default:
            return { ...state };
    }
}
