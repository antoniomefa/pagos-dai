import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, updateOC, getProfile, cancelForm } from './actions';
import UploadFile from 'components/uploadFile';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button as ButtonX,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';
import { downloadFile, notification } from '../actions';

const ONE = 1;
const ZERO = 0;

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            pdf_url: '',
            modalIsOpen: null,
            tax_document: {},
            dropdownOpen: null,
            referral_note: {},
            formValues: {
                observations: '',
                tax_documents: [],
                referral_notes: []
            },
            roles: []
        };
    }

    componentDidMount() {
        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    toggle = modalIsOpen => {
        this.setState({
            modalIsOpen
        });
    }

    onUploadTax = (type, url) => {
        const tax_document = this.state.tax_document;

        if (type === 'pdf') {
            tax_document.pdf_url = url;
        } else {
            tax_document.xml_url = url;
        }

        this.setState({ tax_document });
    }

    onUploadReferral = (url) => {
        const referral_note = this.state.referral_note;

        referral_note.pdf_url = url;

        this.setState({ referral_note });
    }

    saveTax = () => {
        if (!this.state.tax_document.pdf_url || !this.state.tax_document.xml_url) {
            this.props.notification('Debes agregar el xml y el pdf');
            return;
        }

        const data = {
            id: -1,
            ...this.state.tax_document
        };

        const formValues = this.state.formValues;

        formValues.tax_documents = [
            ...formValues.tax_documents,
            ...[data]
        ];

        this.setState({
            modalIsOpen: null,
            tax_document: {},
            formValues
        });
    }

    saveReferral = () => {
        const amount = this.state.formValues.amount;
        if (!this.state.referral_note.pdf_url || amount === '' || isNaN(parseFloat(amount))) {
            if (isNaN(parseFloat(amount))) {
                this.props.notification('El valor del monto debe ser numerico');
            } else {
                this.props.notification('Debes completar todos los campos');
            }
            return;
        }

        const data = {
            id: -1,
            amount: this.state.formValues.amount,
            ...this.state.referral_note
        };

        const formValues = this.state.formValues;

        formValues.referral_notes = [
            ...formValues.referral_notes,
            ...[data]
        ];
        formValues.amount = '';

        this.setState({
            modalIsOpen: null,
            referral_note: {},
            formValues
        });
    }

    showModal = () => {
        const { formRequested, formValues, errorMessages } = this.state;
        if (this.state.modalIsOpen === 'referral') {
            return (
                <Modal isOpen={true} toggle={this.toggle} className={''}>
                    <ModalHeader toggle={this.toggle}>Nota de remisión</ModalHeader>
                    <ModalBody>
                        <div className="form-row">
                            <Input
                                hasLabel
                                label="Monto"
                                htmlFor="amount"
                                requested={formRequested}
                                id="amount"
                                onChange={this.onChange.bind(this, 'amount')}
                                onBlur={this.onBlur.bind(this, 'amount')}
                                value={formValues['amount']}
                                placeholder="Escribe el monto de la nota..."
                                aria="amount"
                                hasError={!!errorMessages['amount']}
                                invalidText={errorMessages['amount']}
                            />
                            <UploadFile
                                onFileUploaded={this.onUploadReferral}
                                previousName={this.state.referral_note.pdf_url}
                                label="PDF o imagen"
                                types="image/x-png,image/gif,image/jpeg,application/pdf"
                            />
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <ButtonX color="primary" onClick={this.saveReferral}>Guardar</ButtonX>{' '}
                        <ButtonX color="secondary" onClick={this.toggle}>Cancel</ButtonX>
                    </ModalFooter>
                </Modal>
            );
        } else if (this.state.modalIsOpen === 'tax') {
            return (
                <Modal isOpen={true} toggle={this.toggle} className={''}>
                    <ModalHeader toggle={this.toggle}>Documento fiscal</ModalHeader>
                    <ModalBody>
                        <div className="form-row">
                            <UploadFile
                                onFileUploaded={this.onUploadTax.bind(this, 'pdf')}
                                previousName={this.state.tax_document.pdf_url}
                                label="PDF"
                                types="application/pdf"
                            />
                        </div>
                        <div className="form-row">
                            <UploadFile
                                onFileUploaded={this.onUploadTax.bind(this, 'xml')}
                                previousName={this.state.tax_document.xml_url}
                                label="XML"
                                types="text/xml"
                            />
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <ButtonX color="primary" onClick={this.saveTax}>Guardar</ButtonX>{' '}
                        <ButtonX color="secondary" onClick={this.toggle}>Cancel</ButtonX>
                    </ModalFooter>
                </Modal>
            );
        }

        return null;
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.order.id) {

            formValues.observations = nextProps.order.observations;
            formValues.tax_documents = nextProps.order.tax_documents;
            formValues.referral_notes = nextProps.order.referral_notes;

            this.setState({
                formValues,
                pdf_url: nextProps.order.pdf_url
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return [];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const form = {
            observations: this.state.formValues['observations'],
            pdf_url: this.state.pdf_url,
            tax_documents: this.state.formValues.tax_documents,
            referral_notes: this.state.formValues.referral_notes
        };

        this.props.updateOC({ ...form, id: this.props.order.id });

    }

    getOptionsRol = () => {
        return this.state.roles.map(r => ({ value: r.id, label: r.name }));
    }

    onFileUploaded = pdf_url => {
        this.setState({ pdf_url });
    }

    toggleDropdown = dropdown => {
        if (dropdown !== this.state.dropdownOpen) {
            this.setState({
                dropdownOpen: dropdown
            });
        } else {
            this.setState({
                dropdownOpen: null
            });
        }
    }

    downloadFile = url => {
        this.props.downloadFile(url, false);
    }

    deleteInvoice(index) {
        const formValues = this.state.formValues;

        formValues.tax_documents = [
            ...formValues.tax_documents.slice(ZERO, index),
            ...formValues.tax_documents.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    deleteReferral(index) {
        const formValues = this.state.formValues;

        formValues.referral_notes = [
            ...formValues.referral_notes.slice(ZERO, index),
            ...formValues.referral_notes.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Observaciones"
                        htmlFor="observations"
                        icon="search"
                        requested={formRequested}
                        id="observations"
                        onChange={this.onChange.bind(this, 'observations')}
                        onBlur={this.onBlur.bind(this, 'observations')}
                        value={formValues['observations']}
                        placeholder="Escribe las observaciones..."
                        aria="observations"
                        hasError={!!errorMessages['observations']}
                        invalidText={errorMessages['observations']}
                        isGroup
                        hasIcon
                    />
                    <UploadFile
                        onFileUploaded={this.onFileUploaded}
                        previousName={this.state.pdf_url}
                        types="application/pdf"
                    />
                </div>
                <div className="form-row">
                    <div className="col-xs-12 col-sm-6" >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <h5 style={{ margin: 0, marginRight: 20 }}>Documentos fiscales</h5>
                            <span className="fa fa-plus-circle" onClick={() => {
                                this.setState({ modalIsOpen: 'tax' });
                            }} />
                        </div>
                        <div style={{ display: 'flex' }}>
                            {
                                this.state.formValues.tax_documents.map((document, index) => (
                                    <Dropdown
                                        isOpen={this.state.dropdownOpen === `tax-${index}`}
                                        toggle={() => this.toggleDropdown(`tax-${index}`)}
                                        style={{ margin: 4 }}
                                        key={index}
                                    >
                                        <DropdownToggle
                                            tag="span"
                                            data-toggle="dropdown"
                                        >
                                            <img src="/assets/images/invoice.png" width="70" />
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={() => {
                                                if (document.pdf_url === '' || !document.pdf_url) {
                                                    const message = 'No existe archivo a descargar';
                                                    this.props.notification(message);
                                                } else {
                                                    this.downloadFile(document.pdf_url);
                                                }
                                            }}>Descargar Factura</DropdownItem>
                                            <DropdownItem onClick={() => {
                                                if (document.xml_url === '' || !document.xml_url) {
                                                    const message = 'No existe archivo a descargar';
                                                    this.props.notification(message);
                                                } else {
                                                    this.downloadFile(document.xml_url);
                                                }
                                            }}>Descargar XML</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem onClick={() => {
                                                this.deleteInvoice(index);
                                            }}>Eliminar</DropdownItem>
                                        </DropdownMenu>
                                    </Dropdown>
                                ))
                            }
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <h5 style={{ margin: 0, marginRight: 20 }}>Notas de remisión</h5>
                            <span className="fa fa-plus-circle" onClick={() => {
                                this.setState({ modalIsOpen: 'referral' });
                            }}/>
                        </div>
                        <div style={{ display: 'flex' }}>
                            {
                                this.state.formValues.referral_notes.map((note, index) => (
                                    <Dropdown
                                        isOpen={this.state.dropdownOpen === `referral-${index}`}
                                        style={{ margin: 4 }}
                                        toggle={() => this.toggleDropdown(`referral-${index}`)}
                                        key={index}
                                    >
                                        <DropdownToggle
                                            tag="span"
                                            data-toggle="dropdown"
                                        >
                                            <img src="/assets/images/referral.png" width="70" />
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={() => {
                                                if (note.pdf_url === '' || !note.pdf_url) {
                                                    const message = 'No existe archivo a descargar';
                                                    this.props.notification(message);
                                                } else {
                                                    this.props.downloadFile(note.pdf_url);
                                                }
                                            }}>Descargar Nota de remisión</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem onClick={() => {
                                                this.deleteReferral(index);
                                            }}>Eliminar</DropdownItem>
                                        </DropdownMenu>
                                    </Dropdown>
                                ))
                            }
                        </div>
                    </div>
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>

                {this.showModal()}
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateOC: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    order: PropTypes.object,
    cancelForm: PropTypes.func
};

const mapStateToProps = ({ purchaseOrderForm }) => ({
    ...purchaseOrderForm
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    notification,
    updateOC,
    downloadFile,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
