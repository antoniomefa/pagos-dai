import {
    ON_GET_PAYMENT_RELATIONSHIPS_SUCCESS, ON_REFRESH,
    ON_FETCH_PAYMENT_RELATIONSHIPS, ON_SET_FILTERS,
    ON_DELETE_PAYMENT
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { sendNotification } from 'actions';
import { downloadB64 } from 'helpers';

export function downloadFile(url) {
    return dispatch => {
        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                dispatch({ type: ON_FETCH_PAYMENT_RELATIONSHIPS });
                downloadB64(mime, name, data);
            })
            .catch(() => {
                //
            });


    };
}

export function send(id) {
    return dispatch => {

        Service({}, `api/paymentrelationships/${id}/send`, 'put')
            .then(() => {
                dispatch({ type: ON_REFRESH });
                dispatch(sendNotification({
                    message: 'Relacion de pago enviada',
                    level: 'success'
                }));
            })
            .catch(() => {
                //
                dispatch(notification('Hubo un error al enviar la relación de pago'));
            });


    };
}

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_PAYMENT_RELATIONSHIPS });

        Service(query, 'api/paymentrelationships', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_PAYMENT_RELATIONSHIPS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/payment-relationships/${id}`));
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/payment-relationships/create'));
    };
}

export const deletePayment = (ids, withBack = false) => dispatch => {
    const ONE = 1;
    ids.forEach((id, index) => {
        Service({}, `api/paymentrelationships/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_PAYMENT, id: data.id });
                if (withBack) {
                    dispatch(push('/payment-relationships'));
                }

                if (index === ids.length - ONE) {
                    dispatch({ type: ON_REFRESH });
                    dispatch(sendNotification({
                        message: 'Relaciones de pago eliminadas',
                        level: 'success'
                    }));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};
