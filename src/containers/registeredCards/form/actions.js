import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';
import { ON_GET_PAYMENT } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/paymentrelationships/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PAYMENT, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Relación no encontrada',
                color: 'danger',
                alias: 'get-relation',
                time: 3
            }));
            dispatch(push('/payment-relationships'));
        });
};

export const updatePayment = data => dispatch => {
    Service(JSON.stringify(data), 'api/paymentrelationships', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const createPayment = data => dispatch => {
    Service(JSON.stringify(data), 'api/paymentrelationships', 'post')
        .then(() => {
            dispatch(goBack());
            dispatch(showAlert({
                text: 'Relación de pago creada',
                color: 'success',
                alias: 'create-relation',
                time: 3
            }));
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
