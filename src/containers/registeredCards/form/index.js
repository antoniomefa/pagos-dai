import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import Select from 'components/select';
import PropTypes from 'prop-types';
import { goEdit, updatePayment, createPayment, getProfile, cancelForm } from './actions';
import UploadFile from 'components/uploadFile';
import { downloadFile, notification } from '../actions';
import { getProviders, getCustomers } from 'utils/catalogs';

const ONE = 1;
const ZERO = 0;

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            pdf_url: '',
            modalIsOpen: null,
            tax_document: {},
            dropdownOpen: null,
            referral_note: {},
            formValues: {
                description: '',
                provider_id: '',
                pdf_url: '',
                client_id: ''
            },
            providers: [],
            customers: []
        };
    }

    componentDidMount() {
        this.getCatalogs();
        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    getCatalogs = () => {
        getProviders().then(providers => this.setState({ providers }));
        getCustomers().then(customers => this.setState({ customers }));
    }

    toggle = modalIsOpen => {
        this.setState({
            modalIsOpen
        });
    }

    onUploadTax = (type, url) => {
        const tax_document = this.state.tax_document;

        if (type === 'pdf') {
            tax_document.pdf_url = url;
        } else {
            tax_document.xml_url = url;
        }

        this.setState({ tax_document });
    }

    onUploadReferral = (url) => {
        const referral_note = this.state.referral_note;

        referral_note.pdf_url = url;

        this.setState({ referral_note });
    }

    saveTax = () => {
        if (!this.state.tax_document.pdf_url || !this.state.tax_document.xml_url) {
            this.props.notification('Debes agregar el xml y el pdf');
            return;
        }

        const data = {
            id: -1,
            ...this.state.tax_document
        };

        const formValues = this.state.formValues;

        formValues.tax_documents = [
            ...formValues.tax_documents,
            ...[data]
        ];

        this.setState({
            modalIsOpen: null,
            tax_document: {},
            formValues
        });
    }

    saveReferral = () => {
        const amount = this.state.formValues.amount;
        if (!this.state.referral_note.pdf_url || amount === '' || isNaN(parseFloat(amount))) {
            if (isNaN(parseFloat(amount))) {
                this.props.notification('El valor del monto debe ser numerico');
            } else {
                this.props.notification('Debes completar todos los campos');
            }
            return;
        }

        const data = {
            id: -1,
            amount: this.state.formValues.amount,
            ...this.state.referral_note
        };

        const formValues = this.state.formValues;

        formValues.referral_notes = [
            ...formValues.referral_notes,
            ...[data]
        ];
        formValues.amount = '';

        this.setState({
            modalIsOpen: null,
            referral_note: {},
            formValues
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.payment.id) {

            formValues.description = nextProps.payment.description;
            formValues.provider_id = nextProps.payment.provider.id;
            formValues.client_id = nextProps.payment.client.id;
            formValues.pdf_url = nextProps.payment.pdf_url;

            this.setState({
                formValues,
                pdf_url: nextProps.payment.pdf_url
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return ['pdf_url', 'client_id', 'provider_id'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [
            /./,
            /./,
            /./
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            if (this.state.formValues.pdf_url === '' || !this.state.formValues.pdf_url) {
                this.props.notification('El pdf es requerido');
            }
            return this.setState({ invalidForm: true });
        }

        const form = {
            description: this.state.formValues['description'],
            pdf_url: this.state.formValues.pdf_url,
            client_id: this.state.formValues.client_id,
            provider_id: this.state.formValues.provider_id
        };

        if (this.__isEditing()) {
            this.props.updatePayment({ ...form, id: this.props.payment.id });
        } else {
            this.props.createPayment(form);
        }

    }

    getOptionsCustomers = () => {
        return this.state.customers.map(r => ({ value: r.id, label: r.bussiness_name }));
    }

    getOptionsProviders = () => {
        return this.state.providers.map(r => ({ value: r.id, label: r.bussiness_name }));
    }

    onFileUploaded = pdf_url => {
        const formValues = this.state.formValues;

        formValues.pdf_url = pdf_url;

        this.setState({ formValues });
    }

    toggleDropdown = dropdown => {
        if (dropdown !== this.state.dropdownOpen) {
            this.setState({
                dropdownOpen: dropdown
            });
        } else {
            this.setState({
                dropdownOpen: null
            });
        }
    }

    downloadFile = url => {
        this.props.downloadFile(url, false);
    }

    deleteInvoice(index) {
        const formValues = this.state.formValues;

        formValues.tax_documents = [
            ...formValues.tax_documents.slice(ZERO, index),
            ...formValues.tax_documents.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    deleteReferral(index) {
        const formValues = this.state.formValues;

        formValues.referral_notes = [
            ...formValues.referral_notes.slice(ZERO, index),
            ...formValues.referral_notes.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Descripción"
                        htmlFor="description"
                        icon="search"
                        requested={formRequested}
                        id="description"
                        onChange={this.onChange.bind(this, 'description')}
                        onBlur={this.onBlur.bind(this, 'description')}
                        value={formValues['description']}
                        placeholder="Escribe una descripción..."
                        aria="description"
                        hasError={!!errorMessages['description']}
                        invalidText={errorMessages['description']}
                        isGroup
                        hasIcon
                    />
                    <UploadFile
                        onFileUploaded={this.onFileUploaded}
                        previousName={this.state.formValues.pdf_url}
                        types="application/pdf"
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Cliente"
                        htmlFor="client_id"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'client_id')}
                        onBlur={this.onBlur.bind(this, 'client_id')}
                        id="client_id"
                        value={formValues['client_id']}
                        placeholder="selecciona un cliente..."
                        options={this.getOptionsCustomers()}
                        hasError={!!errorMessages['client_id']}
                        invalidText={errorMessages['client_id']}
                        required
                        hasIcon
                    />
                    <Select
                        hasLabel
                        label="Proveedor"
                        htmlFor="provider_id"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'provider_id')}
                        onBlur={this.onBlur.bind(this, 'provider_id')}
                        id="provider_id"
                        value={formValues['provider_id']}
                        placeholder="selecciona un proveedor..."
                        options={this.getOptionsProviders()}
                        hasError={!!errorMessages['provider_id']}
                        invalidText={errorMessages['provider_id']}
                        required
                        hasIcon
                    />
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updatePayment: PropTypes.func,
    createPayment: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    payment: PropTypes.object,
    cancelForm: PropTypes.func
};

const mapStateToProps = ({ paymentRelationshipForm }) => ({
    ...paymentRelationshipForm
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    notification,
    updatePayment,
    createPayment,
    downloadFile,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
