import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import { getAccessToken } from 'helpers';
import { URL_BASE } from 'constants';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Radio from '@material-ui/core/Radio';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Paper from "@material-ui/core/Paper";

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import '../form/cardstable.css';
import amex from '../../../../public/assets/images/amex.png'
import visa from '../../../../public/assets/images/visa.png'
import master from '../../../../public/assets/images/mastercard.png'


const styles = theme => ({
    pimaryButton: {
        paddingTop: '5px',
        paddingBottom: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        color: 'white',
        backgroundColor: '#d11217'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    }
});


class CardsTable extends Component {

    constructor(...props) {
        super(...props)
        this.state = {
            nombre: '',
            tarjeta: '',
            cvc: '',
            fechaExpMes: '',
            fechaExpAño: '',
            error: false,
            misTarjetas: {},
            paymentSourceId: '',
            agregarTarjetas: false
        }
      }

    handleChange(field, event) {
        this.setState({[field] : event.target.value});
    }

    handleDelete(paymentSourceId){
        this.eliminaTarjeta(paymentSourceId);
        this.handleUpdate();
    }

    handleUpdate(){
        var counter = 0;
        while(counter < 5){
            this.obtenerTarjetas();
            counter++;
        }
    }

    handleSubmit() {
        var tokenParams = {
            "card": {
              "number": this.state.tarjeta,
              "name": this.state.nombre,
              "exp_year": this.state.fechaExpAño,
              "exp_month": this.state.fechaExpMes,
              "cvc": this.state.cvc,
            //   "address": {
            //       "street1": "Calle 123 Int 404",
            //       "street2": "Col. Condesa",
            //       "city": "Ciudad de Mexico",
            //       "state": "Distrito Federal",
            //       "zip": "12345",
            //       "country": "Mexico"
            //    }
            }
          };
        Conekta.Token.create(tokenParams, this.conektaSuccessResponseHandler.bind(this), this.conektaErrorResponseHandler.bind(this));
    }

    conektaSuccessResponseHandler(token) {
        //this.props.enqueueSnackbar("Tarjeta tokenizada", { variant: 'success', autoHideDuration: 10000, });
        this.grabarTarjeta(token.id)
        this.setState({ agregarTarjetas: false })
        setTimeout(()=>{ this.handleUpdate()},500);
    };

    conektaErrorResponseHandler(response) {
        this.props.enqueueSnackbar(response.message_to_purchaser, { variant: 'warning', autoHideDuration: 10000, });
        this.setState({ error: true })
    };

    obtenerTarjetas() {
        var token = getAccessToken();
        fetch(`${URL_BASE}api/conekta?action=getpaymentsources`,
        {
            method: 'get',
            headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
            }
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No fue posible obtener las tarjetas en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 6000, });
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 6000, });
            throw res;
            } else {
                this.setState({ 
                    misTarjetas: res.data
                 })
            }
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });   
    }
    
    grabarTarjeta(tokenCard) {
        var query = `{"action":"grabatarjeta","token":"${tokenCard}"}`
        var token = getAccessToken();
        fetch(`${URL_BASE}api/conekta?action=grabatarjeta`,
        {
            method: 'post',
            headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
            },
            body: query
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No se pudo grabar la tarjeta en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 6000, });
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 6000, });
            throw res;
            } else if(res.data.id!== undefined){
                this.props.enqueueSnackbar("Tarjeta grabada", { variant: 'success', autoHideDuration: 6000, });
                this.setState({ 
                    paymentSourceId: res.data.id
                 })
            }
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });  
    }

    eliminaTarjeta(paymentSourceId) {
        if(paymentSourceId==''){
            this.props.enqueueSnackbar("Selecciona la tarjeta",
            { variant: 'warning', autoHideDuration: 6000, });
            return 0;
        }
        var query = `{"action":"eliminatarjeta","paymentSourceId":"${paymentSourceId}"}`
        var token = getAccessToken();
        fetch(`${URL_BASE}api/conekta?action=eliminatarjeta`,
        {
            method: 'post',
            headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
            },
            body: query
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No se pudo eliminar la tarjeta en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 6000, });
        } else {
            this.props.enqueueSnackbar("Tarjeta eliminada", { variant: 'success', autoHideDuration: 6000, });
            this.setState({ paymentSourceId: '' })
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 6000, });
            throw res;
            } 
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });   
    }

    hasError = (field) => {
        if(this.state.error)
            if(this.state[field] === '') {
                return true;
            }
        return false;
    };

    showIcon(brand){
        switch(brand){
            case 'visa': 
                return(
                   <div>
                        <img src={visa} height="20px" width="30px"/>
                    </div>
                ) ;
            case 'mastercard': 
                return(
                    <div>
                        <img src={master} height="20px" width="30px"/>
                    </div>
                );
            case 'american_express': 
                return(
                    <div>
                        <img src={amex} height="20px" width="30px"/>
                    </div>
                );
            default: return 'no disponible';
        }
    }

    showAddCards(){
        this.setState({ agregarTarjetas: !this.state.agregarTarjetas })
    }

    componentDidMount = () => {
        const script = document.createElement('script')
        script.src = 'https://cdn.conekta.io/js/latest/conekta.js'
        script.async = true
        document.body.appendChild(script)
        
        Conekta.setPublicKey("key_V2f81Aytvb4R2CsXuF8Wyqg"); // Llave de produccion
        this.handleUpdate();
    }
    
    render() {
        const { classes } = this.props;
        const rows = this.state.misTarjetas;
        
        return ( 
            <Fragment>
                {
                    ( rows.length> 0) && (!this.state.agregarTarjetas) && (
                        <Fragment >
                            <div className="Form-info">
                                <h5>Tarjetas Registradas</h5>
                            </div>
                            <Paper className="table-container">
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">Selecciona</TableCell>
                                            <TableCell>Brand</TableCell>
                                            <TableCell align="center">Tarjetahabiente</TableCell>
                                            <TableCell align="center">Terminación</TableCell>
                                            <TableCell align="center">Expira</TableCell>
                                            <TableCell align="center">Eliminar</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {rows.map(row => (
                                            <TableRow key={row.id}>
                                                <TableCell component="th" scope="row">
                                                    <Radio
                                                        value={row.id}
                                                        checked={row.id != this.state.paymentSourceId ? false : true}
                                                        onChange={this.handleChange.bind(this, 'paymentSourceId')}
                                                    />
                                                </TableCell>
                                                <TableCell align="center">{this.showIcon(row.brand)}</TableCell>
                                                <TableCell align="center">{row.name}</TableCell>
                                                <TableCell align="center">{row.last4}</TableCell>
                                                <TableCell align="center">{row.exp_month}/{row.exp_year}</TableCell>
                                                <TableCell align="center">
                                                    <IconButton 
                                                        aria-label="delete" 
                                                        color="primary" 
                                                        onClick={()=>this.handleDelete(this.state.paymentSourceId)}>
                                                        <DeleteIcon />
                                                    </IconButton>
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Paper>
                                { 
                                    (this.state.agregarTarjetas == false) &&
                                    <Button color="secondary" onClick={()=>this.showAddCards()}>Agregar otra tarjeta</Button>
                                }
                        </Fragment>
                    )
                }
                
                {
                    (!rows.length>0) && (!this.state.agregarTarjetas) &&(
                        <Grid container direction="column" justify="space-between" alignItems="center">
                                <h5>No tiene tarjetas registradas</h5>
                                <Button color="secondary" onClick={()=>this.showAddCards()}>Agregar una tarjeta</Button>
                        </Grid>
                    )
                }
                
                {        
                
                    ( this.state.agregarTarjetas == true ) && (

                        <Fragment >
                            <div className="Form-info">
                                <h5>Resgistre los datos de la tarjeta</h5>
                            </div>
                            <Grid container className={classes.container}>     
                                <Grid item container xs={12} lg={6}>
                                    <FormControl fullWidth className={classes.formControl} error={this.hasError('nombre')}>
                                        <InputLabel>Nombre del tarjetahabiente</InputLabel>
                                        <Input
                                            type="text"
                                            id="nombre"
                                            value={this.state.nombre}
                                            onChange={this.handleChange.bind(this, 'nombre')}                            
                                        />
                                        { 
                                            this.hasError('nombre') ? (
                                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                            ) : (
                                                <FormHelperText id="component-error-text">Tal como aparece en la tarjeta</FormHelperText>
                                            )
                                        }
                                    </FormControl>
                                </Grid>
                                        
                                <Grid item container xs={8} lg={8}>
                                    <FormControl fullWidth className={classes.formControl} error={this.hasError('tarjeta')}>
                                        <InputLabel>Número de la tarjeta</InputLabel>
                                        <Input
                                            type="number"
                                            id="tarjeta"
                                            value={this.state.tarjeta}
                                            onChange={this.handleChange.bind(this, 'tarjeta')}
                                                    
                                        />
                                        {
                                            this.hasError('tarjeta') ? (
                                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                            ) : (
                                                <FormHelperText id="component-error-text">Escribe los 16 dígitos de la tarjeta</FormHelperText>
                                            )
                                        }
                                    </FormControl>
                                </Grid>

                                <Grid item container xs={4} lg={4}>
                                    <FormControl className={classes.formControl} fullWidth error={this.hasError('cvc')}>
                                        <InputLabel>CVC</InputLabel>
                                        <Input
                                            type="text"
                                            id="cvc"
                                            value={this.state.cvc}
                                            onChange={this.handleChange.bind(this, 'cvc')}
                                    />
                                        {
                                            this.hasError('cvc') ? (
                                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                            ) : (
                                                <FormHelperText id="component-error-text">Leer al reverso de la tarjeta</FormHelperText>
                                            )
                                        }
                                    </FormControl>
                                </Grid>
        
                                <Grid item container xs={6} lg={6}>
                                    <FormControl className={classes.formControl} fullWidth error={this.hasError('fechaExpMes')}>
                                        <InputLabel>Mes de expiración</InputLabel>
                                        <Input
                                            type="number"
                                            id="fechaExpMes"
                                            value={this.state.fechaExpMes}
                                            onChange={this.handleChange.bind(this, 'fechaExpMes')}
                                        />
                                        {
                                            this.hasError('fechaExpMes') ? (
                                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                            ) : (
                                                <FormHelperText id="component-error-text">A 2 dígitos (MM)</FormHelperText>
                                            )
                                        }
                                    </FormControl>
                                </Grid>
        
                                <Grid item container xs={6} lg={6}>
                                    <FormControl className={classes.formControl} fullWidth error={this.hasError('fechaExpAño')}>
                                        <InputLabel>Año de expiracón</InputLabel>
                                        <Input
                                            type="number"
                                            id="fechaExpAño"
                                            value={this.state.fechaExpAño}
                                            onChange={this.handleChange.bind(this, 'fechaExpAño')}
                                        />
                                        {
                                            this.hasError('fechaExpAño') ? (
                                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                            ) : (
                                                <FormHelperText id="component-error-text">A 4 dígitos (AAAA)</FormHelperText>
                                            )
                                        }
                                    </FormControl>
                                </Grid>

                                <Grid container direction="row" justify="space-between" alignItems="stretch"> 
                                    <Button color="secondary" onClick={()=>this.showAddCards()}>Cancelar</Button>
                                    <Button variant="contained" color='secondary' className={classes.pimaryButton} onClick={this.handleSubmit.bind(this)}>Guardar tarjeta</Button>
                                </Grid>
                            </Grid>
                        </Fragment>
                    )
                }
            </Fragment>
        );
    }
    
}

export default withStyles(styles)(withSnackbar(CardsTable))