import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SnackbarProvider } from 'notistack';

/* COMPONENTS */
import CardsTable from './form/cardstable'

/* ACTOONS */
import {
    getData,
    addQuery,
    setFilters,
    goProfile,
    downloadFile,
    notification,
    send,
    deletePayment,
    goCreate
} from './actions';


class RegisteredCards extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeStep: 0,
            formSubmitted: false,
            formvalid: true,
            account: null,
            step1: {
                nombres: '',
                nivel: '',
                apellidos: '',
                correo: '',
                telefono: '',
                modalidad: '',
                campus: '',
                carrera: '',
                idcampus: '',
                idniveleducativo: ''
            },
            step2: {
                concepto: 'Monto total de adeudo',
                cantidad: '0'
            },
            step3: {
                accept: false,
                paymethod: 'tarjeta'
            }
        };
    }

    render() {
        const { query, total } = this.props;
        return (
            <SnackbarProvider maxSnack={3}>
                <CardsTable/>
            </SnackbarProvider>
        );
    }
}

RegisteredCards.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    goCreate: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    send: PropTypes.func,
    deletePayment: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

RegisteredCards.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ registeredCards }) => ({
    ...registeredCards
});

const mapDispatchToProps = {
    getData,
    addQuery,
    goCreate,
    downloadFile,
    send,
    deletePayment,
    notification,
    setFilters,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisteredCards);
