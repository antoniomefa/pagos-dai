import { ON_GET_PAYMENT } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/paymentrelationships/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PAYMENT, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Relación no encontrada',
                color: 'danger',
                alias: 'get-payment',
                time: 3
            }));
            dispatch(push('/payment-relationships'));
        });
};
