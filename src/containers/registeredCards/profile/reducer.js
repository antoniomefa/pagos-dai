import { ON_GET_PAYMENT } from './types';

const initialState = {
    payment: null
};

export default function userProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_PAYMENT:
            return {
                ...state,
                payment: action.data
            };
        default:
            return { ...state };
    }
}
