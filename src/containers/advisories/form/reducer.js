import { ON_GET_ADVISORY } from './types';

const initialState = {
    advisory: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_ADVISORY:
            return {
                ...state,
                advisory: action.data
            };
        default:
            return { ...state };
    }
}
