import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_ADVISORY } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createAdvisory = data => dispatch => {
    Service(JSON.stringify(data), 'api/advisories', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Comunicado creado correctamente',
                color: 'primary',
                alias: 'created-advisory',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear el comunicado',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/advisories/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_ADVISORY, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Comunicado no encontrado',
                color: 'danger',
                alias: 'get-advisory',
                time: 3
            }));
            dispatch(push('/advisories'));
        });
};

export const updateAdvisory = data => dispatch => {
    Service(JSON.stringify(data), 'api/advisories', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
