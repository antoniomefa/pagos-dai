import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, createAdvisory, updateAdvisory, getProfile, cancelForm } from './actions';
import { getRoles } from 'utils/catalogs';
import DraftEditor from 'components/draftEditor';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            formValues: {
                title: '',
                body: ''
            },
            roles: []
        };
    }

    componentDidMount() {
        this.__getRoles();

        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.advisory.id) {

            formValues.title = nextProps.advisory.title;
            formValues.body = nextProps.advisory.body;

            this.setState({
                formValues
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    __getRoles = () => {
        getRoles().then(response => {
            this.setState({
                roles: response
            });
        });
    }

    changeEditorContent = (field, content) => {
        const formValues = this.state.formValues;

        formValues[field] = content;

        this.setState({ formValues });
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return ['title', 'body'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [
            /^.*$/,
            /./
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const form = {
            title: this.state.formValues['title'],
            body: this.state.formValues['body']
        };

        if (this.__isEditing()) {
            this.props.updateAdvisory({ ...form, id: this.props.advisory.id });
        } else {
            this.props.createAdvisory(form);
        }

    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Titulo"
                        htmlFor="title"
                        icon="code"
                        requested={formRequested}
                        id="title"
                        onChange={this.onChange.bind(this, 'title')}
                        onBlur={this.onBlur.bind(this, 'title')}
                        value={formValues['title']}
                        placeholder="escribe un titulo..."
                        aria="title"
                        hasError={!!errorMessages['title']}
                        invalidText={errorMessages['title']}
                        required
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <DraftEditor
                        className="col-12"
                        label="Mensaje"
                        parentStyle={{ marginTop: 20, marginBottom: 20 }}
                        isEditing={this.__isEditing()}
                        onBlur={this.onBlur.bind(this, 'body')}
                        hasError={!!errorMessages['body']}
                        invalidText={errorMessages['body']}
                        requested={formRequested}
                        required
                        content={this.state.formValues.body}
                        changeEditorContent={this.changeEditorContent.bind(this, 'body')}
                    />
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateAdvisory: PropTypes.func,
    advisory: PropTypes.object,
    cancelForm: PropTypes.func,
    createAdvisory: PropTypes.func
};

const mapStateToProps = ({ advisoryForm }) => ({
    ...advisoryForm
});

const mapDispatchToProps = {
    goEdit,
    createAdvisory,
    getProfile,
    updateAdvisory,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
