import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import {
    getData,
    addQuery,
    setFilters,
    goProfile,
    goCreate,
    deleteAdvisories,
    sendAdvisory
} from './actions';

/* CATALOGS */
import {
    getAdvisoriesStatus
} from 'utils/catalogs';

class Advisories extends Component {
    constructor(props) {
        super(props);

        this.state = {
            statusCatalog: []
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getStatusCatalog();
    }

    getStatusCatalog = () => {
        getAdvisoriesStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Creado', field: 'created_at', isDate: true},
            {name: 'Titulo', field: 'title'},
            {name: 'Fecha de envio', field: 'updated_at', isDate: true},
            {name: 'Estatus', field: 'status'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { statusCatalog } = this.state;

        var statusOptions = statusCatalog.map(status => ({ label: status, value: status}));
        return [
            { name: 'estatus', field: 'status', options: statusOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addAdvisory = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addAdvisory, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __deleteAdvisories = ids => {
        this.props.deleteAdvisories(ids);
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.__deleteAdvisories, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: (id) => this.__deleteAdvisories([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: (id) => this.props.sendAdvisory(id)
            }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

Advisories.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    deleteAdvisories: PropTypes.func,
    goCreate: PropTypes.func,
    sendAdvisory:PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

Advisories.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ advisories }) => ({
    ...advisories
});

const mapDispatchToProps = {
    getData,
    addQuery,
    deleteAdvisories,
    setFilters,
    goProfile,
    sendAdvisory,
    goCreate
};

export default connect(mapStateToProps, mapDispatchToProps)(Advisories);
