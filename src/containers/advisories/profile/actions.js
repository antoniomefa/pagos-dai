import { ON_GET_ADVISORY } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/advisories/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_ADVISORY, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Usuario no encontrado',
                color: 'danger',
                alias: 'get-advisory',
                time: 3
            }));
            dispatch(push('/advisories'));
        });
};
