import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import { deleteAdvisories } from '../actions';
import renderHTML from 'react-render-html';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteAdvisories([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    renderContent = html => {
        return renderHTML(html);
    }

    render() {
        const { advisory } = this.props;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h5>{advisory.title}</h5>
                    </div>
                    <div className="__card__body">
                        {this.renderContent(advisory.body || '')}
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Eliminar"
                            icon="trash-o"
                            isIcon
                            type="danger"
                            outline
                            onButtonPress={this.onDelete}
                            permission={'delete'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    advisory: PropTypes.object,
    deleteAdvisories: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ advisoryProfile }) => ({
    ...advisoryProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    deleteAdvisories
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
