import {
    ON_GET_ADVISORIES_SUCCESS,
    ON_FETCH_ADVISORIES,
    ON_SET_FILTERS,
    ON_DELETE_ADVISORY
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { sendNotification } from 'actions';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_ADVISORIES });

        Service(query, 'api/advisories', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_ADVISORIES_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export function sendAdvisory(id) {
    return dispatch => {
        Service({}, `api/advisories/${id}/send`, 'post')
            .then(() => {
                dispatch(sendNotification({
                    message: 'Comunicado enviado correctamente',
                    level: 'success'
                }));
            })
            .catch(() => {
                dispatch(sendNotification({
                    message: 'No fue posible enviar el comunicado',
                    level: 'warning'
                }));
            });


    };
}

export const deleteAdvisories = (users, withBack = false) => dispatch => {
    users.forEach(id => {
        Service({}, `api/advisories/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_ADVISORY, id: data.id });
                if (withBack) {
                    dispatch(push('/advisories'));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};

export function goCreate() {
    return dispatch => {
        dispatch(push('/advisories/create'));
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/advisories/${id}`));
    };
}
