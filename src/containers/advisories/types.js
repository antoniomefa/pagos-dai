
export const ON_FETCH_ADVISORIES = 'App/Advisories/ON_FETCH_ADVISORIES';
export const ON_GET_ADVISORIES_SUCCESS = 'App/Advisories/ON_GET_ADVISORIES_SUCCESS';
export const ON_GET_ADVISORIES_FAIL = 'App/Advisories/ON_GET_ADVISORIES_FAIL';
export const ON_SET_FILTERS = 'App/Advisories/ON_SET_FILTERS';
export const ON_DELETE_ADVISORY = 'App/Advisories/ON_DELETE_ADVISORY';
