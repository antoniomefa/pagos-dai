import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, createBill, updateBill, getProfile, cancelForm } from './actions';
import { notification, downloadFile } from '../actions';
import { getProjects, getOrders } from 'utils/catalogs';
import UploadFile from 'components/uploadFile';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button as ButtonX,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            dropdownOpen: null,
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            payment_voucher: {},
            formValues: {
                payment_vouchers: [],
                notes: '',
                amount: ''
            },
            projects: [],
            orders: []
        };
    }

    componentDidMount() {
        this.__getProjects();

        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.bill.id) {

          formValues.pdf_url = nextProps.bill.pdf_url;
          formValues.xml_url = nextProps.bill.xml_url;
          formValues.payment_vouchers = nextProps.bill.payment_vouchers;

          this.setState({
              formValues
          });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    __getProjects = () => {
        getProjects().then(response => {
            this.setState({
                projects: response
            });
        });
    }

    __getOrders = projectId => {
        getOrders(projectId).then(response => {
            this.setState({
                orders: response
            });
        });
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }

            if (field === 'project_id') {
                this.setState({
                    orders: []
                }, () => this.__getOrders(select.value));
            }
        } else {
            delete formValues[field];
        }

        if (field === 'project_id') {
            formValues.purchase_order_id = '';
            this.setState({
                formValues,
                orders: []
            });
        } else {
            this.setState({ formValues });
        }
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
      if (this.__isEditing()) {
        return ['pdf_url', 'xml_url'];
      }
      return ['project_id', 'pdf_url', 'xml_url'];
    }

    /* eslint-disable */
    inputsRegex = () => {
      if (this.__isEditing()) {
        return [
            /^.*$/,
            /^.*$/
        ];
      }
      return [
          /^[0-9]+$/,
          /^.*$/,
          /^.*$/
      ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            const { formValues } = this.state;

            if (formValues.pdf_url === '') {
                this.props.notification('El pdf es requerido');
            } else if (formValues.xml_url === '') {
                this.props.notification('El xml es requerido');
            }

            return this.setState({ invalidForm: true });
        }

        const { formValues } = this.state;

        const form = {
            pdf_url: formValues['pdf_url'],
            xml_url: formValues['xml_url'],
            payment_vouchers: formValues.payment_vouchers
        };

        if (this.__isEditing()) {
            this.props.updateBill({ ...form, id: this.props.bill.id });
        } else {
            this.props.createBill(form);
        }

    }

    getOptionsProjects = () => {
        return this.state.projects.map(r => ({ value: r.id, label: r.name }));
    }

    getOptionsOrders = () => {
        return this.state.orders.map(r => ({ value: r.id, label: `Orden ID: ${r.id}` }));
    }

    onUpload = (url) => {
        const payment_voucher = this.state.payment_voucher;

        payment_voucher.pdf_url = url;

        this.setState({ payment_voucher });
    }

    onSaveVoucher = () => {
        if (!this.state.payment_voucher.pdf_url) {
            this.props.notification('Debes agregar el pdf');
            return;
        }

        if (!this.state.formValues.amount || isNaN(parseFloat(this.state.formValues.amount))) {
            this.props.notification('Debes agregar el monto');
            return;
        }

        const data = {
            id: -1,
            ...this.state.payment_voucher,
            notes: this.state.formValues.notes,
            amount: parseFloat(this.state.formValues.amount)
        };

        const formValues = this.state.formValues;

        formValues.payment_vouchers = [
            ...formValues.payment_vouchers,
            ...[data]
        ];

        formValues.amount = '';
        formValues.notes = '';

        this.setState({
            modalIsOpen: null,
            payment_voucher: {},
            formValues
        });
    }

    toggle = modalIsOpen => {
        this.setState({
            payment_voucher: {},
            modalIsOpen: modalIsOpen.target.value
        });
    }

    showModal = () => {
        const { errorMessages, formValues, formRequested } = this.state;

        if (!this.state.modalIsOpen) return null;

        return (
            <Modal isOpen={true} toggle={this.toggle} className={''}>
                <ModalHeader toggle={this.toggle}>Comprobante de pago</ModalHeader>
                <ModalBody>
                    <div className="form-row">
                        <UploadFile
                            onFileUploaded={this.onUpload}
                            previousName={this.state.payment_voucher.pdf_url}
                            label="Sube tu comprobante de pago"
                            types="image/x-png,image/gif,image/jpeg,application/pdf"
                        />
                    </div>
                    <div className="form-row">
                        <Input
                            hasLabel
                            label="Monto"
                            htmlFor="amount"
                            requested={formRequested}
                            id="amount"
                            onChange={this.onChange.bind(this, 'amount')}
                            onBlur={this.onBlur.bind(this, 'amount')}
                            value={formValues['amount']}
                            placeholder="Monto"
                            aria="amount"
                            hasError={!!errorMessages['amount']}
                            invalidText={errorMessages['amount']}
                        />
                        <Input
                            hasLabel
                            label="Notas"
                            htmlFor="notes"
                            requested={formRequested}
                            id="notes"
                            onChange={this.onChange.bind(this, 'notes')}
                            onBlur={this.onBlur.bind(this, 'notes')}
                            value={formValues['notes']}
                            placeholder="Notas"
                            aria="notes"
                            hasError={!!errorMessages['notes']}
                            invalidText={errorMessages['notes']}
                        />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonX color="primary" onClick={this.onSaveVoucher}>Guardar</ButtonX>{' '}
                    <ButtonX color="secondary" onClick={this.toggle}>Cancel</ButtonX>
                </ModalFooter>
            </Modal>
        );
    }

    toggleDropdown = dropdown => {
        if (dropdown !== this.state.dropdownOpen) {
            this.setState({
                dropdownOpen: dropdown
            });
        } else {
            this.setState({
                dropdownOpen: null
            });
        }
    }

    downloadFile = url => {
        this.props.downloadFile(url, false);
    }

    _delete(index) {
        const ZERO = 0;
        const ONE = 1;
        const formValues = this.state.formValues;

        formValues.payment_vouchers = [
            ...formValues.payment_vouchers.slice(ZERO, index),
            ...formValues.payment_vouchers.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    render() {

        return (
            <form className="container">
                <div className="form-row">
                    <div className="col-xs-12 col-sm-6" >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <h5 style={{ margin: 0, marginRight: 20 }}>Comprobantes de pago</h5>
                            <span className="fa fa-plus-circle" onClick={() => {
                                this.setState({ modalIsOpen: true });
                            }} />
                        </div>
                        <div style={{ display: 'flex' }}>
                            {
                                this.state.formValues.payment_vouchers.map((document, index) => (
                                    <Dropdown
                                        isOpen={this.state.dropdownOpen === `tax-${index}`}
                                        toggle={() => this.toggleDropdown(`tax-${index}`)}
                                        style={{ margin: 4 }}
                                        key={index}
                                    >
                                        <DropdownToggle
                                            tag="span"
                                            data-toggle="dropdown"
                                        >
                                            <img src="/assets/images/voucher.png" width="70" />
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={() => {
                                                if (document.pdf_url === '' || !document.pdf_url) {
                                                    const message = 'No existe archivo a descargar';
                                                    this.props.notification(message);
                                                } else {
                                                    this.downloadFile(document.pdf_url);
                                                }
                                            }}>Descargar comprobante</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem onClick={() => {
                                                this._delete(index);
                                            }}>Eliminar</DropdownItem>
                                        </DropdownMenu>
                                    </Dropdown>
                                ))
                            }
                        </div>
                    </div>
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
                {this.showModal()}
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateBill: PropTypes.func,
    notification: PropTypes.func,
    bill: PropTypes.object,
    cancelForm: PropTypes.func,
    downloadFile: PropTypes.func,
    createBill: PropTypes.func
};

const mapStateToProps = ({ billForm }) => ({
    ...billForm
});

const mapDispatchToProps = {
    goEdit,
    createBill,
    getProfile,
    downloadFile,
    notification,
    updateBill,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
