import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import { getData, addQuery, setFilters, goProfile } from './actions';

/* CATALOGS */
import {
    getProviders,
    getBillsStatus,
    getCustomerTypes
} from 'utils/catalogs';

class BillsToPay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            providersCatalog: [],
            statusCatalog: [],
            customerTypesCatalog: []
        };
    }
    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getProvidersCatalog();
        this.getStatusCatalog();
        this.getCustomerTypesCatalog();
    }

    getProvidersCatalog = () => {
        getProviders().then(providersCatalog => this.setState({ providersCatalog }));
    }

    getStatusCatalog = () => {
        getBillsStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    getCustomerTypesCatalog = () => {
        getCustomerTypes().then(customerTypesCatalog => this.setState({ customerTypesCatalog }));
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Proveedor', field: 'provider.bussiness_name'},
            {name: 'Emisor', field: 'emitter_name'},
            {name: 'Receptor', field: 'reciever_name'},
            {name: 'OC', field: 'purchse_order.id'},
            {name: 'Folio', field: 'folio'},
            {name: 'Serie', field: 'series'},
            {name: 'Estatus', field: 'status'},
            {name: 'Total', field: 'total'},
            {name: 'Tipo cliente', field: 'client.client_type.name'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { providersCatalog } = this.state;
        const { statusCatalog } = this.state;
        const { customerTypesCatalog } = this.state;

        var providersOptions = providersCatalog.map(p => (
            { label: p.bussiness_name, value: p.id }
        ));
        var statusOptions = statusCatalog.map(s => (
            { label: s, value: s}
        ));
        var customerTypesOptions = customerTypesCatalog.map(ct => (
            { label: ct.name, value: ct.id}
        ));
        return [
            { name: 'proveedor', field: 'provider_id', options: providersOptions },
            { name: 'estatus', field: 'tax_document_status', options: statusOptions },
            { name: 'tipo cliente', field: 'client_type_id', options: customerTypesOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addUser = () => {
        // console.log('add user')
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'change-status',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'emailing',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'distribute',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'pass-treasurer',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: () => {}
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: () => {}
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: () => {}
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

BillsToPay.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

BillsToPay.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ billsToPay }) => ({
    ...billsToPay
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(BillsToPay);
