import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import {
  deleteBills,
  downloadFile,
  notification,
  passTrasurer,
  validate
} from '../actions';
import SweetAlert from 'react-bootstrap-sweetalert';

class ProfileUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: null,
            showAlert: null,
            billSelected: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteBills([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    onChangeStatus = (where) => {
        this.props.validate([this.props.bill.id], where);
    }

    onDownload = () => {
      if (this.props.bill.pdf_url === '') this.props.notification('No existe pdf a descargar');
      else this.props.downloadFile(this.props.bill.pdf_url, 'pdf');
      if (this.props.bill.xml_url === '') this.props.notification('No existe pdf a descargar');
      else this.props.downloadFile(this.props.bill.xml_url, 'xml');
    }

    Treasurer = () => {
        this.props.passTrasurer([this.props.bill.id]);
    }

    onInvalidate = text => {
        this.props.validate([this.props.bill.id], 'invalidate', text);
        this.setState({
            showAlert: false,
            noteSelected: null
        });
    }

    _showAlert = () => {
        if (!this.state.showAlert) return null;

        return (
            <SweetAlert
                input
                showCancel
                cancelBtnBsStyle="default"
                confirmBtnText="Guardar"
                validationMsg="Las observaciones no pueden estar vacias!"
                title={'describe porque la invalidas'}
                placeHolder="observaciones"
                onConfirm={this.onInvalidate}
                onCancel={() => this.setState({ billSelected: null, showAlert: false })}
            >
                Observaciones
            </SweetAlert>
        );
    }

    render() {
        const { bill } = this.props;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h3>Orden de compra ID: {
                            bill.order ?
                            bill.purchase_order.id : 'Sin OC'
                        }</h3>
                    </div>
                    <div className="__card__body">
                        <p><strong>Nombre del emisor:</strong>  {bill.emitter_name}</p>
                        <p><strong>RFC del emisor:</strong>  {bill.emitter_rfc}</p>
                        <p><strong>Folio:</strong>  {bill.folio}</p>
                        <p><strong>subtotal:</strong>  {bill.subtotal}</p>
                        <p><strong>total:</strong>  {bill.total}</p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
                {this._showAlert()}
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    downloadFile: PropTypes.func,
    validate: PropTypes.func,
    notification: PropTypes.func,
    passTrasurer: PropTypes.func,
    bill: PropTypes.object,
    deleteBills: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ billProfile }) => ({
    ...billProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    passTrasurer,
    downloadFile,
    validate,
    notification,
    deleteBills
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
