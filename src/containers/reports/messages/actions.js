import {
    ON_GET_REPORT_MESSAGES_SUCCESS,
    ON_FETCH_REPORT_MESSAGES,
    ON_SET_FILTERS
} from './types';
import { browserHistory } from 'react-router';
import Service from 'helpers/service';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_REPORT_MESSAGES });

        Service(query, 'api/reports/messages', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_REPORT_MESSAGES_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}
