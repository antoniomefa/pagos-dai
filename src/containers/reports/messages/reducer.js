import {
    ON_GET_REPORT_MESSAGES_SUCCESS,
    ON_GET_REPORT_MESSAGES_FAIL,
    ON_FETCH_REPORT_MESSAGES,
    ON_SET_FILTERS
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    loading: false
};

export default function providersReducer(state = initialState, action) {
    switch (action.type) {
        case ON_FETCH_REPORT_MESSAGES:
            return {
                ...state,
                loading: true
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_GET_REPORT_MESSAGES_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_REPORT_MESSAGES_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
