import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_MEMORY } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createMemory = data => dispatch => {
    Service(JSON.stringify(data), 'api/memories', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Memoria creada correctamente',
                color: 'primary',
                alias: 'created-memory',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear la memoria',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/memories/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_MEMORY, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Factura no encontrada',
                color: 'danger',
                alias: 'get-memory',
                time: 3
            }));
            dispatch(push('/memories'));
        });
};

export const updateMemory = data => dispatch => {
    Service(JSON.stringify(data), 'api/memories', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
