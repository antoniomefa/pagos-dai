import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'components/select';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, createMemory, updateMemory, getProfile, cancelForm } from './actions';
import { notification, downloadFile } from '../actions';
import { getProjects, getTaxDocuments, getReferralNotes } from 'utils/catalogs';
import UploadFile from 'components/uploadFile';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button as ButtonX,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';
import moment from 'moment';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            payroll: {},
            formValues: {
                project_id: '',
                pdf_url: '',
                xml_url: '',
                support_pdf_url: '',
                initial_date: '',
                final_date: '',
                percentage_vat: '',
                percentage_fees: '',
                payrolls: [],
                invoices: [],
                referral: []
            },
            projects: [],
            taxdocuments: [],
            referralnotes: []
        };
    }

    componentDidMount() {
        this.__getProjects();

        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.memory.id) {
          const format = 'YYYY-MM-DD';

          formValues.pdf_url = nextProps.memory.fees.pdf_url;
          formValues.xml_url = nextProps.memory.fees.xml_url;
          formValues.project_id = nextProps.memory.project.id;
          formValues.initial_date = moment(nextProps.memory.initial_date).format(format);
          formValues.final_date = moment(nextProps.memory.final_date).format(format);
          formValues.percentage_fees = nextProps.memory.percentage_fees + '';
          formValues.percentage_vat = nextProps.memory.percentage_vat + '';
          formValues.payrolls = this.getPayrrols(nextProps);

          this.getCatalogs(true, nextProps);


          this.setState({
              formValues
          });
        }
    }

    parseTaxDocuments = (taxdocuments) => {
      const NF = -1;
      return taxdocuments.filter(
        td => this.props.memory.tax_documents.map(t => t.id).indexOf(td.id) > NF
      ).map(r => ({ value: r.id, label: `ID: ${r.id} Folio: ${r.folio}` }));
    }

    getPayrrols = ({ memory }) => {
      return memory.payrolls.map(payroll => {
        var data = { id: payroll.id };

        if (payroll.tax_document) {
            data = {
                ...data,
                tax_document: {
                    pdf_url: payroll.tax_document.pdf_url,
                    xml_url: payroll.tax_document.xml_url,
                    observations: ''
                }
            };
        } else {
            data = {
                ...data,
                referral_note: {
                    pdf_url: payroll.referral_note.pdf_url,
                    amount: payroll.referral_note.amount,
                    notes: ''
                }
            };
        }

        return data;
      });
    }

    parseReferralNotes = (referralnotes) => {
      const NF = -1;
      return referralnotes.filter(
        td => this.props.memory.referral_notes.map(t => t.id).indexOf(td.id) > NF
      ).map(r => ({ value: r.id, label: `ID: ${r.id}` }));
    }

    __isEditing = () => !!this.props.routeParams.id;

    __getProjects = () => {
        getProjects().then(response => {
            this.setState({
                projects: response
            });
        });
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    getCatalogs = (isEditing = false, props) => {
        getTaxDocuments(this.state.formValues.project_id).then(taxdocuments => {
          if (isEditing) {
            const formValues = this.state.formValues;

            formValues.invoices = this.parseTaxDocuments([
              ...taxdocuments,
              ...props.memory.tax_documents
            ]);

            this.setState({ taxdocuments, formValues });
          } else {
            this.setState({ taxdocuments });
          }
        });

        getReferralNotes(this.state.formValues.project_id).then(referralnotes => {
            if (isEditing) {
              const formValues = this.state.formValues;

              formValues.referral = this.parseReferralNotes([
                ...referralnotes,
                ...props.memory.referral_notes
              ]);

              this.setState({ referralnotes, formValues });
            } else {
              this.setState({ referralnotes });
            }
        });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }

            if (field === 'project_id') {
                this.setState({
                    taxdocuments: [],
                    referralnotes: []
                }, this.getCatalogs);
            }
        } else {
            delete formValues[field];
        }

        if (field === 'project_id') {
            formValues.purchase_order_id = '';
            this.setState({
                formValues,
                orders: []
            });
        } else {
            this.setState({ formValues });
        }
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return [
            'project_id',
            'pdf_url',
            'xml_url',
            'initial_date',
            'final_date',
            'percentage_fees',
            'percentage_vat'
        ];
    }

    /* eslint-disable */
    inputsRegex = () => {
      return [
          /^[0-9]+$/,
          /^.*$/,
          /^.*$/,
          /^.*$/,
          /^.*$/,
          /^[0-9.]+$/,
          /^[0-9.]+$/
      ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        const ZERO = 0;
        if (!this.inputsRequiredFilled()) {
            const { formValues } = this.state;

            if (formValues.pdf_url === '') {
                this.props.notification('El pdf es requerido');
            } else if (formValues.xml_url === '') {
                this.props.notification('El xml es requerido');
            }

            return this.setState({ invalidForm: true });
        }

        const initial_date = moment(this.state.formValues.initial_date);
        const final_date = moment(this.state.formValues.final_date);

        if (initial_date.diff(final_date) >= ZERO) {
            this.props.notification('La fecha final debe ser mayor a la inicial');
            return;
        }



        const { formValues } = this.state;
        const form = {
            initial_date: formValues.initial_date,
            final_date: formValues.final_date,
            percentage_vat: parseFloat(formValues.percentage_vat),
            percentage_fees: parseFloat(formValues.percentage_fees),
            fees: {
                pdf_url: formValues.pdf_url,
                xml_url: formValues.xml_url,
                observations: ''
            },
            project_id: formValues.project_id,
            status: 0,
            tax_documents: formValues.invoices.map(d => d.value),
            referral_notes: formValues.referral.map(d => d.value),
            payrolls: formValues.payrolls
        };

        if (this.__isEditing()) {
            this.props.updateMemory({ ...form, id: this.props.memory.id });
        } else {
            this.props.createMemory(form);
        }

    }

    getOptionsProjects = () => {
        return this.state.projects.map(r => ({ value: r.id, label: r.name }));
    }

    getOptionsTax = () => {
        return this.state
            .taxdocuments.map(r => ({ value: r.id, label: `ID: ${r.id} Folio: ${r.folio}` }));
    }

    getOptionsReferral = () => {
        return this.state
            .referralnotes.map(r => ({ value: r.id, label: `ID: ${r.id}` }));
    }

    onUploadTax = (type, url) => {
        const formValues = this.state.formValues;

        if (type === 'pdf') {
            formValues.pdf_url = url;
        } else {
            formValues.xml_url = url;
        }

        this.setState({ formValues });
    }

    toggle = modalIsOpen => {
        this.setState({
            payment_voucher: {},
            modalIsOpen: modalIsOpen.target.value
        });
    }

    onChangeCheckbox = e => {
        const value = e.target.checked;
        const payroll = this.state.payroll;

        payroll.selected = value;

        this.setState({ payroll });
    }

    onSavePayroll = () => {
        var data = { id: -1 };
        const { payroll, formValues } = this.state;

        if (payroll.selected) {
            if (payroll.pdf_url === '' || !payroll.pdf_url) {
                this.props.notification('El pdf es requerido');
                return;
            }

            if (payroll.xml_url === '' || !payroll.xml_url) {
                this.props.notification('El xml es requerido');
                return;
            }
            data = {
                ...data,
                tax_document: {
                    pdf_url: payroll.pdf_url,
                    xml_url: payroll.xml_url,
                    observations: ''
                }
            };
        } else {
            if (payroll.pdf_url === '' || !payroll.pdf_url) {
                this.props.notification('El pdf es requerido');
                return;
            }

            if (formValues.amount === '' || isNaN(parseFloat(formValues.amount))) {
                this.props.notification('Debes agregar un monto');
                return;
            }

            data = {
                ...data,
                referral_note: {
                    pdf_url: payroll.pdf_url,
                    amount: parseFloat(formValues.amount),
                    notes: ''
                }
            };
        }

        formValues.amount = '';
        formValues.payrolls.push(data);

        this.setState({
            modalIsOpen: false,
            payroll: {},
            formValues
        });
    }

    showModal = () => {
        const { errorMessages, formValues, formRequested } = this.state;

        if (!this.state.modalIsOpen) return null;

        return (
            <Modal isOpen={true} toggle={this.toggle} className={''}>
                <ModalHeader toggle={this.toggle}>Comprobante de pago</ModalHeader>
                <ModalBody>
                    <div className="form-row col-12">
                        <label>
                            <input
                                type="checkbox"
                                value={this.state.payroll.selected}
                                checked={this.state.payroll.selected == true}
                                onChange={this.onChangeCheckbox}
                            /> ¿ Es factura ?
                        </label>
                    </div>
                    {
                        this.state.payroll.selected ? (
                            <div>
                                <div className="form-row">
                                    <UploadFile
                                        onFileUploaded={this.onUploadPayroll.bind(this, 'pdf')}
                                        previousName={this.state.payroll.pdf_url}
                                        label="Sube tu pdf"
                                        types="image/x-png,image/gif,image/jpeg,application/pdf"
                                    />
                                    <UploadFile
                                        onFileUploaded={this.onUploadPayroll.bind(this, 'xml')}
                                        previousName={this.state.payroll.xml_url}
                                        label="Sube tu xml"
                                        types="text/xml"
                                    />
                                </div>
                            </div>
                        ) : (
                            <div>
                                <div className="form-row">
                                    <Input
                                        hasLabel
                                        label="Monto"
                                        htmlFor="amount"
                                        requested={formRequested}
                                        id="amount"
                                        onChange={this.onChange.bind(this, 'amount')}
                                        onBlur={this.onBlur.bind(this, 'amount')}
                                        value={formValues['amount']}
                                        placeholder="Monto"
                                        aria="amount"
                                        hasError={!!errorMessages['amount']}
                                        invalidText={errorMessages['amount']}
                                    />
                                    <UploadFile
                                        onFileUploaded={this.onUploadPayroll.bind(this, 'pdf')}
                                        previousName={this.state.payroll.pdf_url}
                                        label="Sube tu nota de remisión"
                                        types="image/x-png,image/gif,image/jpeg,application/pdf"
                                    />
                                </div>
                            </div>
                        )
                    }
                </ModalBody>
                <ModalFooter>
                    <ButtonX color="primary" onClick={this.onSavePayroll}>Guardar</ButtonX>{' '}
                    <ButtonX color="secondary" onClick={this.toggle}>Cancel</ButtonX>
                </ModalFooter>
            </Modal>
        );
    }

    toggleDropdown = dropdown => {
        if (dropdown !== this.state.dropdownOpen) {
            this.setState({
                dropdownOpen: dropdown
            });
        } else {
            this.setState({
                dropdownOpen: null
            });
        }
    }

    downloadFile = url => {
        this.props.downloadFile(url, false);
    }

    _delete(index) {
        const ZERO = 0;
        const ONE = 1;
        const formValues = this.state.formValues;

        formValues.payrolls = [
            ...formValues.payrolls.slice(ZERO, index),
            ...formValues.payrolls.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    onUpload = (type, url) => {
        const formValues = this.state.formValues;

        if (type === 'pdf') {
            formValues.pdf_url = url;
        } else formValues.xml_url = url;

        this.setState({ formValues });
    }

    onUploadPayroll = (type, url) => {
        const payroll = this.state.payroll;

        if (payroll.selected) {
            // es factura
            if (type === 'pdf') payroll.pdf_url = url;
            else payroll.xml_url = url;
        } else {
            // noes factura
            payroll.pdf_url = url;
        }

        this.setState({ payroll });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Proyecto"
                        htmlFor="project_id"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'project_id')}
                        onBlur={this.onBlur.bind(this, 'project_id')}
                        id="project_id"
                        value={formValues['project_id']}
                        placeholder="selecciona un proyecto..."
                        options={this.getOptionsProjects()}
                        hasError={!!errorMessages['project_id']}
                        invalidText={errorMessages['project_id']}
                        required
                    />
                </div>
                <div>
                    <label>Sube tu factura de honorarios</label>
                    <div className="form-row">
                        <UploadFile
                            onFileUploaded={this.onUpload.bind(this, 'pdf')}
                            previousName={this.state.formValues.pdf_url}
                            label="Sube tu pdf"
                            types="application/pdf"
                        />
                        <UploadFile
                            onFileUploaded={this.onUpload.bind(this, 'xml')}
                            previousName={this.state.formValues.xml_url}
                            label="Sube tu xml"
                            types="text/xml"
                        />
                    </div>
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Fecha de inicio"
                        htmlFor="initial_date"
                        icon="calendar"
                        requested={formRequested}
                        id="initial_date"
                        type="date"
                        onChange={this.onChange.bind(this, 'initial_date')}
                        onBlur={this.onBlur.bind(this, 'initial_date')}
                        value={formValues['initial_date']}
                        placeholder="Cual es la fecha de inicio"
                        aria="initial_date"
                        hasError={!!errorMessages['initial_date']}
                        invalidText={errorMessages['initial_date']}
                        isGroup
                        hasIcon
                        required
                    />
                    <Input
                        hasLabel
                        label="Fecha de fin"
                        htmlFor="final_date"
                        icon="calendar"
                        type="date"
                        requested={formRequested}
                        id="final_date"
                        onChange={this.onChange.bind(this, 'final_date')}
                        onBlur={this.onBlur.bind(this, 'final_date')}
                        value={formValues['final_date']}
                        placeholder="Cual es la fecha de fin"
                        aria="final_date"
                        hasError={!!errorMessages['final_date']}
                        invalidText={errorMessages['final_date']}
                        isGroup
                        required
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Honorarios"
                        htmlFor="percentage_fees"
                        icon="percentage"
                        id="percentage_fees"
                        hasError={!!errorMessages['percentage_fees']}
                        invalidText={errorMessages['percentage_fees']}
                        requested={formRequested}
                        placeholder="porcentaje honorarios..."
                        aria="percentage_fees"
                        type="number"
                        required
                        value={formValues['percentage_fees']}
                        onChange={this.onChange.bind(this, 'percentage_fees')}
                        onBlur={this.onBlur.bind(this, 'percentage_fees')}
                        isGroup
                    />
                    <Input
                        hasLabel
                        label="IVA"
                        htmlFor="percentage_vat"
                        icon="percentage"
                        id="percentage_vat"
                        hasError={!!errorMessages['percentage_vat']}
                        invalidText={errorMessages['percentage_vat']}
                        requested={formRequested}
                        placeholder="porcentaje IVA..."
                        aria="percentage_vat"
                        type="number"
                        value={formValues['percentage_vat']}
                        onChange={this.onChange.bind(this, 'percentage_vat')}
                        onBlur={this.onBlur.bind(this, 'percentage_vat')}
                        isGroup
                        required
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Facturas"
                        htmlFor="invoices"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'invoices')}
                        onBlur={this.onBlur.bind(this, 'invoices')}
                        id="invoices"
                        multi
                        value={formValues['invoices']}
                        placeholder="selecciona las facturas..."
                        options={this.getOptionsTax()}
                        hasError={!!errorMessages['invoices']}
                        invalidText={errorMessages['invoices']}
                    />
                    <Select
                        hasLabel
                        label="Notas de remision"
                        htmlFor="referral"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'referral')}
                        onBlur={this.onBlur.bind(this, 'referral')}
                        id="referral"
                        multi
                        value={formValues['referral']}
                        placeholder="selecciona las notas de remisión..."
                        options={this.getOptionsReferral()}
                        hasError={!!errorMessages['referral']}
                        invalidText={errorMessages['referral']}
                    />
                </div>
                <div className="col-xs-12 col-sm-6" >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <h5 style={{ margin: 0, marginRight: 20 }}>Rayas</h5>
                        <span className="fa fa-plus-circle" onClick={() => {
                            this.setState({ modalIsOpen: true });
                        }} />
                    </div>
                    <div style={{ display: 'flex' }}>
                        {
                            this.state.formValues.payrolls.map((document, index) => (
                                <Dropdown
                                    isOpen={this.state.dropdownOpen === `tax-${index}`}
                                    toggle={() => this.toggleDropdown(`tax-${index}`)}
                                    style={{ margin: 4 }}
                                    key={index}
                                >
                                    <DropdownToggle
                                        tag="span"
                                        data-toggle="dropdown"
                                    >
                                        <img src="/assets/images/voucher.png" width="70" />
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem onClick={() => {
                                            if (document.pdf_url === '' || !document.pdf_url) {
                                                const message = 'No existe archivo a descargar';
                                                this.props.notification(message);
                                            } else {
                                                this.downloadFile(document.pdf_url);
                                            }
                                        }}>Descargar comprobante</DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem onClick={() => {
                                            this._delete(index);
                                        }}>Eliminar</DropdownItem>
                                    </DropdownMenu>
                                </Dropdown>
                            ))
                        }
                    </div>
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
                {this.showModal()}
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateMemory: PropTypes.func,
    notification: PropTypes.func,
    downloadFile: PropTypes.func,
    memory: PropTypes.object,
    cancelForm: PropTypes.func,
    createMemory: PropTypes.func
};

const mapStateToProps = ({ memoryForm }) => ({
    ...memoryForm
});

const mapDispatchToProps = {
    goEdit,
    createMemory,
    getProfile,
    downloadFile,
    notification,
    updateMemory,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
