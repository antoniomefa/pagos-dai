
export const data = [
    {
        id: 1, name: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 1,
        folio: 'F-S00212', serie: '1',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 2, name: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 12,
        folio: 'F-S002567', serie: '1',
        provider: 'Luis Angel Sa de CV', status: 'iPendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 3, name: '209176456', emisor: 'Barragan', receptor: 'Luis', oc: 14,
        folio: 'F-S00234', serie: '1',
        provider: 'Marco SA de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 4, name: '209897111', emisor: 'Barragan', receptor: 'Luis', oc: 15,
        folio: 'F-S00243', serie: '1',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 5, name: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 21,
        folio: 'F-S002424', serie: '1',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 6, name: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 19,
        folio: 'F-S0022345', serie: '1',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 7, name: '209176456', emisor: 'Barragan', receptor: 'Luis', oc: 11,
        folio: 'F-S002', serie: '1',
        provider: 'Marco SA de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 8, name: '209897111', emisor: 'Barragan', receptor: 'Luis', oc: 122,
        folio: 'F-S002', serie: '1',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 9, name: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 144,
        folio: 'F-S002', serie: '1',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 10, name: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 122,
        folio: 'F-S002', serie: '1',
        provider: 'Luis Angel Sa de CV'
    }
];

export const data2 = [
    {
        id: 11, name: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 212,
        folio: 'F-S002343', serie: '1',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 12, name: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 221,
        folio: 'F-S0025456', serie: '1',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 13, name: 'Bosques acueducto', emisor: 'Barragan', receptor: 'Luis', oc: 115,
        folio: 'F-S002234', serie: '1',
        provider: 'Marco SA de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 14, name: 'Colinas del bosque', emisor: 'Barragan', receptor: 'Luis', oc: 191,
        folio: 'F-S00247', serie: '1',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 15, name: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 241,
        folio: 'F-S002678', serie: '1',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 16, name: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 341,
        folio: 'F-S002768', serie: '1',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 17, name: 'Bosques acueducto', emisor: 'Barragan', receptor: 'Luis', oc: 391,
        folio: 'F-S00223', serie: '1',
        provider: 'Marco SA de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 18, name: 'Colinas del bosque', emisor: 'Barragan', receptor: 'Luis', oc: 971,
        folio: 'F-S00298765', serie: '1',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 19, name: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 541,
        folio: 'F-S00224', serie: '1',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    },
    {
        id: 20, name: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 111,
        folio: 'F-S002', serie: '1',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', total: 12000, customerType: 'interno'
    }
];
