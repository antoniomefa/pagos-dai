import {
    ON_GET_MEMORIES_SUCCESS,
    ON_FETCH_MEMORIES,
    ON_SET_FILTERS,
    ON_DELETE_MEMORY,
    ON_REFRESH
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { sendNotification } from 'actions';
import { downloadB64, downloadB64AsZIP } from 'helpers';

export function downloadFile(url) {
    return () => {
        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                downloadB64(mime, name, data);
            })
            .catch(() => {
                //
            });


    };
}

export function downloadZip(urls, zipName) {
    return () => {
        const ONE = 1;
        var promises = [];

        urls.forEach((url) => {
            const query = { url };
            promises.push(Service(query, 'api/files', 'get'));
        });

        Promise.all(promises).then(values => {
            const data = values.map((v, index) => {
                const name = urls[index].split('/')[ONE];
                const mime = name.split('.')[ONE];

                return {
                    ...v,
                    name,
                    mime
                };
            });

            downloadB64AsZIP(data, zipName);
        });
    };
}

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_MEMORIES });

        Service(query, 'api/memories', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_MEMORIES_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/memories/${id}`));
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/memories/create'));
    };
}

export const deleteMemories = (memories, withBack = false) => dispatch => {
    memories.forEach(id => {
        Service({}, `api/memories/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_MEMORY, id: data.id });
                if (withBack) {
                    dispatch(push('/memories'));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};

export const validateMemory = (id) => dispatch => {
  Service({}, `api/memories/${id}/validate`, 'put')
      .then(() => {
        dispatch({ type: ON_REFRESH });
        dispatch(sendNotification({
          message: 'Memoria validada',
          level: 'success'
        }));
      })
      .catch(() => {
          // error al eliminar al usuario
          dispatch(notification('Error al validar la memoria'));
      });
};

export const distributeMemory = (id) => dispatch => {
  Service({}, `api/memories/${id}/distribute`, 'put')
      .then(() => {
        dispatch({ type: ON_REFRESH });
        dispatch(sendNotification({
          message: 'Memoria distribuida',
          level: 'success'
        }));
      })
      .catch(() => {
        dispatch(notification('Error al distribuir la memoria'));
      });
};
