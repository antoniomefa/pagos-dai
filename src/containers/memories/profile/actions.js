import { ON_GET_MEMORY } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/memories/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_MEMORY, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Memoria no encontrada',
                color: 'danger',
                alias: 'get-memory',
                time: 3
            }));
            dispatch(push('/memories'));
        });
};
