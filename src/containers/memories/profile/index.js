import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import {
  deleteMemories,
  downloadFile,
  notification,
  downloadZip
} from '../actions';
import moment from 'moment';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteMemories([this.props.routeParams.id], true);
    }

    download = () => {
        const found = this.props.memory;

        if (found && found.fees.pdf_url !== '') this.props.downloadFile(found.fees.pdf_url);
        else this.props.notification('No existe pdf para descargar.');
        if (found && found.fees.xml_url !== '') this.props.downloadFile(found.fees.xml_url);
        else this.props.notification('No existe xml para descargar.');
    }

    downloadAll = () => {
        const found = this.props.memory;

        if (!found) return this.props.notification('No se encontro memoria.');

        const format = 'DD-MMMM-YYYY';

        const initialDate = moment(found.initial_date).format(format);
        const finalDate = moment(found.final_date).format(format);

        const memoryName = `memoria-${initialDate}-al-${finalDate}`;

        const urls = [
            found.fees.pdf_url,
            found.fees.xml_url,
            ...found.referral_notes.map(r => r.pdf_url),
            ...found.tax_documents.map(r => r.pdf_url),
            ...found.tax_documents.map(r => r.xml_url)
        ];

        found.payrolls.forEach(item => {
            if (item.tax_document) {
                urls.push(item.tax_document.pdf_url);
                urls.push(item.tax_document.xml_url);
            } else {
                urls.push(item.referral_note.pdf_url);
            }
        });

        this.props.downloadZip(urls, memoryName);

    }

    onUpdate = () => {
        this.props.goEdit();
    }

    render() {
        const { memory } = this.props;
        const format = 'DD [de] MMMM [de] YYYY';

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h3>Memoria ID: {memory.id}</h3>
                    </div>
                    <div className="__card__body">
                        <p><strong>% honorarios:</strong>  {memory.percentage_fees}</p>
                        <p><strong>% IVA:</strong>  {memory.percentage_vat}</p>
                        <p><strong>fecha inicial:</strong>  {
                            moment(memory.initial_date).format(format)
                        }</p>
                        <p><strong>fecha final:</strong>  {
                            moment(memory.final_date).format(format)
                        }</p>
                        <p><strong>proyecto:</strong>  {
                            memory.project ? memory.project.name : 'Sin proyecto'
                        }</p>
                        <p><strong>Estatus:</strong>  {
                            memory.status
                        }</p>
                        <p><strong>cantidad de documentos fiscales:</strong>  {
                            memory.tax_documents ? memory.tax_documents.length : '0'
                        }</p>
                        <p><strong>cantidad de notas de remisión:</strong>  {
                            memory.referral_notes ? memory.referral_notes.length : '0'
                        }</p>
                        <p><strong>cantidad de rayas:</strong>  {
                            memory.payrolls ? memory.payrolls.length : '0'
                        }</p>
                        {
                            memory.fees && (
                                <div>
                                    <h5>Factura de honorarios</h5>
                                    <p><strong>numero de certificado:</strong>  {
                                        memory.fees.certificate_number
                                    }</p>
                                    <p><strong>moneda:</strong>  {
                                        memory.fees.currency
                                    }</p>
                                    <p><strong>emisor:</strong>  {
                                        memory.fees.emitter_name
                                    }</p>
                                    <p><strong>emisor rfc:</strong>  {
                                        memory.fees.emitter_rfc
                                    }</p>
                                    <p><strong>Folio:</strong>  {
                                        memory.fees.folio
                                    }</p>
                                    <p><strong>Metodo de pago:</strong>  {
                                        memory.fees.payment_method
                                    }</p>
                                    <p><strong>Estatus:</strong>  {
                                        memory.fees.status
                                    }</p>
                                    <p><strong>Subtotal:</strong>  {
                                        memory.fees.subtotal
                                    }</p>
                                    <p><strong>Total:</strong>  {
                                        memory.fees.total
                                    }</p>
                                </div>
                            )
                        }
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Descargar"
                            icon="download"
                            isIcon
                            type="info"
                            outline
                            onButtonPress={this.download}
                            permission={'download'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Descargar zip"
                            icon="file-archive"
                            isIcon
                            type="info"
                            outline
                            onButtonPress={this.downloadAll}
                            permission={'download'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Eliminar"
                            icon="trash-o"
                            isIcon
                            type="danger"
                            outline
                            onButtonPress={this.onDelete}
                            permission={'delete'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    downloadFile: PropTypes.func,
    downloadZip: PropTypes.func,
    notification: PropTypes.func,
    memory: PropTypes.object,
    deleteMemories: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ memoryProfile }) => ({
    ...memoryProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    downloadFile,
    notification,
    deleteMemories,
    downloadZip
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
