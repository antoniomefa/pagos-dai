import { ON_GET_MEMORY } from './types';

const initialState = {
    memory: {}
};

export default function memoryProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_MEMORY:
            return {
                ...state,
                memory: action.data
            };
        default:
            return { ...state };
    }
}
