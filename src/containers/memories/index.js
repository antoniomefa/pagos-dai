import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import {
  getData,
  addQuery,
  setFilters,
  goProfile,
  goCreate,
  deleteMemories,
  validateMemory,
  distributeMemory,
  downloadFile,
  notification,
  downloadZip
} from './actions';

/* CATALOGS */
import {
    getMemoryStatus,
    getProjects
} from 'utils/catalogs';

class Memories extends Component {
    constructor(props) {
        super(props);

        this.state = {
            statusCatalog: [],
            projectsCatalog: []
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getStatusCatalog();
        this.getProjectsCatalog();
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.refresh) {
            this.props.getData(this.props.query);
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: '% iva', field: 'percentage_vat'},
            {name: '% honorarios', field: 'percentage_fees'},
            {name: 'Proyecto', field: 'project.name'},
            {name: 'Fecha inicial', field: 'initial_date', isDate: true},
            {name: 'Fecha final', field: 'final_date', isDate: true},
            {name: 'Estatus', field: 'status'}
        ];
    }

    __getDataRows = () => this.props.rows;

    getStatusCatalog = () => {
        getMemoryStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    getProjectsCatalog = () => {
        getProjects().then(projectsCatalog => this.setState({ projectsCatalog }));
    }

    __getFilters() {
        const { statusCatalog } = this.state;
        const { projectsCatalog } = this.state;

        var statusOptions = statusCatalog.map(s => ({ label: s, value: s}));
        var projectsOptions = projectsCatalog.map(p => ({ label: p.name, value: p.id }));
        return [
            { name: 'proyecto', field: 'project_id', options: projectsOptions },
            { name: 'estatus', field: 'status', options: statusOptions }
        ];
    }

    download = id => {
        const found = this.__getDataRows().find(d => d.id == id);

        if (found && found.fees.pdf_url !== '') this.props.downloadFile(found.fees.pdf_url);
        else this.props.notification('No existe pdf para descargar.');
        if (found && found.fees.xml_url !== '') this.props.downloadFile(found.fees.xml_url);
        else this.props.notification('No existe xml para descargar.');
    }

    downloadAll = id => {
        const found = this.__getDataRows().find(d => d.id == id);

        if (!found) return this.props.notification('No se encontro memoria.');

        const format = 'DD-MMMM-YYYY';

        const initialDate = moment(found.initial_date).format(format);
        const finalDate = moment(found.final_date).format(format);

        const memoryName = `memoria-${initialDate}-al-${finalDate}`;

        const urls = [
            found.fees.pdf_url,
            found.fees.xml_url,
            ...found.referral_notes.map(r => r.pdf_url),
            ...found.tax_documents.map(r => r.pdf_url),
            ...found.tax_documents.map(r => r.xml_url)
        ];

        found.payrolls.forEach(item => {
            if (item.tax_document) {
                urls.push(item.tax_document.pdf_url);
                urls.push(item.tax_document.xml_url);
            } else {
                urls.push(item.referral_note.pdf_url);
            }
        });

        this.props.downloadZip(urls, memoryName);

    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addUser = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: ids => this.props.deleteMemories(ids), tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: id => this.props.deleteMemories([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Distribuir',
                permission: 'distribute',
                action: id => this.props.distributeMemory(id)
            },
            {
                icon: 'check',
                style: 'btn btn-success',
                tooltip: 'Validar',
                permission: 'change-status',
                action: id => this.props.validateMemory(id)
            },
            {
                icon: 'download',
                style: 'btn btn-info',
                tooltip: 'Descargar',
                permission: 'download',
                action: id => this.download(id)
            },
            {
                icon: 'file-archive',
                style: 'btn btn-info',
                tooltip: 'Descargar',
                permission: 'download',
                action: id => this.downloadAll(id)
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

Memories.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    goCreate: PropTypes.func,
    setFilters: PropTypes.func,
    validateMemory: PropTypes.func,
    distributeMemory: PropTypes.func,
    downloadZip: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    deleteMemories: PropTypes.func,
    goProfile: PropTypes.func
};

Memories.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ memories }) => ({
    ...memories
});

const mapDispatchToProps = {
    getData,
    goCreate,
    addQuery,
    setFilters,
    validateMemory,
    distributeMemory,
    downloadFile,
    downloadZip,
    notification,
    deleteMemories,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Memories);
