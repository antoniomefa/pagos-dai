
export const ON_FETCH_MEMORIES = 'App/Memories/ON_FETCH_MEMORIES';
export const ON_GET_MEMORIES_SUCCESS = 'App/Memories/ON_GET_MEMORIES_SUCCESS';
export const ON_GET_MEMORIES_FAIL = 'App/Memories/ON_GET_MEMORIES_FAIL';
export const ON_SET_FILTERS = 'App/Memories/ON_SET_FILTERS';
export const ON_DELETE_MEMORY = 'App/Memories/ON_DELETE_MEMORY';
export const ON_REFRESH = 'App/Memories/ON_REFRESH';
