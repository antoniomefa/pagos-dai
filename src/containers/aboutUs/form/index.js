import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DraftEditor from 'components/draftEditor';
import Button from 'components/button';
import { getData, setLoaded, updateData } from './actions';

class AboutUs extends Component {

    constructor(props) {
        super(props);

        this.state = {
            mission: props.mission,
            vision: props.vision
        };
    }

    componentDidMount() {
        this.props.getData();
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.loaded) {
            this.setState({
                mission: nextProps.mission,
                vision: nextProps.vision
            });

            this.props.setLoaded(false);
        }
    }

    onButtonPress = () => {
        this.props.updateData(this.state.mission, this.state.vision);
    }

    changeEditorContent = (field, content) => {
        const state = this.state;

        state[field] = content;

        this.setState(state);
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <DraftEditor
                            label="Misión"
                            isEditing
                            content={this.state.mission}
                            changeEditorContent={this.changeEditorContent.bind(this, 'mission')}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <DraftEditor
                            label="Visión"
                            isEditing
                            content={this.state.vision}
                            changeEditorContent={this.changeEditorContent.bind(this, 'vision')}
                        />
                        <div style={{ display: 'flex', justifyContent: 'flex-end', padding: 10 }}>
                            <button
                                className={'btn btn-light'}
                                onClick={() => {}}
                            >
                                {'Cancelar'}
                            </button>
                            <Button
                                text="Guardar"
                                type="primary"
                                outline
                                onButtonPress={this.onButtonPress}
                                permission={'update'}
                                permissions={this.props.permisos}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

AboutUs.propTypes = {
    mission: PropTypes.string,
    vision: PropTypes.string,
    getData: PropTypes.func,
    setLoaded: PropTypes.func,
    loaded: PropTypes.bool,
    updateData: PropTypes.func,
    permisos: PropTypes.array
};

AboutUs.defaultProps = {

};

const mapStateToProps = ({ aboutForm }) => ({
    ...aboutForm
});

const mapDispatchToProps = {
    getData, setLoaded, updateData
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);
