import { ON_GET_ABOUT, ON_SET_LOADED } from './types';
import Service from 'helpers/service';
import { push } from 'react-router-redux';
import { sendNotification } from 'actions';

export const getData = () => dispatch => {
    Service({}, 'about', 'get')
        .then(({ data }) => {
            dispatch({
                type: ON_GET_ABOUT,
                mission: data.mission,
                vision: data.vision
            });
        })
        .catch(() => {
            //
        });
};

const saveMission = value => dispatch => {
    Service(JSON.stringify({ value }), 'about/mision', 'put')
        .then(() => {
            dispatch(sendNotification({
                message: 'Misión actualizada',
                level: 'success'
            }));
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'Error al actualizar la misión',
                level: 'warning'
            }));
        });
};

const saveVision = value => dispatch => {
    Service(JSON.stringify({ value }), 'about/vision', 'put')
        .then(() => {
            dispatch(push('/about-us'));
            dispatch(sendNotification({
                message: 'Visión actualizada',
                level: 'success'
            }));
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'Error al actualizar la visión',
                level: 'warning'
            }));
        });
};

export const updateData = (mission, vision) => dispatch => {
    dispatch(saveMission(mission));
    dispatch(saveVision(vision));
};

export const setLoaded = value => ({ type: ON_SET_LOADED, value });
