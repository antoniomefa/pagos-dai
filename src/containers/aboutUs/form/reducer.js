import { ON_GET_ABOUT, ON_SET_LOADED } from './types';

const initialState = {
    mission: null,
    vision: null,
    loaded: false
};

export default function aboutUsReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_ABOUT:
            return {
                ...state,
                mission: action.mission,
                vision: action.vision,
                loaded: true
            };
        case ON_SET_LOADED:
            return {
                ...state,
                loaded: action.value
            };
        default:
            return {
                ...state
            };
    }
}
