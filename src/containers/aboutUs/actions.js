import { ON_GET_ABOUT } from './types';
import { push } from 'react-router-redux';
import Service from 'helpers/service';

export function go(to) {
    return dispatch => {
        dispatch(push(`/about-us/${to}`));
    };
}

export const getData = () => dispatch => {
    Service({}, 'about', 'get')
        .then(({ data }) => {
            dispatch({
                type: ON_GET_ABOUT,
                mission: data.mission,
                vision: data.vision
            });
        })
        .catch(() => {
            //
        });
};
