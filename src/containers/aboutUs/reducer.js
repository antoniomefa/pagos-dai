import { ON_GET_ABOUT } from './types';

const initialState = {
    mission: '',
    vision: ''
};

export default function aboutUsReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_ABOUT:
            return {
                ...state,
                mission: action.mission,
                vision: action.vision
            };
        default:
            return {
                ...state
            };
    }
}
