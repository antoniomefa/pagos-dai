import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';
import Button from 'components/button';
import { go, getData } from './actions';

class AboutUs extends Component {

    componentDidMount() {
        this.props.getData();
    }

    renderContent = (html) => {
        return renderHTML(html);
    }

    render() {
        return (
            <div className="container">
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button
                        text="Editar"
                        type="primary"
                        outline
                        onButtonPress={() => this.props.go('edit')}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <div>
                    <p><small>misión</small></p>
                    {this.renderContent(this.props.mission)}
                </div>
                <div>
                    <p><small>visión</small></p>
                    {this.renderContent(this.props.vision)}
                </div>
            </div>
        );
    }
}

AboutUs.propTypes = {
    mission: PropTypes.string,
    vision: PropTypes.string,
    permisos: PropTypes.array,
    getData: PropTypes.func,
    go: PropTypes.func
};

AboutUs.defaultProps = {

};

const mapStateToProps = ({ aboutUs }) => ({
    ...aboutUs
});

const mapDispatchToProps = {
    go,
    getData
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);
