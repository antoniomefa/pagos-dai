import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Select from 'components/select';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, createUser, updateUser, getProfile, cancelForm } from './actions';
import { getRoles } from 'utils/catalogs';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            formValues: {
                email: '',
                fullname: '',
                pass1: '',
                rol: null,
                ext: ''
            },
            roles: []
        };
    }

    componentDidMount() {
        this.__getRoles();

        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.user.id) {

            formValues.email = nextProps.user.email;
            formValues.fullname = nextProps.user.name;
            formValues.ext = nextProps.user.ext;
            formValues.rol = nextProps.user.roles.map(r => ({ value: r.id, label: r.name }));
            formValues.pass1 = '';

            this.setState({
                formValues
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    __getRoles = () => {
        getRoles().then(response => {
            this.setState({
                roles: response
            });
        });
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        if (this.__isEditing()) {
            return ['fullname', 'email', 'rol'];
        }
        return ['fullname', 'email', 'rol', 'pass1'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        if (this.__isEditing()) {
            return [
                /^[a-zA-Z ]+$/,
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                /^.*$/
            ];
        }
        return [
            /^[a-zA-Z ]+$/,
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            /^.*$/,
            /^.*$/
        ];
    }
    /* eslint-enable */

    checkPhone = () => {
        var exp = /^[0-9]{8}(?:[0-9]{2})?$/;
        const { formValues } = this.state;

        if (formValues.ext == '') return true;

        return exp.test(formValues.ext);
    }

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled() || !this.checkPhone()) {
            return this.setState({ invalidForm: true });
        }

        const form = {
            name: this.state.formValues['fullname'],
            email: this.state.formValues['email'],
            password: this.state.formValues['pass1'],
            ext: this.state.formValues['ext'],
            roles: this.state.formValues['rol'].map(r => r.value)
        };

        if (this.__isEditing()) {
            this.props.updateUser({ ...form, id: this.props.user.id });
        } else {
            this.props.createUser(form);
        }

    }

    getOptionsRol = () => {
        return this.state.roles.map(r => ({ value: r.id, label: r.name }));
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Nombre completo"
                        htmlFor="fullname"
                        icon="user"
                        requested={formRequested}
                        id="fullname"
                        onChange={this.onChange.bind(this, 'fullname')}
                        onBlur={this.onBlur.bind(this, 'fullname')}
                        value={formValues['fullname']}
                        placeholder="nombre completo..."
                        aria="fullname"
                        hasError={!!errorMessages['fullname']}
                        invalidText={errorMessages['fullname']}
                        required
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        type="email"
                        label="Correo electronico"
                        htmlFor="email"
                        icon="envelope-o"
                        requested={formRequested}
                        onChange={this.onChange.bind(this, 'email')}
                        onBlur={this.onBlur.bind(this, 'email')}
                        id="email"
                        value={formValues['email']}
                        placeholder="correo electoronico..."
                        aria="email"
                        hasError={!!errorMessages['email']}
                        invalidText={errorMessages['email']}
                        required
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Rol"
                        htmlFor="rol"
                        icon="gear"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'rol')}
                        onBlur={this.onBlur.bind(this, 'rol')}
                        id="rol"
                        multi
                        value={formValues['rol']}
                        placeholder="selecciona un rol..."
                        options={this.getOptionsRol()}
                        hasError={!!errorMessages['rol']}
                        invalidText={errorMessages['rol']}
                        required
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        label="Contraseña"
                        htmlFor="pass1"
                        icon="key"
                        id="pass1"
                        hasError={!!errorMessages['pass1']}
                        invalidText={errorMessages['pass1']}
                        requested={formRequested}
                        placeholder="contraseña..."
                        aria="pass1"
                        type="password"
                        value={formValues['pass1']}
                        onChange={this.onChange.bind(this, 'pass1')}
                        onBlur={this.onBlur.bind(this, 'pass1')}
                        required={!this.__isEditing()}
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Teléfono"
                        htmlFor="ext"
                        icon="phone"
                        type="text"
                        hasError={!this.checkPhone()}
                        required={!this.checkPhone()}
                        invalidText={'El teléfono tiene un formato invalido'}
                        requested={formRequested}
                        id="ext"
                        onChange={this.onChange.bind(this, 'ext')}
                        onBlur={this.onBlur.bind(this, 'ext')}
                        value={formValues['ext']}
                        placeholder="Número telefonico..."
                        aria="ext"
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateUser: PropTypes.func,
    user: PropTypes.object,
    cancelForm: PropTypes.func,
    createUser: PropTypes.func
};

const mapStateToProps = ({ userFormReducer }) => ({
    ...userFormReducer
});

const mapDispatchToProps = {
    goEdit,
    createUser,
    getProfile,
    updateUser,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
