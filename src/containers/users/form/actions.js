import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_USER } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createUser = data => dispatch => {
    Service(JSON.stringify(data), 'api/users', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Usuario creado correctamente',
                color: 'primary',
                alias: 'created-user',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear el usuario',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/users/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_USER, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Usuario no encontrado',
                color: 'danger',
                alias: 'get-user',
                time: 3
            }));
            dispatch(push('/users'));
        });
};

export const updateUser = data => dispatch => {
    Service(JSON.stringify(data), 'api/users', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
