import { ON_GET_USERS_SUCCESS, ON_FETCH_USERS, ON_SET_FILTERS, ON_DELETE_USER } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_USERS });

        Service(query, 'api/users', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_USERS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/users/${id}`));
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/users/create'));
    };
}

export const deleteUsers = (users, withBack = false) => dispatch => {
    users.forEach(id => {
        Service({}, `api/users/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_USER, id: data.id });
                if (withBack) {
                    dispatch(push('/users'));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};
