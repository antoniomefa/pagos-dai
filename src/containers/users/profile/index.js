import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import { deleteUsers } from '../actions';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteUsers([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    render() {
        const { user } = this.props;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h3>{user.name}</h3>
                    </div>
                    <div className="__card__body">
                        <p><strong>Email:</strong>  {user.email}</p>
                        {
                            user.roles && user.roles.length ? (
                                <p>
                                    <strong>Roles:</strong> {user.roles.map(r => r.name).join(', ')}
                                </p>
                            ) : (
                                <p>
                                    <strong>Rol:</strong>  Sin rol
                                </p>
                            )
                        }
                        <p><strong>Teléfono:</strong>  {user.ext || 'Sin teléfono'}</p>
                    </div>
                    <div className="__card__footer">
                        {
                            this.props.user.id !== this.props.auth.user.id && (
                                <Button
                                    text="Eliminar"
                                    icon="trash-o"
                                    isIcon
                                    type="danger"
                                    outline
                                    onButtonPress={this.onDelete}
                                    permission={'delete'}
                                    permissions={this.props.permisos}
                                />
                            )
                        }
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    user: PropTypes.object,
    deleteUsers: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ userProfile, auth }) => ({
    ...userProfile,
    auth
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    deleteUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
