import { ON_GET_USER } from './types';

const initialState = {
    user: {}
};

export default function userProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_USER:
            return {
                ...state,
                user: action.data
            };
        default:
            return { ...state };
    }
}
