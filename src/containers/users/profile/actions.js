import { ON_GET_USER } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/users/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_USER, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Usuario no encontrado',
                color: 'danger',
                alias: 'get-user',
                time: 3
            }));
            dispatch(push('/users'));
        });
};
