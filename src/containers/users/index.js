/* CORE */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import { getData, addQuery, setFilters, goProfile, goCreate, deleteUsers } from './actions';

class App extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Nombre', field: 'name'},
            {name: 'Correo', field: 'username'},
            {name: 'Rol', field: 'roles.0.name'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        return [];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addUser = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __deleteUsers = users => {
        this.props.deleteUsers(users.filter(u => this.props.auth.user.id !== u));
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'Eliminar',
                permission: 'delete',
                action: this.__deleteUsers, tooltip: 'Eliminar usuarios'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                validation: true,
                permission: 'delete',
                action: id => this.__deleteUsers([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

App.defaultProps = {
    total: 0,
    rows: [],
    getData: () => {}
};

App.propTypes = {
    query: PropTypes.object,
    auth: PropTypes.object,
    total: PropTypes.number,
    rows: PropTypes.array,
    getData: PropTypes.func,
    goProfile: PropTypes.func,
    setFilters: PropTypes.func,
    deleteUsers: PropTypes.func,
    goCreate: PropTypes.func
};

const mapStateToProps = ({ users, auth }) => ({
    ...users,
    auth
});

const mapDispatchToProps = {
    getData,
    goProfile,
    setFilters,
    goCreate,
    deleteUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
