
export const ON_FETCH_USERS = 'App/Users/ON_FETCH_USERS';
export const ON_GET_USERS_SUCCESS = 'App/Users/ON_GET_USERS_SUCCESS';
export const ON_GET_USERS_FAIL = 'App/Users/ON_GET_USERS_FAIL';
export const ON_SET_FILTERS = 'App/Users/ON_SET_FILTERS';
export const ON_DELETE_USER = 'App/Users/ON_DELETE_USER';
