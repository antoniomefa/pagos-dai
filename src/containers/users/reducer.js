import {
    ON_GET_USERS_SUCCESS,
    ON_GET_USERS_FAIL,
    ON_FETCH_USERS,
    ON_SET_FILTERS,
    ON_DELETE_USER
} from './types';

const ONE = 1;
const ZERO = 0;

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    loading: false
};

export default function usersReducer(state = initialState, action) {
    var index = state.rows.findIndex(r => r.id === action.id);

    switch (action.type) {
        case ON_FETCH_USERS:
            return {
                ...state,
                loading: true
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_GET_USERS_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_USERS_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        case ON_DELETE_USER:
            return {
                ...state,
                rows: [
                    ...state.rows.slice(ZERO, index),
                    ...state.rows.slice(index + ONE)
                ]
            };
        default:
            return {
                ...state
            };
    }
}
