import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_BILL } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createBill = data => dispatch => {
    Service(JSON.stringify(data), 'api/taxdocuments', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Factura creada correctamente',
                color: 'primary',
                alias: 'created-tax',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear la factura',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/taxdocuments/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_BILL, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Factura no encontrada',
                color: 'danger',
                alias: 'get-tax',
                time: 3
            }));
            dispatch(push('/bills'));
        });
};

export const updateBill = data => dispatch => {
    Service(JSON.stringify(data), 'api/taxdocuments', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
