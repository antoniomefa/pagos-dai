
export const ON_FETCH_BILLS = 'App/Bills/ON_FETCH_BILLS';
export const ON_GET_BILLS_SUCCESS = 'App/Bills/ON_GET_BILLS_SUCCESS';
export const ON_GET_BILLS_FAIL = 'App/Bills/ON_GET_BILLS_FAIL';
export const ON_SET_FILTERS = 'App/Bills/ON_SET_FILTERS';
export const ON_DELETE_BILL = 'App/Bills/ON_DELETE_BILL';
export const ON_PASS_TREASURER = 'App/Bills/ON_PASS_TREASURER';
export const ON_REFRESH = 'App/Bills/ON_REFRESH';
