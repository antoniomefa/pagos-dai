import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from 'components/input';
import Button from 'components/button';

class Contacts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            formValues: {},
            contact: null
        };
    }

    _getInitials = contact => {
        const FROM = 0;
        const UNTIL = 2;

        return contact.name.substr(FROM, UNTIL).toUpperCase();
    }

    removeContact = (contact, index) => {
        const ZERO = 0;
        const ONE = 1;

        const contacts = [
            ...this.props.contacts.slice(ZERO, index),
            ...this.props.contacts.slice(index + ONE)
        ];

        this.props.onChange(contacts);
    }

    _renderContact = (contact, index) => (
        <div key={index} className="__contact_card">
            {
                this.props.isEditable && (
                    <span
                        className="fa fa-times __button_delete"
                        onClick={() => this.removeContact(contact, index)}
                    />
                )
            }
            <div className="__contact_card_symbol">
                {this._getInitials(contact)}
            </div>
            <div className="__contact_card_info" onClick={() => {
                if (this.props.isEditable) {
                    this.setState({
                        formValues: {
                            name: contact.name,
                            email: contact.email
                        }
                    });
                }
            }}>
                <span>{contact.name}</span>
                <span><small>{contact.email}</small></span>
            </div>
        </div>
    );

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return ['name', 'email'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [
            /^[a-zA-Z ]+$/,
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        this.props.onAddNewContact(this.state.formValues);
        this.setState({ formValues: {} });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    renderForm = () => {
        const { isEditable } = this.props;
        const { formValues, errorMessages, formRequested } = this.state;

        if (!isEditable) return null;

        return (
            <div className="container">
                <div
                    className="form-row"
                    style={{ display: 'flex', alignItems: 'flex-end', justifyContent: 'center' }}
                >
                    <Input
                        hasLabel
                        label="Nombre"
                        htmlFor="name"
                        isGroup
                        hasIcon
                        id="name"
                        icon="user"
                        hasError={!!errorMessages['name']}
                        requested={formRequested}
                        placeholder="nombre..."
                        aria="name"
                        parentClass="col-xs-12 col-sm-4"
                        value={formValues['name']}
                        onChange={this.onChange.bind(this, 'name')}
                        onBlur={this.onBlur.bind(this, 'name')}
                        required
                    />
                    <Input
                        hasLabel
                        type="email"
                        label="Correo electronico"
                        htmlFor="email"
                        icon="envelope-o"
                        requested={formRequested}
                        onChange={this.onChange.bind(this, 'email')}
                        onBlur={this.onBlur.bind(this, 'email')}
                        id="emailContact"
                        value={formValues['email']}
                        placeholder="correo electoronico..."
                        aria="email"
                        parentClass="col-xs-12 col-sm-4"
                        hasError={!!errorMessages['email']}
                        required
                        isGroup
                        hasIcon
                    />
                    <div className="form-group">
                        <Button
                            text="agregar"
                            type="primary"
                            outline
                            isIcon
                            icon={'plus'}
                            onButtonPress={this.onSave}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { contacts } = this.props;

        return (
            <div>
                <small><b>contactos</b></small>
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {
                        contacts.map((contact, index) => this._renderContact(contact, index))
                    }
                </div>
                <div>
                    {this.renderForm()}
                </div>
            </div>
        );
    }
}

Contacts.propTypes = {
    contacts: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired
    })),
    isEditable: PropTypes.bool,
    onAddNewContact: PropTypes.func,
    onChange: PropTypes.func,
    permisos: PropTypes.array
};

Contacts.defaultProps = {
    contacts: []
};

export default Contacts;
