import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import { getData, addQuery, setFilters, goProfile } from './actions';

class BillsToPay extends Component {
    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed.page);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query.page);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'OC', field: 'oc'},
            {name: 'Factura', field: 'factura'},
            {name: 'Total', field: 'total'},
            {name: 'Monto', field: 'monto'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        return [
            { name: 'OC', field: 'OC', options: [
                {value: 'oc 1', label: 'oc'},
                {value: 'oc 2', label: 'oc'}
            ] },
            { name: 'factura', field: 'factura', options: [
                {value: 'factura 1', label: 'factura'},
                {value: 'factura 2', label: 'factura'}
            ] }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newOffset);
        this.props.setFilters({
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        });
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData(page);
    }

    __addUser = () => {
        // console.log('add user')
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'change-status',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'emailing',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'distribute',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'edit', label: 'cambiar estatus', style: 'outline-secondary',
                permission: 'pass-treasurer',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: () => {}
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: () => {}
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: () => {}
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

BillsToPay.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

BillsToPay.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ paymentVouchers }) => ({
    ...paymentVouchers
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(BillsToPay);
