import {
    ON_GET_PAYMENT_VOUCHERS_SUCCESS,
    ON_FETCH_PAYMENT_VOUCHERS,
    ON_SET_FILTERS
} from './types';
import { data } from './data';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';

export function getData(page) {
    return dispatch => {
        const TOTAL = 2000;
        const actionData = {
            type: ON_GET_PAYMENT_VOUCHERS_SUCCESS,
            payload: {
                data,
                page,
                total: TOTAL
            }
        };

        dispatch({ type: ON_FETCH_PAYMENT_VOUCHERS });
        dispatch(actionData);
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/payment-vouchers/${id}`));
    };
}
