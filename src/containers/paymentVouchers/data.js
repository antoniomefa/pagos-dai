
export const data = [
    {
        id: 1, project: 'San isidro', oc: 1,
        factura: 'F-S00212',
        monto: 2000, total: 12000
    },
    {
        id: 2, project: 'San rafael', oc: 12,
        factura: 'F-S002567',
        monto: 2000, total: 12000
    },
    {
        id: 3, project: '209176456', oc: 14,
        factura: 'F-S00234',
        monto: 2000, total: 12000
    },
    {
        id: 4, project: '209897111', oc: 15,
        factura: 'F-S00243',
        monto: 2000, total: 12000
    },
    {
        id: 5, project: 'San isidro', oc: 21,
        factura: 'F-S002424',
        monto: 2000, total: 12000
    },
    {
        id: 6, project: 'San rafael', oc: 19,
        factura: 'F-S0022345',
        monto: 2000, total: 12000
    },
    {
        id: 7, project: '209176456', oc: 11,
        factura: 'F-S002',
        monto: 2000, total: 12000
    },
    {
        id: 8, project: '209897111', oc: 122,
        factura: 'F-S002',
        monto: 2000, total: 12000
    },
    {
        id: 9, project: 'San isidro', oc: 144,
        factura: 'F-S002',
        monto: 2000, total: 12000
    },
    {
        id: 10, project: 'San rafael', oc: 122,
        factura: 'F-S002',
        monto: 2000, total: 12000
    }
];

export const data2 = [
    {
        id: 11, project: 'San isidro', oc: 212,
        factura: 'F-S002343',
        monto: 2000, total: 12000
    },
    {
        id: 12, project: 'San rafael', oc: 221,
        factura: 'F-S0025456',
        monto: 2000, total: 12000
    },
    {
        id: 13, project: 'Bosques acueducto', oc: 115,
        factura: 'F-S002234',
        monto: 2000, total: 12000
    },
    {
        id: 14, project: 'Colinas del bosque', oc: 191,
        factura: 'F-S00247',
        monto: 2000, total: 12000
    },
    {
        id: 15, project: 'San isidro', oc: 241,
        factura: 'F-S002678',
        monto: 2000, total: 12000
    },
    {
        id: 16, project: 'San rafael', oc: 341,
        factura: 'F-S002768',
        monto: 2000, total: 12000
    },
    {
        id: 17, project: 'Bosques acueducto', oc: 391,
        factura: 'F-S00223',
        monto: 2000, total: 12000
    },
    {
        id: 18, project: 'Colinas del bosque', oc: 971,
        factura: 'F-S00298765',
        monto: 2000, total: 12000
    },
    {
        id: 19, project: 'San isidro', oc: 541,
        factura: 'F-S00224',
        monto: 2000, total: 12000
    },
    {
        id: 20, project: 'San rafael', oc: 111,
        factura: 'F-S002',
        monto: 2000, total: 12000
    }
];
