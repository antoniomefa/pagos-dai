import {
    ON_GET_CONFIGURATIONS_SUCCESS,
    ON_FETCH_CONFIGURATIONS,
    ON_SET_FILTERS,
    ON_UPDATE_CONFIG
} from './types';
import { browserHistory } from 'react-router';
import Service from 'helpers/service';
import { push } from 'react-router-redux';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_CONFIGURATIONS });

        Service(query, 'api/config', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_CONFIGURATIONS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export function updateConfig(data) {
    return dispatch => {
        Service(JSON.stringify(data), 'api/config', 'put')
            .then(() => {
                dispatch({
                    type: ON_UPDATE_CONFIG
                });
            })
            .catch(() => {
                //
            });
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/configurations/${id}`));
    };
}
