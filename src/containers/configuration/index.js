import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import SweetAlert from 'react-bootstrap-sweetalert';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import { getData, addQuery, setFilters, goProfile, updateConfig } from './actions';

class Configuration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAlert: false,
            configSelected: null
        };
    }
    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.updated) {
            this.setState({
                showAlert: false,
                configSelected: null
            }, () => this.props.getData(this.props.query));
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Nombre', field: 'key'},
            {name: 'Valor', field: 'value'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        return [];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addUser = () => {
        // console.log('add user')
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addUser, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __getMasiveActions = () => {
        return [];
    }

    update = id => {
        this.setState({
            showAlert: true,
            configSelected: id
        });
    }

    getSelected = () => {
        if (!this.state.configSelected) return null;

        const finded = this.__getDataRows().find(r => r.id == this.state.configSelected);

        if (!finded) return null;

        return finded;
    }

    onUpdate = (newValue) => {
        const finded = this.getSelected();
        if (!finded) return;

        const form = {
            id: finded.id,
            key: finded.key,
            value: newValue
        };

        this.props.updateConfig(form);
    }

    _showAlert = () => {
        if (!this.state.showAlert) return null;

        const config = this.getSelected();

        return (
            <SweetAlert
                input
                showCancel
                cancelBtnBsStyle="default"
                confirmBtnText="Guardar"
                validationMsg="El nuevo valor no puede estar vacio!"
                title={config.key}
                placeHolder="Escirbe su nuevo valor"
                onConfirm={this.onUpdate}
                onCancel={() => this.setState({ configSelected: null, showAlert: false })}
            >
                Escribe su nuevo valor
            </SweetAlert>
        );
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'edit',
                style: 'btn btn-info',
                tooltip: 'editar',
                permission: 'update',
                action: (id) => this.update(id)
            }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
                {this._showAlert()}
             </div>
        );
    }
}

Configuration.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    setFilters: PropTypes.func,
    updateConfig: PropTypes.func,
    goProfile: PropTypes.func
};

Configuration.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ configuration }) => ({
    ...configuration
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goProfile,
    updateConfig
};

export default connect(mapStateToProps, mapDispatchToProps)(Configuration);
