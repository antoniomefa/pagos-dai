
export const ON_FETCH_CONFIGURATIONS = 'App/Configurations/ON_FETCH_CONFIGURATIONS';
export const ON_GET_CONFIGURATIONS_SUCCESS = 'App/Configurations/ON_GET_CONFIGURATIONS_SUCCESS';
export const ON_GET_CONFIGURATIONS_FAIL = 'App/Configurations/ON_GET_CONFIGURATIONS_FAIL';
export const ON_SET_FILTERS = 'App/Configurations/ON_SET_FILTERS';
export const ON_UPDATE_CONFIG = 'App/Configurations/ON_UPDATE_CONFIG';
