
export const ON_FETCH_CUSTOMERS = 'App/Customers/ON_FETCH_CUSTOMERS';
export const ON_GET_CUSTOMERS_SUCCESS = 'App/Customers/ON_GET_CUSTOMERS_SUCCESS';
export const ON_GET_CUSTOMERS_FAIL = 'App/Customers/ON_GET_CUSTOMERS_FAIL';
export const ON_SET_FILTERS = 'App/Customers/ON_SET_FILTERS';
export const CUSTOMERS_SYNC_SUCCESS = 'App/Providers/CUSTOMERS_SYNC_SUCCESS';
