import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';
import { ON_GET_CUSTOMER } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/clients/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_CUSTOMER, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Cliente no encontrado',
                color: 'danger',
                alias: 'get-customer',
                time: 3
            }));
            dispatch(push('/customers'));
        });
};

export const saveCustomer = data => dispatch => {
    Service(JSON.stringify(data), 'api/clients', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};
