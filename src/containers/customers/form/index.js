import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile, saveCustomer } from './actions';
import Select from 'components/select';

import Contacts from 'containers/contacts';
import { getCustomerTypes } from 'utils/catalogs';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            types: [],
            formValues: {
                contacts: []
            }
        };
    }

    componentDidMount() {
        this.__getClientTypes();

        if (this.props.routeParams.id) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    __getClientTypes = () => {
        getCustomerTypes().then(response => {
            this.setState({
                types: response
            });
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.customer.id) {

            formValues.user_name = nextProps.customer.user ? nextProps.customer.user.name : '';
            formValues.user_email = nextProps.customer.user ? nextProps.customer.user.username : '';
            formValues.user_ext = nextProps.customer.user ? nextProps.customer.user.ext : '';
            formValues.contacts = nextProps.customer.contacts;
            formValues.client_type_id = nextProps.customer.client_type.id;
            formValues.user_password = '';

            this.setState({
                formValues
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id && this.props.customer.user;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            const value = select.value;

            formValues[field] = value;
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        if (this.__isEditing()) {
            return ['user_name', 'user_email', 'client_type_id'];
        }
        return ['user_name', 'user_email', 'user_password', 'client_type_id'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        if (this.__isEditing()) {
            return [
                /^[a-zA-Z]+$/,
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                /./
            ];
        }

        return [
            /^[a-zA-Z]+$/,
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            /^.*$/,
            /./
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const { formValues } = this.state;
        const { customer } = this.props;
        const NOT_FOUND = -1;

        const form = {
            id: customer.id,
            user_id: this.__isEditing() ? customer.user.id : NOT_FOUND,
            user_email: formValues.user_email,
            user_password: btoa(formValues.user_password),
            user_ext: formValues.user_ext,
            user_name: formValues.user_name,
            client_type_id: formValues.client_type_id,
            contacts: formValues.contacts
        };

        this.props.saveCustomer(form);
    }

    onChangeContacts = contacts => {
        const formValues = this.state.formValues;

        formValues.contacts = contacts;

        this.setState({ formValues });
    }

    getOptionsRol = () => {
        return this.state.types.map(r => ({ value: r.id, label: r.name }));
    }

    onAddNewContact = contact => {
        const ZERO = 0;
        const formValues = this.state.formValues;
        const newContact = contact.id > ZERO ? contact : { ...contact, id: -1 };

        formValues.contacts = [
            ...formValues.contacts,
            ...[{ ...newContact }]
        ];

        this.setState({ formValues });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Nombre de usuario"
                        htmlFor="user_name"
                        icon="user"
                        id="user_name"
                        hasError={!!errorMessages['user_name']}
                        invalidText={errorMessages['user_name']}
                        requested={formRequested}
                        placeholder="nombre de usuario..."
                        aria="user_name"
                        value={formValues['user_name']}
                        onChange={this.onChange.bind(this, 'user_name')}
                        onBlur={this.onBlur.bind(this, 'user_name')}
                        required
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        type="email"
                        label="Correo electronico"
                        htmlFor="user_email"
                        icon="envelope-o"
                        requested={formRequested}
                        onChange={this.onChange.bind(this, 'user_email')}
                        onBlur={this.onBlur.bind(this, 'user_email')}
                        id="user_email"
                        value={formValues['user_email']}
                        placeholder="correo electoronico..."
                        aria="user_email"
                        hasError={!!errorMessages['user_email']}
                        invalidText={errorMessages['user_email']}
                        required
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Contraseña"
                        htmlFor="user_password"
                        icon="key"
                        id="user_password"
                        hasError={!!errorMessages['user_password']}
                        invalidText={errorMessages['user_password']}
                        requested={formRequested}
                        placeholder="contraseña..."
                        aria="user_password"
                        type="password"
                        value={formValues['user_password']}
                        onChange={this.onChange.bind(this, 'user_password')}
                        onBlur={this.onBlur.bind(this, 'user_password')}
                        required={!this.__isEditing()}
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        label="Extensión telefónica"
                        htmlFor="user_ext"
                        icon="phone"
                        type="number"
                        requested={formRequested}
                        id="user_ext"
                        onChange={this.onChange.bind(this, 'user_ext')}
                        onBlur={this.onBlur.bind(this, 'user_ext')}
                        value={formValues['user_ext']}
                        placeholder="extensión..."
                        aria="user_ext"
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Select
                        hasLabel
                        label="Tipo de cliente"
                        htmlFor="client_type_id"
                        icon="user"
                        requested={formRequested}
                        onChange={this.onChangeSelect.bind(this, 'client_type_id')}
                        onBlur={this.onBlur.bind(this, 'client_type_id')}
                        id="client_type_id"
                        value={formValues['client_type_id']}
                        placeholder="selecciona uno..."
                        options={this.getOptionsRol()}
                        hasError={!!errorMessages['client_type_id']}
                        invalidText={errorMessages['client_type_id']}
                        required
                        isGroup
                        hasIcon
                    />
                </div>
                <Contacts
                    isEditable
                    {...this.props}
                    onAddNewContact={this.onAddNewContact}
                    onChange={this.onChangeContacts}
                    contacts={this.state.formValues.contacts} />
                <div className="pull-right">
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
            </form>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    customer: PropTypes.object,
    getProfile: PropTypes.func,
    saveCustomer: PropTypes.func,
    routeParams: PropTypes.object
};

const mapStateToProps = ({ customerForm }) => ({
    ...customerForm
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    saveCustomer
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
