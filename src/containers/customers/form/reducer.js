import { ON_GET_CUSTOMER } from './types';

const initialState = {
    customer: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_CUSTOMER:
            return {
                ...state,
                customer: action.data
            };
        default:
            return { ...state };
    }
}
