import {
    ON_GET_CUSTOMERS_SUCCESS,
    ON_GET_CUSTOMERS_FAIL,
    ON_FETCH_CUSTOMERS,
    ON_SET_FILTERS,
    CUSTOMERS_SYNC_SUCCESS
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    isSync: false,
    loading: false
};

export default function customersReducer(state = initialState, action) {
    switch (action.type) {
        case ON_FETCH_CUSTOMERS:
            return {
                ...state,
                loading: true,
                isSync: false
            };
        case CUSTOMERS_SYNC_SUCCESS:
            return {
                ...state,
                isSync: true
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_GET_CUSTOMERS_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_CUSTOMERS_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
