import { ON_GET_CUSTOMER } from './types';

const initialState = {
    customer: null
};

export default function customerProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_CUSTOMER:
            return {
                ...state,
                customer: action.data
            };
        default:
            return { ...state };
    }
}
