import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import Contacts from 'containers/contacts';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {

    }

    onUpdate = () => {
        this.props.goEdit();
    }

    _getContactsText = () => {
        const ONE = 1;
        const { customer } = this.props;

        if (customer.contacts.length == ONE) return '1 contacto';

        return `${customer.contacts.length} contactos`;
    }

    render() {
        const { customer } = this.props;

        if (!customer) return null;
        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card_provider">
                    <div className="__card__header">
                        <h5>{customer.bussiness_name}</h5>
                    </div>
                    <div className="__card__body">
                        <p><span>RFC:</span>{customer.rfc}</p>
                        <p><span>Teléfono:</span>{customer.phone}</p>
                        {
                            customer.client_type ? (
                                <p><span>Tipo:</span>{customer.client_type.name}</p>
                            ) : (
                                <p><span>Tipo:</span>Desconocido</p>
                            )
                        }
                        {
                            !customer.user ? (
                                <p>
                                    <span>Usuario:</span>  {'Sin usuario'}
                                </p>
                            ) : (
                                <p>
                                    <span>Usuario:</span>
                                    {`   ${customer.user.name}(${customer.user.email})`}
                                </p>
                            )
                        }
                        <p><span>Contactos:</span>{this._getContactsText()}</p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                    <Contacts
                        {...this.props}
                        contacts={customer.contacts} />
                </div>
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    getProfile: PropTypes.func,
    routeParams: PropTypes.object,
    customer: PropTypes.object,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ customerProfile }) => ({
    ...customerProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
