import { ON_GET_BILL } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/taxdocuments/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_BILL, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Factura no encontrada',
                color: 'danger',
                alias: 'get-bill',
                time: 3
            }));
            dispatch(push('/bills'));
        });
};

export const validate = (id, where) => dispatch => {
    Service({}, `api/taxdocuments/${id}/${where}`, 'put')
        .then(() => {
            dispatch(sendNotification({
                message: 'Factura validada',
                level: 'success'
            }));
        })
        .catch((e) => {
            // error
            dispatch(sendNotification({
                message: e,
                level: 'warning'
            }));
        });
};
