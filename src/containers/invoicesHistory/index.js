import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SnackbarProvider } from 'notistack';

/* COMPONENTS */
import InvoicesTable from './form/invoicestable'

/* ACTOONS */
import {
  getData,
  addQuery,
  setFilters,
  goProfile,
  goCreate,
  deleteInvoicesHistory,
  downloadFile,
  passTrasurer,
  notification,
  validate
} from './actions';

class InvoicesHistory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAlert: null,
            InvoicesHistoryelected: null,
            providersCatalog: [],
            statusCatalog: [],
            customerTypesCatalog: []
        };
    }

    
    render() {
        return (
            <SnackbarProvider maxSnack={3}>
                <InvoicesTable/>
        </SnackbarProvider>
        );
    }
}

InvoicesHistory.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    setFilters: PropTypes.func,
    downloadFile: PropTypes.func,
    deleteInvoicesHistory: PropTypes.func,
    notification: PropTypes.func,
    passTrasurer: PropTypes.func,
    goCreate: PropTypes.func,
    validate: PropTypes.func,
    goProfile: PropTypes.func
};

InvoicesHistory.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ invoicesHistory }) => ({
    ...invoicesHistory
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goCreate,
    validate,
    downloadFile,
    passTrasurer,
    deleteInvoicesHistory,
    notification,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(InvoicesHistory);
