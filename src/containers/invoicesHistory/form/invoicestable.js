import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import { getAccessToken } from 'helpers';
import { URL_BASE } from 'constants';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import ReceiptIcon from '@material-ui/icons/Receipt';

import Paper from "@material-ui/core/Paper";

import '../form/invoicestable.css';

 const data = [
    {
        "year": 2019,
        "month": 1,
        "xmlurl": "http://facturasapp.sytes.net:1000/facturas/M107012.xml",
        "pdfurl": "http://facturasapp.sytes.net:1000/facturas/M107012.pdf"
    },
    {
        "year": 2019,
        "month": 2,
        "xmlurl": "http://facturasapp.sytes.net:1000/facturas/M107013.xml",
        "pdfurl": "http://facturasapp.sytes.net:1000/facturas/M107013.pdf"
    },
    {
        "year": 2019,
        "month": 6,
        "xmlurl": "http://facturasapp.sytes.net:1000/facturas/M107012.xml",
        "pdfurl": "http://facturasapp.sytes.net:1000/facturas/M107012.pdf"
    },
    {
        "year": 2019,
        "month": 12,
        "xmlurl": "http://facturasapp.sytes.net:1000/facturas/M107013.xml",
        "pdfurl": "http://facturasapp.sytes.net:1000/facturas/M107013.pdf"
    }
]; 



const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
    pimaryButton: {
        paddingTop: '5px',
        paddingBottom: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        color: 'white',
        backgroundColor: '#d11217'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    }
});


class CardsTable extends Component {

    constructor(...props) {
        super(...props)
        this.state = {
            //myInvoices: data,
            myInvoices: {}
        }
      }

    handleDownload(downloadUrl, type){
        setTimeout(() => {
            const response = {
              file: downloadUrl,
            };
            window.open(response.file);
          }, 100);
    this.props.enqueueSnackbar('Archivo descargado', { variant: 'success', autoHideDuration: 1000, });
    }

    handleUpdate(){
        var counter = 0;
        while(counter < 9){
            this.getInvoices();
            counter++;
        }
    }

    getInvoices() {
        var token = getAccessToken();
        fetch(`${URL_BASE}api/invoices`,
        {
            method: 'get',
            headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
            }
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No fue posible obtener las facturas en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 3000, });
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 5000, });
            throw res;
            } else {
                this.setState({ 
                    myInvoices: res.data
                 })
            }
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });   
    }
    
    showMonth(month){
        switch(month){
            case 1: return 'ENE';
            case 2: return 'FEB';
            case 3: return 'MAR';
            case 4: return 'ABR';
            case 5: return 'MAY';
            case 6: return 'JUN';
            case 7: return 'JUL';
            case 8: return 'AGO';
            case 9: return 'SEP';
            case 10: return 'OCT';
            case 11: return 'NOV';
            case 12: return 'DIC';
            default: return '-';
        }
    }

    componentDidMount = () => {
        const script = document.createElement('script')
        script.src = 'https://cdn.conekta.io/js/latest/conekta.js'
        script.async = true
        document.body.appendChild(script)
        Conekta.setPublicKey("key_V2f81Aytvb4R2CsXuF8Wyqg"); // Llave de produccion
        this.handleUpdate();
    }
    
    render() {
        const { classes } = this.props;
        const rows = this.state.myInvoices;
        
        return ( 
            <Fragment>
                {
                    ( rows.length> 0) && (!this.state.agregarTarjetas) && (
                        <Fragment>
                            <div>
                                <h5>Historial de facturas</h5>
                            </div>
                            <Paper className="table-container">
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">Año</TableCell>
                                            <TableCell align="center">Mes</TableCell>
                                            <TableCell align="center">PDF</TableCell>
                                            <TableCell align="center">CFDI</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {rows.map(row => (
                                            <TableRow>
                                                <TableCell align="center">{row.year}</TableCell>
                                                <TableCell align="center">{this.showMonth(row.month)}</TableCell>
                                                <TableCell align="center">
                                                    <IconButton 
                                                        aria-label="PDF" 
                                                        color="primary" 
                                                        onClick={()=>this.handleDownload(row.pdfurl, 'pdf')}>
                                                        <PictureAsPdfIcon />
                                                    </IconButton>    
                                                </TableCell>
                                                <TableCell align="center">
                                                    <IconButton 
                                                        aria-label="CFDI" 
                                                        color="primary" 
                                                        onClick={()=>this.handleDownload(row.xmlurl, 'xml')}>
                                                        <ReceiptIcon />
                                                    </IconButton>
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Paper>
                        </Fragment>
                    )
                }
                
                {
                    (!rows.length>0) &&(
                        <Grid container direction="row" justify="center" alignItems="center">
                            <h5>No tiene facturas registradas</h5>
                        </Grid>
                    )
                }
                
            </Fragment>
        );
    }
    
}

export default withStyles(styles)(withSnackbar(CardsTable))