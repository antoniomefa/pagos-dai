import { ON_GET_REFERRAL } from './types';

const initialState = {
    bill: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_REFERRAL:
            return {
                ...state,
                bill: action.data
            };
        default:
            return { ...state };
    }
}
