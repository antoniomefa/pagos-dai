import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'components/select';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, createBill, updateBill, getProfile, cancelForm } from './actions';
import { notification, downloadFile } from '../actions';
import { getProjects, getOrders } from 'utils/catalogs';
import UploadFile from 'components/uploadFile';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button as ButtonX,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            dropdownOpen: null,
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            payment_voucher: {},
            formValues: {
                payment_vouchers: [],
                project_id: '',
                amount: '',
                support_pdf_url: '',
                purchase_order_id: '',
                pdf_url: ''
            },
            projects: [],
            orders: []
        };
    }

    componentDidMount() {
        this.__getProjects();

        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    __isRequiredSupport = () => {
        const ZERO = 0;
        const { auth: { user }} = this.props;

        return user.roles[ZERO].name.toLowerCase() === 'proveedor';
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.bill.id) {
          formValues.pdf_url = nextProps.bill.pdf_url;
          formValues.support_pdf_url = nextProps.bill.support ? nextProps.bill.support.pdf_url : '';
          formValues.payment_vouchers = nextProps.bill.payment_vouchers || [];
          formValues.amount = nextProps.bill.amount + '';
          formValues.project_id = nextProps.bill.project ?
            nextProps.bill.project.id : '';
          formValues.purchase_order_id = nextProps.bill.purchase_order ?
            nextProps.bill.purchase_order.id : '';

          this.setState({
              formValues
          });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    __getProjects = () => {
        getProjects().then(response => {
            this.setState({
                projects: response
            });
        });
    }

    __getOrders = projectId => {
        getOrders(projectId).then(response => {
            this.setState({
                orders: response
            });
        });
    }

    onChangeVoucher = (field, e) => {
        const value = e.target.value;
        const payment_voucher = this.state.payment_voucher;

        payment_voucher[field] = value;

        this.setState({ payment_voucher });
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }

            if (field === 'project_id') {
                this.setState({
                    orders: []
                }, () => this.__getOrders(select.value));
            }
        } else {
            delete formValues[field];
        }

        if (field === 'project_id') {
            formValues.purchase_order_id = '';
            this.setState({
                formValues,
                orders: []
            });
        } else {
            this.setState({ formValues });
        }
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        if (this.__isEditing()) {
            if (this.__isRequiredSupport()) {
                return ['project_id', 'xml_url', 'amount', 'support_pdf_url'];
            }
            return ['project_id', 'pdf_url', 'amount'];
        }
        return ['project_id', 'pdf_url', 'amount'];
    }

    /* eslint-disable */
    inputsRegex = () => {
      if (this.__isEditing()) {
            if (this.__isRequiredSupport()) {
                return [
                    /^.*$/,
                    /^.*$/,
                    /^[0-9]+$/,
                    /^.*$/
                ];
            }
            return [
                /^.*$/,
                /^.*$/,
                /^[0-9]+$/
            ];
        }
      return [
          /^.*$/,
          /^.*$/,
          /^[0-9]+$/
      ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            const { formValues } = this.state;

            if (formValues.pdf_url === '') {
                this.props.notification('El pdf es requerido');
            } else if (formValues.support_pdf_url === '') {
                this.props.notification('El soporte es requerido');
            }

            return this.setState({ invalidForm: true });
        }

        const NOT_FOUND = -1;
        const { formValues } = this.state;
        const field = 'purchase_order_id';
        const purchase_order_id = formValues[field] === '' ? NOT_FOUND : formValues[field];

        const form = {
            pdf_url: formValues['pdf_url'],
            project_id: formValues['project_id'],
            support_pdf_url: formValues['support_pdf_url'],
            amount: parseFloat(formValues.amount),
            payment_vouchers: formValues.payment_vouchers,
            purchase_order_id
        };

        if (this.__isEditing()) {
            this.props.updateBill({ ...form, id: this.props.bill.id });
        } else {
            this.props.createBill(form);
        }

    }

    getOptionsProjects = () => {
        return this.state.projects.map(r => ({ value: r.id, label: r.name }));
    }

    getOptionsOrders = () => {
        return this.state.orders.map(r => ({ value: r.id, label: `Orden ID: ${r.id}` }));
    }

    onUploadSupport = url => {
        const formValues = this.state.formValues;

        formValues.support_pdf_url = url;

        this.setState({ formValues });
    }

    onUploadTax = (type, url) => {
        const formValues = this.state.formValues;

        if (type === 'pdf') {
            formValues.pdf_url = url;
        }

        this.setState({ formValues });
    }

    onUpload = (url) => {
        const payment_voucher = this.state.payment_voucher;

        payment_voucher.pdf_url = url;

        this.setState({ payment_voucher });
    }

    onSaveVoucher = () => {
        if (!this.state.payment_voucher.pdf_url) {
            this.props.notification('Debes agregar el pdf');
            return;
        }

        const { payment_voucher } = this.state;

        if (!payment_voucher.amount || isNaN(parseFloat(payment_voucher.amount))) {
            this.props.notification('Debes agregar el monto');
            return;
        }

        const data = {
            id: -1,
            ...this.state.payment_voucher,
            notes: this.state.formValues.notes,
            amount: parseFloat(this.state.payment_voucher.amount)
        };

        const formValues = this.state.formValues;

        formValues.payment_vouchers = [
            ...formValues.payment_vouchers,
            ...[data]
        ];

        this.setState({
            modalIsOpen: null,
            payment_voucher: {},
            formValues
        });
    }

    toggle = modalIsOpen => {
        this.setState({
            payment_voucher: {},
            modalIsOpen: modalIsOpen.target.value
        });
    }

    showModal = () => {
        const { errorMessages, formRequested } = this.state;

        if (!this.state.modalIsOpen) return null;

        return (
            <Modal isOpen={true} toggle={this.toggle} className={''}>
                <ModalHeader toggle={this.toggle}>Comprobante de pago</ModalHeader>
                <ModalBody>
                    <div className="form-row">
                        <UploadFile
                            onFileUploaded={this.onUpload}
                            previousName={this.state.payment_voucher.pdf_url}
                            label="Sube tu comprobante de pago"
                            types="image/x-png,image/gif,image/jpeg,application/pdf"
                        />
                    </div>
                    <div className="form-row">
                        <Input
                            hasLabel
                            label="Monto"
                            htmlFor="amount"
                            requested={formRequested}
                            id="amount"
                            onChange={this.onChangeVoucher.bind(this, 'amount')}
                            onBlur={this.onBlur.bind(this, 'amount')}
                            value={this.state.payment_voucher['amount']}
                            placeholder="Monto"
                            aria="amount"
                            hasError={!!errorMessages['amount']}
                            invalidText={errorMessages['amount']}
                        />
                        <Input
                            hasLabel
                            label="Notas"
                            htmlFor="notes"
                            requested={formRequested}
                            id="notes"
                            onChange={this.onChangeVoucher.bind(this, 'notes')}
                            onBlur={this.onBlur.bind(this, 'notes')}
                            value={this.state.payment_voucher['notes']}
                            placeholder="Notas"
                        />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonX color="primary" onClick={this.onSaveVoucher}>Guardar</ButtonX>{' '}
                    <ButtonX color="secondary" onClick={this.toggle}>Cancel</ButtonX>
                </ModalFooter>
            </Modal>
        );
    }

    toggleDropdown = dropdown => {
        if (dropdown !== this.state.dropdownOpen) {
            this.setState({
                dropdownOpen: dropdown
            });
        } else {
            this.setState({
                dropdownOpen: null
            });
        }
    }

    _delete(index) {
        const ZERO = 0;
        const ONE = 1;
        const formValues = this.state.formValues;

        formValues.payment_vouchers = [
            ...formValues.payment_vouchers.slice(ZERO, index),
            ...formValues.payment_vouchers.slice(index + ONE)
        ];

        this.setState({ formValues });
    }

    downloadFile = url => {
        this.props.downloadFile(url, false);
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                {
                  !this.__isEditing() ? (
                    <div className="form-row">
                        <Select
                            hasLabel
                            label="Proyecto"
                            htmlFor="project_id"
                            requested={formRequested}
                            onChange={this.onChangeSelect.bind(this, 'project_id')}
                            onBlur={this.onBlur.bind(this, 'project_id')}
                            id="project_id"
                            value={formValues['project_id']}
                            placeholder="selecciona un proyecto..."
                            options={this.getOptionsProjects()}
                            hasError={!!errorMessages['project_id']}
                            invalidText={errorMessages['project_id']}
                            required
                        />
                        <Select
                            hasLabel
                            label="Orden de compra"
                            htmlFor="purchase_order_id"
                            requested={formRequested}
                            onChange={this.onChangeSelect.bind(this, 'purchase_order_id')}
                            onBlur={this.onBlur.bind(this, 'purchase_order_id')}
                            id="purchase_order_id"
                            value={formValues['purchase_order_id']}
                            placeholder="selecciona una orden de compra..."
                            required={this.__isRequiredSupport()}
                            options={this.getOptionsOrders()}
                            hasError={!!errorMessages['purchase_order_id']}
                            invalidText={errorMessages['purchase_order_id']}
                        />
                    </div>
                  ) : (
                    <div><h5>Folio: {this.props.bill.folio}</h5></div>
                  )
                }
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Monto"
                        htmlFor="amount"
                        requested={formRequested}
                        id="amount"
                        onChange={this.onChange.bind(this, 'amount')}
                        onBlur={this.onBlur.bind(this, 'amount')}
                        value={formValues['amount']}
                        placeholder="Escribe el monto de la nota..."
                        aria="amount"
                        hasError={!!errorMessages['amount']}
                        invalidText={errorMessages['amount']}
                    />
                    <UploadFile
                        onFileUploaded={this.onUploadTax.bind(this, 'pdf')}
                        previousName={this.state.formValues.pdf_url}
                        label="PDF"
                        types="application/pdf"
                    />
                </div>
                <div className="form-row">
                    <UploadFile
                        onFileUploaded={this.onUploadSupport}
                        previousName={this.state.formValues.support_pdf_url}
                        label="Soporte"
                        types="application/pdf"
                    />
                </div>
                {
                  this.__isEditing() && (
                    <div className="col-xs-12 col-sm-6" >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <h5 style={{ margin: 0, marginRight: 20 }}>Comprobantes de pago</h5>
                            <span className="fa fa-plus-circle" onClick={() => {
                                this.setState({ modalIsOpen: true });
                            }} />
                        </div>
                        <div style={{ display: 'flex' }}>
                            {
                                this.state.formValues.payment_vouchers.map((document, index) => (
                                    <Dropdown
                                        isOpen={this.state.dropdownOpen === `tax-${index}`}
                                        toggle={() => this.toggleDropdown(`tax-${index}`)}
                                        style={{ margin: 4 }}
                                        key={index}
                                    >
                                        <DropdownToggle
                                            tag="span"
                                            data-toggle="dropdown"
                                        >
                                            <img src="/assets/images/voucher.png" width="70" />
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={() => {
                                                if (document.pdf_url === '' || !document.pdf_url) {
                                                    const message = 'No existe archivo a descargar';
                                                    this.props.notification(message);
                                                } else {
                                                    this.downloadFile(document.pdf_url);
                                                }
                                            }}>Descargar comprobante</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem onClick={() => {
                                                this._delete(index);
                                            }}>Eliminar</DropdownItem>
                                        </DropdownMenu>
                                    </Dropdown>
                                ))
                            }
                        </div>
                    </div>
                  )
                }
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
                {this.showModal()}
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    updateBill: PropTypes.func,
    notification: PropTypes.func,
    downloadFile: PropTypes.func,
    bill: PropTypes.object,
    cancelForm: PropTypes.func,
    createBill: PropTypes.func
};

const mapStateToProps = ({ referralNoteForm, auth }) => ({
    ...referralNoteForm,
    auth
});

const mapDispatchToProps = {
    goEdit,
    createBill,
    getProfile,
    notification,
    downloadFile,
    updateBill,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
