
export const ON_FETCH_REFERRAL_NOTES = 'App/ReferralNotes/ON_FETCH_REFERRAL_NOTES';
export const ON_GET_REFERRAL_NOTES_SUCCESS = 'App/ReferralNotes/ON_GET_REFERRAL_NOTES_SUCCESS';
export const ON_GET_REFERRAL_NOTES_FAIL = 'App/ReferralNotes/ON_GET_REFERRAL_NOTES_FAIL';
export const ON_SET_FILTERS = 'App/ReferralNotes/ON_SET_FILTERS';
export const ON_DELETE_REFERRAL_NOTE = 'App/ReferralNotes/ON_DELETE_REFERRAL_NOTE';
export const ON_PASS_TREASURER = 'App/ReferralNotes/ON_PASS_TREASURER';
export const ON_REFRESH = 'App/ReferralNotes/ON_REFRESH';
