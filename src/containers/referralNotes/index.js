import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';
import SweetAlert from 'react-bootstrap-sweetalert';

/* ACTOONS */
import {
  getData,
  addQuery,
  setFilters,
  goProfile,
  goCreate,
  deleteReferralNotes,
  downloadFile,
  passTrasurer,
  notification,
  validate
} from './actions';

/* CATALOGS */
import {
    getProviders,
    getReferralNoteStatus,
    getCustomerTypes
} from 'utils/catalogs';

class Bills extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAlert: null,
            noteSelected: null,
            providersCatalog: [],
            statusCatalog: [],
            customerTypesCatalog: []
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getProvidersCatalog();
        this.getStatusCatalog();
        this.getCustomerTypesCatalog();
    }

    getProvidersCatalog = () => {
        getProviders().then(providersCatalog => this.setState({ providersCatalog }));
    }

    getStatusCatalog = () => {
        getReferralNoteStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    getCustomerTypesCatalog = () => {
        getCustomerTypes().then(customerTypesCatalog => this.setState({ customerTypesCatalog }));
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
      if (nextProps.refresh) {
        this.props.getData(this.props.query);
      }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Proveedor', field: 'provider.bussiness_name'},
            {name: 'Estatus', field: 'status'},
            {name: 'Monto', field: 'amount'},
            {name: 'Tipo cliente', field: 'client.client_type.name'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { providersCatalog } = this.state;
        const { statusCatalog } = this.state;
        const { customerTypesCatalog } = this.state;

        var providersOptions = providersCatalog.map(p => (
            { label: p.bussiness_name, value: p.id }
        ));
        var statusOptions = statusCatalog.map(s => (
            { label: s, value: s}
        ));
        var customerTypesOptions = customerTypesCatalog.map(ct => (
            { label: ct.name, value: ct.id}
        ));
        return [
            { name: 'proveedor', field: 'provider_id', options: providersOptions },
            { name: 'estatus', field: 'referral_note_status', options: statusOptions },
            { name: 'tipo cliente', field: 'client_type_id', options: customerTypesOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    getSelected = () => {
        const finded = this.__getDataRows().find(r => r.id === this.state.noteSelected);

        return finded;
    }

    onInvalidate = text => {
        this.props.validate([this.state.noteSelected], 'invalidate', text);
        this.setState({
            showAlert: false,
            noteSelected: null
        });
    }

    _showAlert = () => {
        if (!this.state.showAlert) return null;

        return (
            <SweetAlert
                input
                showCancel
                cancelBtnBsStyle="default"
                confirmBtnText="Guardar"
                validationMsg="Las observaciones no pueden estar vacias!"
                title={'describe porque la invalidas'}
                placeHolder="observaciones"
                onConfirm={this.onInvalidate}
                onCancel={() => this.setState({ noteSelected: null, showAlert: false })}
            >
                Observaciones
            </SweetAlert>
        );
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addBill = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addBill, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    __treasurer = ids => {
      this.props.passTrasurer(ids);
    }

    __delete = ids => {
      this.props.deleteReferralNotes(ids);
    }

    __download = id => {
      const finded = this.__getDataRows().find(b => b.id === id);

      if (!finded) this.props.notification('No se encontro la nota de remisión');
      else {
        if (finded.pdf_url === '') this.props.notification('No existe pdf a descargar');
        else this.props.downloadFile(finded.pdf_url, 'pdf');
      }
    }

    __changeStatus = (where, ids) => {
        this.props.validate(ids, where);
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'Eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.__delete, tooltip: 'agregar nuevo usuario'
            },
            {
                icon: 'money', label: 'Pasar a tesoreria', style: 'outline-success',
                permission: 'pass-treasurer',
                action: this.props.passTrasurer, tooltip: 'pasar a tesoreria'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: id => this.props.deleteReferralNotes([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: this.__download
            },
            {
                icon: 'dollar-sign',
                style: 'btn btn-success',
                tooltip: 'Pasar a tesorero',
                permission: 'pass-treasurer',
                action: id => this.props.passTrasurer([id])
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: () => {}
            },
            {
                icon: 'check',
                style: 'btn btn-success',
                tooltip: 'Validar',
                permission: 'change-status',
                action: id => this.props.validate([id], 'validate')
            },
            {
                icon: 'times',
                style: 'btn btn-danger',
                tooltip: 'invalidar',
                permission: 'change-status',
                action: id => this.setState({ showAlert: true, noteSelected: id })
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
                {this._showAlert()}
             </div>
        );
    }
}

Bills.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    setFilters: PropTypes.func,
    downloadFile: PropTypes.func,
    deleteReferralNotes: PropTypes.func,
    notification: PropTypes.func,
    passTrasurer: PropTypes.func,
    goCreate: PropTypes.func,
    validate: PropTypes.func,
    goProfile: PropTypes.func
};

Bills.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ referralNotes }) => ({
    ...referralNotes
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goCreate,
    validate,
    downloadFile,
    passTrasurer,
    deleteReferralNotes,
    notification,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Bills);
