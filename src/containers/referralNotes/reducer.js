import {
    ON_GET_REFERRAL_NOTES_SUCCESS,
    ON_GET_REFERRAL_NOTES_FAIL,
    ON_FETCH_REFERRAL_NOTES,
    ON_SET_FILTERS,
    ON_DELETE_REFERRAL_NOTE,
    ON_REFRESH,
    ON_PASS_TREASURER
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    refresh: false,
    rows: [],
    loading: false
};

const ZERO = 0;
const ONE = 1;

export default function providersReducer(state = initialState, action) {
    var index = state.rows.findIndex(r => r.id === action.id);

    switch (action.type) {
        case ON_FETCH_REFERRAL_NOTES:
            return {
                ...state,
                loading: true,
                refresh: false
            };
        case ON_PASS_TREASURER:
        case ON_REFRESH:
          return {
            ...state,
            refresh: true
          };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
          case ON_DELETE_REFERRAL_NOTE:
              return {
                  ...state,
                  rows: [
                      ...state.rows.slice(ZERO, index),
                      ...state.rows.slice(index + ONE)
                  ]
              };
        case ON_GET_REFERRAL_NOTES_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_REFERRAL_NOTES_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
