import {
  ON_GET_REFERRAL_NOTES_SUCCESS,
  ON_FETCH_REFERRAL_NOTES,
  ON_SET_FILTERS,
  ON_DELETE_REFERRAL_NOTE,
  ON_REFRESH,
  ON_PASS_TREASURER
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import { sendNotification } from 'actions';
import Service from 'helpers/service';
import { downloadB64 } from 'helpers';

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export function downloadFile(url, type) {
    return dispatch => {
        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                downloadB64(mime, name, data);
            })
            .catch(() => {
                dispatch(notification(`Error al descargar el ${type}`));
            });
    };
}

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_REFERRAL_NOTES });

        Service(query, 'api/referralnotes', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_REFERRAL_NOTES_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });
    };
}

export function passTrasurer(referral_notes) {
    return dispatch => {
        Service(JSON.stringify({ referral_notes }), 'api/referralnotes/sendtotreasury', 'put')
            .then(() => {
            dispatch(sendNotification({
                message: 'Factura pasada a tesoreria',
                level: 'success'
            }));
              dispatch({ type: ON_PASS_TREASURER });
            })
            .catch((e) => {
                dispatch(notification(e));
            });
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/referral-notes/create'));
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/referral-notes/${id}`));
    };
}

export const deleteReferralNotes = (bills, withBack = false) => dispatch => {
    bills.forEach(id => {
        Service({}, `api/referralnotes/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_REFERRAL_NOTE, id: data.id });
                if (withBack) {
                    dispatch(push('/referral-notes'));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};

export const validate = (ids, where, observations = null) => dispatch => {
    const ONE = 1;
    const form = observations ? JSON.stringify({ observations }) : {};

    ids.map((id, index) => {
        Service(form, `api/referralnotes/${id}/${where}`, 'put')
            .then(() => {
                dispatch(sendNotification({
                    message: `Nota de remisión ${where === 'validate' ? 'validada' : 'invalidada'}`,
                    level: 'success'
                }));
                if (index === ids.length - ONE) {
                    dispatch({ type: ON_REFRESH });
                }
            })
            .catch((e) => {
                // error
                dispatch(sendNotification({
                    message: e,
                    level: 'warning'
                }));
            });
    });
};
