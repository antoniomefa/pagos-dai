import { ON_GET_REFERRAL_NOTE } from './types';

const initialState = {
    referralNote: {}
};

export default function referralNoteProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_REFERRAL_NOTE:
            return {
                ...state,
                referralNote: action.data
            };
        default:
            return { ...state };
    }
}
