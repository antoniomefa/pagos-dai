import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import {
  deleteReferralNotes,
  downloadFile,
  notification,
  validate,
  passTrasurer
} from '../actions';
import SweetAlert from 'react-bootstrap-sweetalert';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null,
            showAlert: null,
            noteSelected: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteReferralNotes([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    onChangeStatus = (where) => {
        this.props.validate([this.props.referralNote.id], where);
    }

    onDownload = () => {
      if (this.props.referralNote.pdf_url === '') {
          this.props.notification('No existe pdf a descargar');
      } else this.props.downloadFile(this.props.referralNote.pdf_url, 'pdf');
    }

    Treasurer = () => {
        this.props.passTrasurer([this.props.referralNote.id]);
    }

    onInvalidate = text => {
        this.props.validate([this.props.referralNote.id], 'invalidate', text);
        this.setState({
            showAlert: false,
            noteSelected: null
        });
    }

    _showAlert = () => {
        if (!this.state.showAlert) return null;

        return (
            <SweetAlert
                input
                showCancel
                cancelBtnBsStyle="default"
                confirmBtnText="Guardar"
                validationMsg="Las observaciones no pueden estar vacias!"
                title={'describe porque la invalidas'}
                placeHolder="observaciones"
                onConfirm={this.onInvalidate}
                onCancel={() => this.setState({ noteSelected: null, showAlert: false })}
            >
                Observaciones
            </SweetAlert>
        );
    }

    render() {
        const { referralNote } = this.props;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h3>
                            Nota de remisión, OC ID: {
                                referralNote.purchase_order ?
                                referralNote.purchase_order.id : 'Sin OC'
                            }
                        </h3>
                    </div>
                    <div className="__card__body">
                        <p><strong>Monto:</strong>  {referralNote.amount}</p>
                        <p><strong>Estatus:</strong>  {referralNote.status}</p>
                        <p><strong>proveedor:</strong>  {
                            referralNote.provider ?
                            referralNote.provider.bussiness_name : 'Sin proveedor'
                        }</p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Eliminar"
                            icon="trash-o"
                            isIcon
                            type="danger"
                            outline
                            onButtonPress={this.onDelete}
                            permission={'delete'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Descargar"
                            type="dark"
                            icon="download"
                            isIcon
                            outline
                            onButtonPress={this.onDownload}
                            permission={'download'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Validar"
                            type="dark"
                            icon="check"
                            isIcon
                            outline
                            onButtonPress={this.onChangeStatus.bind(this, 'validate')}
                            permission={'change-status'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Invalidar"
                            type="dark"
                            icon="times"
                            isIcon
                            outline
                            onButtonPress={() => {
                                this.setState({ showAlert: true, noteSelected: referralNote.id });
                            }}
                            permission={'change-status'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
                {this._showAlert()}
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    downloadFile: PropTypes.func,
    validate: PropTypes.func,
    notification: PropTypes.func,
    passTrasurer: PropTypes.func,
    referralNote: PropTypes.object,
    deleteReferralNotes: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ referralNoteProfile }) => ({
    ...referralNoteProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    passTrasurer,
    downloadFile,
    validate,
    notification,
    deleteReferralNotes
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
