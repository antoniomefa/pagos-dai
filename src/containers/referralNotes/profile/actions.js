import { ON_GET_REFERRAL_NOTE } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/referralnotes/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_REFERRAL_NOTE, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Notas de remisión no encontrada',
                color: 'danger',
                alias: 'get-referralNote',
                time: 3
            }));
            dispatch(push('/referral-notes'));
        });
};

export const validate = (id, where) => dispatch => {
    Service({}, `api/referralnotes/${id}/${where}`, 'put')
        .then(() => {
            dispatch(sendNotification({
                message: 'Nota de remisión validada',
                level: 'success'
            }));
        })
        .catch((e) => {
            // error
            dispatch(sendNotification({
                message: e,
                level: 'warning'
            }));
        });
};
