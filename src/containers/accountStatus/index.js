import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './assets/App.css';
import Form from './payform/Form';
import { SnackbarProvider } from 'notistack';

class AccountStatus extends Component {
    
    render(){
        return(
            <SnackbarProvider maxSnack={3}>
                {/*<div className="App">
                    <header className="App-header">
                         <div className="App-header-info">
                            <h3>Sistema de pagos de <strong>DAI ALARMAS</strong></h3>
                        </div> 
                    </header>
                    
                </div>*/}
                <Form /> 
            </SnackbarProvider>
        )
    }
}

AccountStatus.propTypes = {
    // query: PropTypes.object,
    // rows: PropTypes.array,
    // total: PropTypes.number,
    // getData: PropTypes.func,
    // addQuery: PropTypes.func,
    // setFilters: PropTypes.func,
    // goProfile: PropTypes.func
};

AccountStatus.defaultProps = {
    // getData: () => {},
    // addQuery: () => {},
    // setFilters: () => {},
    // goProfile: () => {}
};

const mapStateToProps = ({ accountStatus }) => ({
    ...accountStatus
});

const mapDispatchToProps = {
    // getData,
    // addQuery,
    // setFilters,
    // goProfile
};

//export default AccountStatus;
export default connect(mapStateToProps, mapDispatchToProps)(AccountStatus);