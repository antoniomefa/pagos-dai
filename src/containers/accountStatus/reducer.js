import {
    ON_GET_ACCOUNT_STATUS_SUCCESS,
} from './types';

const initialState = {
    cuenta: '',
    concepto: '',
    cantidad: ''
};

export default function providersReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_ACCOUNT_STATUS_SUCCESS:
            return {
                ...state,
                cantidad: action.payload.saldo,
                concepto: action.payload.descripcion
            };
        default:
            return {
                ...state
            };
    }
}
