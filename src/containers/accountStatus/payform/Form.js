import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withSnackbar } from 'notistack';

//import Step1 from './steps/step1';
import Step2 from './steps/step2';
import Step3 from './steps/step3';
import Step4 from './steps/step4';
import { Grid } from '@material-ui/core';

const styles = theme => ({
  backButton: {
    marginRight: theme.spacing.unit,
  },
  pimaryButton: {
    backgroundColor: '#d11217'
  }
});

const defaultState = {
    activeStep: 0,
    formSubmitted: false,
    formvalid: true,
    account: null,
    step1: {
        nombres: '',
        apellidos: '',
        correo: '',
        telefono: '',
    },
    step2: {
        concepto: 'Monto total de adeudo',
        cantidad: '1'
    },
    step3: {
        accept: false,
        paymethod: 'tarjeta'
    }
}

class PayForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ...defaultState
        };
    }

    updateModel = (step, field, value) => {
        this.setState({
            [step]: {
                ...this.state[step],
                [field]: value
            }
        });
    }

    getSteps = () => {
        var steps = ['Información de pago', 'Forma de pago', 'Confirmación', 'Realizar pago'];
        steps.shift();
        return steps;
    }

    setAccount = (account) => {
        this.setState({ account });
    };

    getStepContent(stepIndex) {
        let indx =  stepIndex;

        switch (indx) {
        // case 0:
        //     return (
        //         <Step1
        //             changeState={this.updateModel.bind(this, 'step1')}
        //             formSubmitted={this.state.formSubmitted}
        //             state={this.state.step1}
        //         />
        //     );
        case 0:
            return (
                <Step2
                    changeState={this.updateModel.bind(this, 'step2')}
                    formSubmitted={this.state.formSubmitted}
                    nivel={this.state.step1.nivel}
                    campus={this.state.step1.campus}
                    setAccount={this.setAccount}
                    state={this.state.step2}
                />
            );
        case 1:
            return (
                <Step3
                    step1={this.state.step1}
                    step2={this.state.step2}
                    formSubmitted={this.state.formSubmitted}
                    handleChange={this.updateModel.bind(this, 'step3')}
                    account={this.state.account}
                    accept={this.state.step3.accept}
                    paymethod={this.state.step3.paymethod}
                />
            );
         case 2:
            return (
                <Step4
                    step1={this.state.step1}
                    step2={this.state.step2}
                    step3={this.state.step3}
                    account={this.state.account}
                    next={this.handleNext}
                />
            );
        default:
            return 'Unknown stepIndex';
      }
    }

    handleClickVariant = (msg, variant) => () => {
        // variant could be success, error, warning or info
        this.props.enqueueSnackbar(msg, { variant });
    };

    verify(step) {
        let model = this.state[step];
        var well = Object.keys(model).every(e => model[e] !== '');
        return well;
    }

    handleNext = () => {
        let formvalid = true;
        let formSubmitted = true;
        let step = this.state.activeStep  ;
        let newStep = this.state.activeStep;
    
        switch (step) {
            // case 0:
            //     formvalid = this.verify('step1');
            //     break;
            case 0:
                formvalid = this.verify('step2');
                break;
            case 1:
                formvalid = this.state.step3.accept;
                break;
            case 2:
                formvalid = false;
                this.handleReset();
                return;
            default:
        }

        if(formvalid) {
            newStep = newStep + 1;
            formSubmitted = false;
        }

        this.setState({
            formvalid,
            formSubmitted,
            activeStep: newStep
        });
    };

    handleBack = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1,
        }));
    };

    handleReset = () => {
        this.setState(defaultState);
    };

    render() {
        const { classes } = this.props;
        const steps = this.getSteps();
        const { activeStep } = this.state;

        return (
            <div className={classes.root}>
                <div className="Form-info">
                    <h5>Pago en Línea</h5>
                </div>
                <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map(label => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>
                <div /* className="Form-content" */>
                    {this.state.activeStep === steps.length ? (
                        <Grid container direction="column" justify="center" alignItems="center">
                            <h5>Proceso de pago completado</h5>
                            <Button variant="contained" 
                                    color="secondary"  
                                    className={classes.pimaryButton}
                                    onClick={this.handleReset}>
                                        Consultar saldo
                            </Button>
                        </Grid>
                    ) : (
                        <div className="Form-container">
                            {this.getStepContent(activeStep)}
                            <div className="Form-buttons">
                                <Button
                                    disabled={activeStep === 0}
                                    onClick={this.handleBack}
                                    className={classes.backButton}
                                >
                                    Atras
                                </Button>
                                <Button 
                                    variant="contained" 
                                    color="secondary" 
                                    onClick={this.handleNext} 
                                    className={classes.pimaryButton}
                                >
                                    {activeStep === steps.length - 1 ? 'Iniciar otro pago' : 'Siguiente'}
                                </Button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

PayForm.propTypes = {
    classes: PropTypes.object,
};
  
PayForm.propTypes = {
    //classes: PropTypes.object.isRequired,
    //getAccountStatus: PropTypes.func,
};
  
const mapStateToProps = ({ payForm }) => ({
    ...payForm
});
  
const mapDispatchToProps = {
  //getAccountStatus,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(withSnackbar(withStyles(styles)(PayForm)));
//export default withSnackbar(withStyles(styles)(HorizontalLabelPositionBelowStepper));

