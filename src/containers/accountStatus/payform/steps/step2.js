import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';
import { connect } from 'react-redux';
import  {URL_BASE}  from '../../../../constants';
import { getAccountStatus } from '../actions';
import { getAccessToken } from '../../../../helpers';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Step2 extends React.Component {
    state = {
        cuenta: null
    };

    componentDidMount= () => {
        this.getAccount();
    }

    getAccount() {
        const accessToken = getAccessToken();
        fetch(`${URL_BASE}api/estadocuenta/`, {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': accessToken,
                }
            })
            .then( res =>  res.json())
            .then( response => {
                this.setState({ 
                    cuenta: response.data,
                    concepto: response.data.descripcion,
                    cantidad: response.data.saldo
                 },() => {
                     this.props.setAccount(response.data);
                    }
                )
                return  response.data;
            }
            );
    }
    
    handleChange = (field, event) => {
        this.props.changeState(field, event.target.value);
        this.setState({[field]: event.target.value})
    };

    hasError = (field) => {
        if(this.props.formSubmitted) {
            if(this.props.state[field] === '') {
                return true;
            }
        }
        return false;
    };

    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.container}>
                {
                    (this.state.cantidad > 0) ? (
                        <Fragment>
                            <Grid item container xs={12} lg={6}>
                                <TextField
                                    id="standard-select-currency"
                                    className={classes.textField+' '+ classes.formControl}
                                    label="Concepto"
                                    error={this.hasError('concepto')}
                                    fullWidth
                                    value={(this.state.concepto == "Descripcion del mes, o puede estar vacio") ? "Adeudo" : this.state.concepto}
                                    helperText={this.hasError('concepto') ? "Este campo es requerido" : "Selecciona un concepto"}
                                    onChange={this.handleChange.bind(this, 'concepto')}
                                    inputProps= {{
                                        readOnly: true
                                    }}
                                    margin="normal"
                                >{(this.state.concepto == "Descripcion del mes, o puede estar vacio") ? "Adeudo" : this.state.concepto}
                                </TextField>
                            </Grid>
                            <Grid item container xs={12} lg={6}>
                                <TextField
                                    id="formatted-numberformat-input"
                                    className={classes.formControl}
                                    label="Cantidad a pagar"
                                    error={this.hasError('cantidad')}
                                    fullWidth
                                    value={this.state.cantidad}
                                    helperText={this.hasError('cantidad') ? "Este campo es requerido" : "Cantidad a pagar"}
                                    onChange={this.handleChange.bind(this, 'cantidad')}
                                    InputProps={{
                                    readOnly: true,
                                    inputComponent: NumberFormatCustom,
                                    }}
                                />
                            </Grid>
                </Fragment>
                ) : (
                    <Grid container justify="center" alignItems="center">
                            <h5>No tiene adeudos pendientes</h5>
                    </Grid>
                )
                }
            </Grid>
        );
    }
}

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

Step2.propTypes = {
  classes: PropTypes.object.isRequired,
  getAccountStatus: PropTypes.func,
  dispatch: PropTypes.func
};

const mapStateToProps = ({ payForm }) => ({
    ...payForm
});

const mapDispatchToProps = {
    getAccountStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Step2));
