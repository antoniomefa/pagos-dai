import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import { Grid } from '@material-ui/core';

const styles = {
    formControl: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    card: {
        minWidth: '85%',
    },
    title: {
        fontWeight: 800,
        flex: 0.2
    },
    title2: {
        flex: 0.8
    },
    flex: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    info: {
        display: 'flex',
        flex: 1
    },
    header: {
        textAlign: 'center',
        marginBottom: 12
    }
};

function SimpleCard(props) {
    const { classes } = props;

    const hasError = (field) => {
        if(props.formSubmitted) {
            if(!props[field]) {
                return true;
            }
        }
        return false;
    };
    
    const [value, setValue] = useState('tarjeta');
      
    function handleChange(event) {
        setValue(event.target.value);   
        props.handleChange('paymethod', event.target.value);
    }

    return (
        <Grid container direction="column" justify="center" alignItems="center" >
            <Card className={classes.card}>
                <CardContent>
                    <h1 className={classes.header}><Typography className={classes.title +' title-resp'} color="textSecondary" >Confirmar información</Typography></h1>
                    <div>

                        <div className={classes.info + ' content-resp'}>
                            <Typography className={classes.title +' title-resp'} >
                            Cliente: 
                            </Typography>
                            <Typography className={classes.title2 + ' title2-resp'} >
                                <small>{localStorage.username}</small>
                            </Typography>
                        </div>
                        <div className={classes.info + ' content-resp'}>
                            <Typography className={classes.title +' title-resp'} >
                                email:
                            </Typography>
                            <Typography className={classes.title2 + ' title2-resp'} >
                                <small>{localStorage.useremail}</small>
                            </Typography>
                        </div>
                        <div className={classes.info + ' content-resp'}>
                            <Typography className={classes.title +' title-resp'} >
                                Concepto:
                            </Typography>
                            <Typography className={classes.title2 + ' title2-resp'} >
                                <small>{(props.account.descripcion == "Descripcion del mes, o puede estar vacio") ? "Adeudo total" : props.account.descripcion}</small>
                            </Typography>
                        </div>
                    </div>
                        
                    <div className={classes.info + ' content-resp'}>
                        <Typography className={classes.title +' title-resp'} gutterBottom>
                            Cantidad:
                        </Typography>
                        <Typography className={classes.title2 + ' title2-resp'} gutterBottom>
                            <small>$ {props.account.saldo}</small>
                        </Typography>
                    </div>
                </CardContent>
                
                <CardActions className={classes.flex}>
                    <h2><Typography className={classes.title} color="textSecondary">Elige un método de pago</Typography></h2>
                </CardActions>

                <Grid container direction="row" justify="center">
                    <FormControl component="fieldset">
                        <RadioGroup aria-label="método de pago" name="position" value={props.paymethod} onChange={handleChange} row>
                            <FormControlLabel
                            value="tarjeta"
                            control={
                                <Radio color="primary" />}
                            label={<Typography>Pago con tarjeta</Typography>}
                            labelPlacement="bottom"
                            classes="style={font-size=5px}"
                            />
                            <FormControlLabel
                            value="oxxo"
                            control={
                                <Radio color="primary" />}
                            label="Pago en Oxxo"
                            labelPlacement="bottom"
                            />
                            <FormControlLabel
                            value="spei"
                            control={
                                <Radio color="primary" />}
                            label="Pago por SPEI"
                            labelPlacement="bottom"
                            />
                        </RadioGroup>
                    </FormControl>
                </Grid>
            </Card>

                <FormControl className={classes.formControl} error={hasError('accept')} fullWidth>
                        <div>
                            <Checkbox
                                checked={props.accept}
                                onChange={(e) => props.handleChange('accept', e.target.checked)}
                                value={'accept'}
                                color="primary"
                            />
                            <a href="https://alarmasdai.com/aviso.php" target={'_blank'}>
                                Acepto los términos y condiciones
                            </a>
                            {
                                hasError('accept') && (
                                    <FormHelperText id="component-error-text">Debes aceptar los términos</FormHelperText>
                                )
                            }
                        </div>
                </FormControl>
                   
        </Grid>
    );
}

SimpleCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);
