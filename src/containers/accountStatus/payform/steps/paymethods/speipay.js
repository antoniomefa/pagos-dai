import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import logo from '../../../../../../public/assets/images/spei_brand.png';
import { withSnackbar } from 'notistack';
import html2canvas from 'html2canvas';
import './oxxopay.css';
import { URL_BASE } from '../../../../../constants';
import { getAccessToken } from '../../../../../helpers';

const ZERO = 0;

class SpeiPay extends Component {
  
  constructor(...props) {
    super(...props)
    this.state = {
      amount: '',
      reference: ''
    }
  }
  
  loguedIn() {
    return !!localStorage.getItem('usr');
  };

  getConektaObj(amount) {
    var token = getAccessToken();
    var query = `{"action":"generacargo","tipoPago":"spei","cantidad":${amount}}`
    fetch(`${URL_BASE}/api/conekta`,
      {
        method: 'post',
        headers: {
        'Authorization': token,
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Accept-Encoding': 'gzip, deflate',
        'Connection': 'keep-alive'
        },
        body: query
      })
    .then( response => {
      if( response.status !== 200) {
        this.props.enqueueSnackbar("No se pudo obtener la referencia en este momento, intentalo más tarde.",
         { variant: 'warning', autoHideDuration: 6000 });
      }
      response.json().then( res => {
        if(typeof res == "string"){
          this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 3000 });
          throw res;
        } else if(res.data.charges.data[ZERO].payment_method.clabe!== undefined){
          this.setState({
            amount: res.data.amount,
            reference: res.data.charges.data[ZERO].payment_method.clabe
          })
        }
      })
    })
    .catch(err => {
      if (typeof err === 'object') {
          const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
          this.props.enqueueSnackbar(message500, { variant: 'warning', autoHideDuration: 3000 });
      } else if (typeof err === 'string') {
        this.props.enqueueSnackbar(err, { variant: 'warning', autoHideDuration: 3000 });
      }
      throw err;
    });
  }

  downloadImg() {
    html2canvas(document.querySelector('.opps'),{
      width: 475, height: 690, y: 260
  }).then(function(canvas) {
      var a = document.createElement('a');
        a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
        a.download = 'Ficha_Digital_Spei.jpg';
        a.click();
  });
  }

  componentDidMount() {
      this.getConektaObj(this.props.amount);
  }
    
  render(){
    return (
      <div>
        <div className="opps">
          <div className="opps-header">
            <div className="opps-reminder">Ficha digital. No es necesario imprimir.</div>
            <div className="opps-info">
              <div className="opps-brand"><img src={logo} alt="SPEI"/></div>
              <div className="opps-ammount">
                <h3>Monto a pagar</h3>
                <h2>${this.props.amount}<sup>MXN</sup></h2>
                <p>Utiliza exactamente esta cantidad al realizar el pago.</p>
              </div>
            </div>
            <div className="opps-reference">
              <h3>CLABE</h3>
              <h1 className="reference-number">{this.state.reference}</h1>
            </div>
          </div>
          <div className="opps-instructions">
            <h3>Instrucciones</h3>
            <ol>
              <li>Accede a tu banca en línea.</li>
              <li>Da de alta la CLABE en esta ficha. <strong>El banco deberá de ser STP</strong>.</li>
              <li>Realiza la transferencia correspondiente por la cantidad exacta en esta ficha, <strong>de lo contrario se rechazará el cargo</strong>.</li>
              <li>Al confirmar tu pago, el portal de tu banco generará un comprobante digital. <strong>En el podrás verificar que se haya realizado correctamente.</strong> Conserva este comprobante de pago.</li>
            </ol>
            <div className="opps-footnote">Al completar estos pasos recibirás un correo de <strong>DAI</strong> confirmando tu pago.</div>
          </div>
        </div>	
        <CardActions style={{justifyContent: 'center'}}>
          <Button variant="outlined" color="secondary" onClick={this.downloadImg}>Descargar ficha de pago</Button>
        </CardActions>
      </div>
    )
  }
}

export default withSnackbar(SpeiPay);