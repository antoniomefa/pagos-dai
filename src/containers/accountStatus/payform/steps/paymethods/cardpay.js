import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import { getAccessToken } from '../../../../../helpers';
import { URL_BASE } from '../../../../../constants';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Radio from '@material-ui/core/Radio';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';

import Paper from "@material-ui/core/Paper";

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import './oxxopay.css';
import amex from '../../../../../../public/assets/images/amex.png'
import visa from '../../../../../../public/assets/images/visa.png'
import master from '../../../../../../public/assets/images/mastercard.png'


const styles = theme => ({
    pimaryButton: {
        paddingTop: '5px',
        paddingBottom: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        color: 'white',
        backgroundColor: '#d11217',
    },
    cancelButton: {
        marginRight: theme.spacing.unit,
      },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    }
});


class CardPay extends Component {

    constructor(...props) {
        super(...props)
        this.state = {
            nombre: '',
            tarjeta: '',
            cvc: '',
            fechaExpMes: '',
            fechaExpAño: '',
            error: false,
            misTarjetas: {},
            paymentSourceId: '',
            agregarTarjetas: false,
            fullScreen: false
        }
      }

    handleChange(field, event) {
        this.setState({[field] : event.target.value});
    }

    handleUpdate(){
        var counter = 0;
        while(counter < 5){
            this.obtenerTarjetas();
            counter++;
        }
    }

    handleSubmit() {
        var tokenParams = {
            "card": {
              "number": this.state.tarjeta,
              "name": this.state.nombre,
              "exp_year": this.state.fechaExpAño,
              "exp_month": this.state.fechaExpMes,
              "cvc": this.state.cvc,
            //   "address": {
            //       "street1": "Calle 123 Int 404",
            //       "street2": "Col. Condesa",
            //       "city": "Ciudad de Mexico",
            //       "state": "Distrito Federal",
            //       "zip": "12345",
            //       "country": "Mexico"
            //    }
            }
          };
        Conekta.Token.create(tokenParams, this.conektaSuccessResponseHandler.bind(this), this.conektaErrorResponseHandler.bind(this));
    }

    conektaSuccessResponseHandler(token) {
        this.grabarTarjeta(token.id)
        this.setState({ agregarTarjetas: false })
        setTimeout(()=>{ this.handleUpdate()},500);
    };

    conektaErrorResponseHandler(response) {
        this.props.enqueueSnackbar(response.message_to_purchaser, { variant: 'warning', autoHideDuration: 10000, });
        this.setState({ error: true })
    };

    obtenerTarjetas() {
        var token = getAccessToken();
        fetch(`${URL_BASE}api/conekta?action=getpaymentsources`,
        {
            method: 'get',
            headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
            }
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No fue posible obtener las tarjetas en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 6000, });
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 6000, });
            throw res;
            } else {
                this.setState({ 
                    misTarjetas: res.data
                 })
            }
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });   
    }
    
    grabarTarjeta(tokenCard) {
        var query = `{"action":"grabatarjeta","token":"${tokenCard}"}`
        var token = getAccessToken();
        fetch(`${URL_BASE}api/conekta?action=grabatarjeta`,
        {
            method: 'post',
            headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
            },
            body: query
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No se pudo grabar la tarjeta en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 6000, });
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 6000, });
            throw res;
            } else if(res.data.id!== undefined){
                this.props.enqueueSnackbar("Tarjeta grabada", { variant: 'success', autoHideDuration: 6000, });
                this.setState({ 
                    paymentSourceId: res.data.id
                 })
            }
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });  
    }

    sendConektaObj(amount, paymentSourceId) {
        if(paymentSourceId==''){
            this.props.enqueueSnackbar("Selecciona una tarjeta",
            { variant: 'warning', autoHideDuration: 6000, });
            return 0;
        }
        var query = `{"action":"generacargo","tipoPago":"card","cantidad":"${amount}","payment_source_id":"${paymentSourceId}"}`
        fetch(`${URL_BASE}/api/conekta`,
        {
            method: 'post',
            headers: {
            'Authorization': getAccessToken(),
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'keep-alive'
            },
            body: query
        })
        .then( response => {
        if( response.status !== 200) {
            this.props.enqueueSnackbar("No se pudo realizar el cobro en este momento, intentalo más tarde.",
            { variant: 'warning', autoHideDuration: 6000, });
        }
        response.json().then( res => {
            if(typeof res == "string"){
            this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 6000, });
            throw res;
            } else if(res.data.charges.data[0].payment_method.auth_code !== undefined){
                this.props.enqueueSnackbar("Cobro exitoso!", { variant: 'success', autoHideDuration: 6000, });
                this.props.next();
            }
        })
        })
        .catch(err => {
        if (typeof err === 'object') {
            const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
            this.props.enqueueSnackbar(message500, { variant: 'warning' });
        } else if (typeof err === 'string') {
            this.props.enqueueSnackbar(err, { variant: 'warning' });
        }
        throw err;
        });   
    }

    hasError = (field) => {
        if(this.state.error)
            if(this.state[field] === '') {
                return true;
            }
        return false;
    };

    showIcon(brand){
        switch(brand){
            case 'visa': 
                return(
                    <div>
                        <img src={visa} height="20px" width="30px"/>
                    </div>
                ) ;
            case 'mastercard': 
                return(
                    <div>
                        <img src={master} height="20px" width="30px"/>
                    </div>
                );
            case 'american_express': 
                return(
                    <div>
                        <img src={amex} height="20px" width="30px"/>
                    </div>
                );
            default: return 'no disponible';
        }
    }

    showAddCards(){
        let maxWidth = false
        if(window.innerWidth < 600){
            maxWidth = true
        }
        this.setState({ 
            agregarTarjetas: !this.state.agregarTarjetas,
            fullScreen: maxWidth })
    }

    componentDidMount = () => {
        Conekta.setPublicKey("key_V2f81Aytvb4R2CsXuF8Wyqg"); // Llave de produccion
        this.handleUpdate();
    }
    
    render() {
        const { classes } = this.props;
        const rows = this.state.misTarjetas;
        const amount = this.props.account.saldo;
        return ( 
            <Fragment>
                {
                    ( rows.length> 0) && (!this.state.agregarTarjetas) && (
                        <Grid container xs={12}>
                            <Grid item xs={12}>
                                <h5>Seleccione una tarjeta</h5>
                            </Grid>

                            <Grid item xs={12}>
                                <Paper >
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="center">Selecciona</TableCell>
                                                <TableCell>Brand</TableCell>
                                                <TableCell align="center">Terminación</TableCell>
                                                <TableCell align="center">Expira</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {rows.map(row => (
                                                <TableRow key={row.id}>
                                                    <TableCell component="th" scope="row">
                                                        <Radio
                                                            value={row.id}
                                                            checked={row.id != this.state.paymentSourceId ? false : true}
                                                            onChange={this.handleChange.bind(this, 'paymentSourceId')}
                                                        />
                                                    </TableCell>
                                                    <TableCell align="center">{this.showIcon(row.brand)}</TableCell>
                                                    <TableCell align="center">{row.last4}</TableCell>
                                                    <TableCell align="center">{row.exp_month}/{row.exp_year}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </Paper>
                            </Grid>

                            <Grid container direction="row" justify="space-between" alignItems="flex-end">
                                <Grid item xs={12} sm={6}>
                                    <Button color="secondary"  onClick={()=>this.showAddCards()}>Agregar otra tarjeta</Button>
                                </Grid>
                                <Grid container xs={12} sm={6} justify="flex-end" alignItems="flex-end" spacing={2}>
                                    <Grid item xs={6} sm={3}>
                                        <Typography variant="subtitle1">Total: $ {amount}</Typography>
                                    </Grid>
                                    <Grid item xs={6} sm={3}>
                                        <Button variant="contained" color="secondary" className={classes.pimaryButton} onClick={()=>this.sendConektaObj(amount, this.state.paymentSourceId)}>Pagar</Button>
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>
                    )
                }
                
                {
                    (!rows.length>0) && (!this.state.agregarTarjetas) &&(
                        <Grid container direction="column" justify="space-between" alignItems="center">
                                <h5>No tiene tarjetas registradas</h5>
                                <Button color="secondary" onClick={()=>this.showAddCards()}>Agregar una tarjeta</Button>
                        </Grid>
                    )
                }
                
                {        
                    ( this.state.agregarTarjetas == true ) && (

                        <Dialog open={this.state.agregarTarjetas} onClose={()=>this.showAddCards()} fullWidth={true} fullScreen={this.state.fullScreen} maxWidth="sm" aria-labelledby="form-dialog-title">
                            <DialogTitle id="form-dialog-title">Resgistre los datos de la tarjeta</DialogTitle>
                            <DialogContent>
                                <Grid container className={classes.container}>   
                                    <Grid item container xs={12} lg={12}>
                                        <FormControl fullWidth className={classes.formControl} error={this.hasError('nombre')}>
                                            <InputLabel>Nombre del tarjetahabiente</InputLabel>
                                            <Input
                                                type="text"
                                                id="nombre"
                                                value={this.state.nombre}
                                                onChange={this.handleChange.bind(this, 'nombre')}                            
                                            />
                                            { 
                                                this.hasError('nombre') ? (
                                                    <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                                ) : (
                                                    <FormHelperText id="component-error-text">Tal como aparece en la tarjeta</FormHelperText>
                                                )
                                            }
                                        </FormControl>
                                    </Grid>
                                            
                                    <Grid item container xs={8} lg={8}>
                                        <FormControl fullWidth className={classes.formControl} error={this.hasError('tarjeta')}>
                                            <InputLabel>Número de la tarjeta</InputLabel>
                                            <Input
                                                type="number"
                                                id="tarjeta"
                                                value={this.state.tarjeta}
                                                onChange={this.handleChange.bind(this, 'tarjeta')}
                                                        
                                            />
                                            {
                                                this.hasError('tarjeta') ? (
                                                    <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                                ) : (
                                                    <FormHelperText id="component-error-text">Escribe los 16 dígitos de la tarjeta</FormHelperText>
                                                )
                                            }
                                        </FormControl>
                                    </Grid>

                                    <Grid item container xs={4} lg={4}>
                                        <FormControl className={classes.formControl} fullWidth error={this.hasError('cvc')}>
                                            <InputLabel>CVC</InputLabel>
                                            <Input
                                                type="text"
                                                id="cvc"
                                                value={this.state.cvc}
                                                onChange={this.handleChange.bind(this, 'cvc')}
                                        />
                                            {
                                                this.hasError('cvc') ? (
                                                    <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                                ) : (
                                                    <FormHelperText id="component-error-text">Leer al reverso de la tarjeta</FormHelperText>
                                                )
                                            }
                                        </FormControl>
                                    </Grid>
            
                                    <Grid item container xs={6} lg={6}>
                                        <FormControl className={classes.formControl} fullWidth error={this.hasError('fechaExpMes')}>
                                            <InputLabel>Mes de expiración</InputLabel>
                                            <Input
                                                type="number"
                                                id="fechaExpMes"
                                                value={this.state.fechaExpMes}
                                                onChange={this.handleChange.bind(this, 'fechaExpMes')}
                                            />
                                            {
                                                this.hasError('fechaExpMes') ? (
                                                    <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                                ) : (
                                                    <FormHelperText id="component-error-text">A 2 dígitos (MM)</FormHelperText>
                                                )
                                            }
                                        </FormControl>
                                    </Grid>
            
                                    <Grid item container xs={6} lg={6}>
                                        <FormControl className={classes.formControl} fullWidth error={this.hasError('fechaExpAño')}>
                                            <InputLabel>Año de expiracón</InputLabel>
                                            <Input
                                                type="number"
                                                id="fechaExpAño"
                                                value={this.state.fechaExpAño}
                                                onChange={this.handleChange.bind(this, 'fechaExpAño')}
                                            />
                                            {
                                                this.hasError('fechaExpAño') ? (
                                                    <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                                                ) : (
                                                    <FormHelperText id="component-error-text">A 4 dígitos (AAAA)</FormHelperText>
                                                )
                                            }
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </DialogContent>
                            <DialogActions>
                                <Button color="secondary" className={classes.cancelButton} onClick={()=>this.showAddCards()}>
                                    Cancel
                                </Button>
                                <Button variant="contained" color='secondary' className={classes.pimaryButton} onClick={this.handleSubmit.bind(this)}>
                                    Guardar tarjeta
                                </Button>
                            </DialogActions>
                        </Dialog>
                    )
                }
            </Fragment>
        );
    }
    
}

export default withStyles(styles)(withSnackbar(CardPay))