import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import json from '../oferta.json';
import NumberFormat from 'react-number-format';
import MaskedInput from 'react-text-mask';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Step1 extends React.Component {

    componentDidMount() {
        this.forceUpdate();
    }

    handleChange = (field, event) => {
        let value = event.target.value;
        this.props.changeState(field, value);
    };

    hasError = (field) => {
        if(this.props.formSubmitted) {
            if(this.props.state[field] === '') {
                return true;
            }
        }

        return false;
    };

    render() {
        const { classes } = this.props;

        return (
            <Grid container className={classes.container}>
                <Grid item container xs={12} lg={6}>
                    <FormControl fullWidth className={classes.formControl} error={this.hasError('nombres')}>
                        <InputLabel htmlFor="component-simple">Nombres</InputLabel>
                        <Input
                            id="component-simple"
                            value={this.props.state.nombres}
                            onChange={this.handleChange.bind(this, 'nombres')}
                            aria-describedby="component-helper-text"
                        />
                        {
                            this.hasError('nombres') ? (
                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                            ) : (
                                <FormHelperText id="component-error-text">Escribe tus nombres</FormHelperText>
                            )
                        }
                    </FormControl>
                </Grid>
                <Grid item container xs={12} lg={6}>
                    <FormControl fullWidth className={classes.formControl} error={this.hasError('apellidos')}>
                        <InputLabel htmlFor="component-helper">Apellidos</InputLabel>
                        <Input
                            id="component-helper"
                            value={this.props.state.apellidos}
                            onChange={this.handleChange.bind(this, 'apellidos')}
                            aria-describedby="component-helper-text"
                        />
                        {
                            this.hasError('apellidos') ? (
                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                            ) : (
                                <FormHelperText id="component-error-text">Escribe tus apellidos</FormHelperText>
                            )
                        }
                    </FormControl>
                </Grid>
                <Grid item container xs={12} lg={6}>
                    <FormControl fullWidth className={classes.formControl} error={this.hasError('correo')}>
                        <InputLabel htmlFor="component-error">Correo electrónico</InputLabel>
                        <Input
                            id="component-error"
                            value={this.props.state.correo}
                            onChange={this.handleChange.bind(this, 'correo')}
                            aria-describedby="component-error-text"
                        />
                        {
                            this.hasError('apellidos') ? (
                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                            ) : (
                                <FormHelperText id="component-error-text">Escribe tu correo electrónico</FormHelperText>
                            )
                        }
                    </FormControl>
                </Grid>
                <Grid item container xs={12} lg={6}>
                    <FormControl className={classes.formControl} fullWidth error={this.hasError('telefono')}>
                        <InputLabel htmlFor="telefono">Teléfono</InputLabel>
                        <Input
                            value={this.props.state.telefono}
                            onChange={this.handleChange.bind(this, 'telefono')}
                            id="telefono"
                            inputComponent={TextMaskCustom}
                        />
                        {
                            this.hasError('apellidos') ? (
                                <FormHelperText id="component-error-text">Este campo es requerido</FormHelperText>
                            ) : (
                                <FormHelperText id="component-error-text">Escribe tu teléfono</FormHelperText>
                            )
                        }
                    </FormControl>
                </Grid>
                <Grid item container xs={12} lg={6}>
                    <TextField
                        id="standard-select-currency"
                        select
                        error={this.hasError('nivel')}
                        fullWidth
                        label="Nivel académico"
                        className={classes.textField+' '+classes.formControl}
                        value={this.props.state.nivel}
                        onChange={this.handleChange.bind(this, 'nivel')}
                        SelectProps={{
                          MenuProps: {
                            className: classes.menu,
                          },
                        }}
                        helperText={this.hasError('apellidos') ? "Este campo es requerido" : "Selecciona tu nivel académico"}
                        margin="normal"
                    >
                        {
                            Object.keys(json.Oferta).map(d => (
                                <MenuItem key={d} value={d}>
                                    {json.Oferta[d]}
                                </MenuItem>
                            ))
                        }
                  </TextField>
                </Grid>
                {
                    this.props.state.nivel && (
                        <Grid item container xs={12} lg={6}>
                            <TextField
                                id="standard-select-currency"
                                select
                                error={this.hasError('carrera')}
                                fullWidth
                                label="Carrera"
                                className={classes.textField+' '+classes.formControl}
                                value={this.props.state.carrera}
                                onChange={this.handleChange.bind(this, 'carrera')}
                                SelectProps={{
                                  MenuProps: {
                                    className: classes.menu,
                                  },
                                }}
                                helperText={this.hasError('apellidos') ? "Este campo es requerido" : "Selecciona tu carrera"}
                                margin="normal"
                             >
                                {
                                    Object.keys(json.Careers[this.props.state.nivel]).map(d => (
                                        <MenuItem key={d} value={d}>
                                          {json.Careers[this.props.state.nivel][d]}
                                        </MenuItem>
                                    ))
                                }
                          </TextField>
                        </Grid>
                    )
                }
                {
                    this.props.state.carrera && (
                        <Grid item container xs={12} lg={6}>
                            <TextField
                                id="standard-select-currency"
                                select
                                fullWidth
                                error={this.hasError('modalidad')}
                                label="Modalidad de estudio"
                                className={classes.textField+' '+classes.formControl}
                                value={this.props.state.modalidad}
                                onChange={this.handleChange.bind(this, 'modalidad')}
                                SelectProps={{
                                  MenuProps: {
                                    className: classes.menu,
                                  },
                                }}
                                helperText={this.hasError('apellidos') ? "Este campo es requerido" : "Selecciona tu modalidad"}
                                margin="normal"
                            >
                                {
                                    Object.keys(json.Modes[this.props.state.carrera]).map(d => (
                                        <MenuItem key={d} value={d}>
                                            {json.Modes[this.props.state.carrera][d]}
                                        </MenuItem>
                                    ))
                                }
                            </TextField>
                        </Grid>
                    )
                }
                {
                    this.props.state.modalidad && (
                        <Grid item container xs={12} lg={6}>
                            <TextField
                                id="standard-select-currency"
                                select
                                fullWidth
                                error={this.hasError('campus')}
                                label="Campus"
                                className={classes.textField+' '+classes.formControl}
                                value={this.props.state.campus}
                                onChange={this.handleChange.bind(this, 'campus')}
                                SelectProps={{
                                  MenuProps: {
                                    className: classes.menu,
                                  },
                                }}
                                helperText={this.hasError('apellidos') ? "Este campo es requerido" : "Selecciona tu campus"}
                                margin="normal"
                            >
                                {
                                    Object.keys(json.Campuses[this.props.state.carrera]).map(d => (
                                        <MenuItem key={d} value={d}>
                                            {json.Campuses[this.props.state.carrera][d]}
                                        </MenuItem>
                                    ))
                                }
                            </TextField>
                        </Grid>
                    )
                }
            </Grid>
        );
    }
}

function NumberFormatCustom(props) {
  const { inputRef, onChange, field, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            [field]: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
    />
  );
}

function TextMaskCustom(props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={ref => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
      showMask={false}
    />
  );
}

TextMaskCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
};

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

Step1.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Step1);
