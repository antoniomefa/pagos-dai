import { ON_GET_ACCOUNT_STATUS_SUCCESS } from './types';
import { getAccessToken } from '../../../helpers';
import { URL_BASE } from 'source/constants';

export function getAccountStatus() {
    const accessToken = getAccessToken();
    fetch(`${URL_BASE}/api/estadocuenta/`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': accessToken,
            }
        })
        .then( res =>  res.json())
        .then( response => {
            dispatch({
                type: ON_GET_ACCOUNT_STATUS_SUCCESS,
                payload: {
                    data: response.data
                }
            });
        });
}
