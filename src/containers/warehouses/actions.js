import {
    ON_GET_WAREHOUSES_SUCCESS,
    ON_FETCH_WAREHOUSES,
    ON_SET_FILTERS,
    ON_DELETE_WAREHOUSE
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { downloadB64 } from 'helpers';
import { sendNotification } from 'actions';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_WAREHOUSES });

        Service(query, 'api/warehouses', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_WAREHOUSES_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });


    };
}

export const deleteWarehouses = (warehouses, withBack = false) => dispatch => {
    warehouses.forEach(id => {
        Service({}, `api/warehouses/${id}`, 'delete')
            .then(({ data }) => {
                dispatch({ type: ON_DELETE_WAREHOUSE, id: data.id });
                if (withBack) {
                    dispatch(push('/warehouses'));
                }
            })
            .catch(() => {
                // error al eliminar al usuario
            });
    });
};

export function downloadFile(url) {
    return () => {

        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                downloadB64(mime, name, data);
            })
            .catch(() => {
                //
            });


    };
}

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goCreate() {
    return dispatch => {
        dispatch(push('/warehouses/create'));
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/warehouses/${id}`));
    };
}
