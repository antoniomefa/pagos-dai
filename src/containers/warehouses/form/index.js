import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, createWarehouse, updateWarehouse, getProfile, cancelForm } from './actions';
import UploadFile from 'components/uploadFile';
import Tabs from 'components/tabs';
import DraftEditor from 'components/draftEditor';

class FormUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 1,
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            image_url: '',
            formValues: {
                title: '',
                content: null,
                warehouses_reception_schedule: null,
                tax_documents_reception_schedule: null
            },
            roles: []
        };
    }

    componentDidMount() {
        if (this.__isEditing()) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;

        if (nextProps.warehouse.id) {

            const fieldTax = 'tax_documents_reception_schedule';
            const fieldReception = 'warehouses_reception_schedule';

            formValues.title = nextProps.warehouse.title;
            formValues[fieldReception] = nextProps.warehouse[fieldReception];
            formValues[fieldTax] = nextProps.warehouse[fieldTax];
            formValues.content =  nextProps.warehouse.content;

            this.setState({
                formValues,
                image_url: nextProps.warehouse.image_url
            });
        }
    }

    getContentEditor = () => {
        return (
            <DraftEditor
                className="col-12"
                parentStyle={{ marginTop: 20, marginBottom: 20 }}
                isEditing={this.__isEditing()}
                content={this.state.formValues.content}
                changeEditorContent={this.changeEditorContent.bind(this, 'content')}
            />
        );
    }

    getReceptionScheduleEditor = () => {
        const field = 'warehouses_reception_schedule';
        return (
            <DraftEditor
                className="col-12"
                parentStyle={{ marginTop: 20, marginBottom: 20 }}
                isEditing={this.__isEditing()}
                content={this.state.formValues[field]}
                changeEditorContent={this.changeEditorContent.bind(this, field)}
            />
        );
    }

    getTaxReceptionScheduleEditor = () => {
        const field = 'tax_documents_reception_schedule';
        return (
            <DraftEditor
                className="col-12"
                parentStyle={{ marginTop: 20, marginBottom: 20 }}
                isEditing={this.__isEditing()}
                content={this.state.formValues[field]}
                changeEditorContent={this.changeEditorContent.bind(this, field)}
            />
        );
    }

    changeEditorContent = (field, content) => {
        const formValues = this.state.formValues;

        formValues[field] = content;

        this.setState({ formValues });
    }

    getTabs = () => [
        {
            id: 1,
            title: 'Contenido',
            Component: this.getContentEditor()
        },
        {
            id: 2,
            title: 'Horario de recepcion de almacen',
            Component: this.getReceptionScheduleEditor()
        },
        {
            id: 3,
            title: 'Horario de recepcion de documentos fiscales',
            Component: this.getTaxReceptionScheduleEditor()
        }
    ]

    toggle = tab => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        return ['title'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        return [
            /^[a-zA-Z ]+$/
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const { formValues } = this.state;
        const tax_documents_reception_schedule = formValues['tax_documents_reception_schedule'];
        const warehouses_reception_schedule = formValues['warehouses_reception_schedule'];

        const form = {
            title: this.state.formValues['title'],
            content: this.state.formValues['content'],
            warehouses_reception_schedule,
            tax_documents_reception_schedule,
            image_url: this.state.image_url
        };

        if (this.__isEditing()) {
            this.props.updateWarehouse({ ...form, id: this.props.warehouse.id });
        } else {
            this.props.createWarehouse(form);
        }

    }

    getOptionsRol = () => {
        return this.state.roles.map(r => ({ value: r.id, label: r.name }));
    }

    onFileUploaded = image_url => {
        this.setState({ image_url });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Nombre"
                        htmlFor="title"
                        icon="user"
                        requested={formRequested}
                        id="title"
                        onChange={this.onChange.bind(this, 'title')}
                        onBlur={this.onBlur.bind(this, 'title')}
                        value={formValues['title']}
                        placeholder="Escribe el nombre el almacen..."
                        aria="title"
                        hasError={!!errorMessages['title']}
                        invalidText={errorMessages['title']}
                        required
                        isGroup
                        hasIcon
                    />
                    <UploadFile
                        onFileUploaded={this.onFileUploaded}
                        previousName={this.state.image_url}
                        types="image/x-png,image/gif,image/jpeg"
                    />
                </div>
                <Tabs
                    activeTab={this.state.activeTab}
                    toggle={this.toggle}
                    tabs={this.getTabs()}
                />
                <div className="pull-right row">
                    <Button
                        text="Cancelar"
                        type="dark"
                        outline
                        onButtonPress={this.props.cancelForm}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
                <small>
                    Los campos marcados con <i className="isRequired">*</i> son requeridos
                </small>
            </form>
        );
    }
}

FormUser.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    getProfile: PropTypes.func,
    updateWarehouse: PropTypes.func,
    warehouse: PropTypes.object,
    cancelForm: PropTypes.func,
    createWarehouse: PropTypes.func
};

const mapStateToProps = ({ warehouseForm }) => ({
    ...warehouseForm
});

const mapDispatchToProps = {
    goEdit,
    createWarehouse,
    getProfile,
    updateWarehouse,
    cancelForm
};

export default connect(mapStateToProps, mapDispatchToProps)(FormUser);
