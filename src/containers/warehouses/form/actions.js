import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_WAREHOUSE } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createWarehouse = data => dispatch => {
    Service(JSON.stringify(data), 'api/warehouses', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Almacen creado correctamente',
                color: 'primary',
                alias: 'created-warehouse',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear el almacen',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/warehouses/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_WAREHOUSE, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Usuario no encontrado',
                color: 'danger',
                alias: 'get-user',
                time: 3
            }));
            dispatch(push('/warehouses'));
        });
};

export const updateWarehouse = data => dispatch => {
    Service(JSON.stringify(data), 'api/warehouses', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
