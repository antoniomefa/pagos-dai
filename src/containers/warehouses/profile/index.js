import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import { deleteWarehouses } from '../actions';
import { downloadFile, notification } from 'containers/warehouses/actions';

class ProfileWarehouse extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {
        this.props.deleteWarehouses([this.props.routeParams.id], true);
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    onDownload = () => {
        const { warehouse, downloadFile, notification } = this.props;
        if (warehouse.image_url !== '') downloadFile(warehouse.image_url);
        else notification('No existe archivo para descargar.');
    }

    render() {
        const { warehouse } = this.props;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h5>{warehouse.title}</h5>
                    </div>
                    <div className="__card__body">

                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Eliminar"
                            icon="trash-o"
                            isIcon
                            type="danger"
                            outline
                            onButtonPress={this.onDelete}
                            permission={'delete'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Descargar"
                            type="primary"
                            icon="download"
                            isIcon
                            onButtonPress={this.onDownload}
                            permission={'download'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ProfileWarehouse.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    downloadFile: PropTypes.func,
    warehouse: PropTypes.object,
    notification: PropTypes.func,
    deleteWarehouses: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ warehouseProfile }) => ({
    ...warehouseProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    deleteWarehouses,
    downloadFile,
    notification
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileWarehouse);
