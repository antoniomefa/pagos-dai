import { ON_GET_WAREHOUSE } from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/warehouses/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_WAREHOUSE, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Almacen no encontrado',
                color: 'danger',
                alias: 'get-warehouse',
                time: 3
            }));
            dispatch(push('/warehouses'));
        });
};
