import { ON_GET_WAREHOUSE } from './types';

const initialState = {
    warehouse: {}
};

export default function userProfileReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_WAREHOUSE:
            return {
                ...state,
                warehouse: action.data
            };
        default:
            return { ...state };
    }
}
