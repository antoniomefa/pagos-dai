
export const ON_FETCH_WAREHOUSES = 'App/Providers/ON_FETCH_WAREHOUSES';
export const ON_GET_WAREHOUSES_SUCCESS = 'App/Providers/ON_GET_WAREHOUSES_SUCCESS';
export const ON_GET_WAREHOUSES_FAIL = 'App/Providers/ON_GET_WAREHOUSES_FAIL';
export const ON_SET_FILTERS = 'App/Providers/ON_SET_FILTERS';
export const ON_DELETE_WAREHOUSE = 'App/Providers/ON_DELETE_WAREHOUSE';
