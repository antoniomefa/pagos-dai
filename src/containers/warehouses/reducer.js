import {
    ON_GET_WAREHOUSES_SUCCESS,
    ON_GET_WAREHOUSES_FAIL,
    ON_FETCH_WAREHOUSES,
    ON_SET_FILTERS,
    ON_DELETE_WAREHOUSE
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    loading: false
};

export default function providersReducer(state = initialState, action) {
    const ZERO = 0;
    const ONE = 1;

    var index = state.rows.findIndex(r => r.id === action.id);

    switch (action.type) {
        case ON_FETCH_WAREHOUSES:
            return {
                ...state,
                loading: true
            };
        case ON_DELETE_WAREHOUSE:
            return {
                ...state,
                rows: [
                    ...state.rows.slice(ZERO, index),
                    ...state.rows.slice(index + ONE)
                ]
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_GET_WAREHOUSES_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_WAREHOUSES_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
