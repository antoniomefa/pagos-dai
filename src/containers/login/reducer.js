import { ON_LOGIN_FAILED, ON_LOGIN_SUCCESS, ON_RESET_FORM } from './types';

const initialState = {
    requested: false,
    loguedIn: false,
    hasError: false,
    hasErrorText: '',
    type: ''
};

export default function loginReducer(state = initialState, action) {
    switch (action.type) {
        case ON_LOGIN_FAILED:
            return {
                ...state,
                hasError: true,
                loguedIn: false,
                requested: true,
                hasErrorText: action.hasErrorText
            };
        case ON_RESET_FORM:
            return { ...initialState };
        case ON_LOGIN_SUCCESS:
            return {
                ...state,
                loguedIn: true,
                hasError: false,
                requested: true
            };
        default:
            return { ...state };
    }
}
