import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { showAlert, sendNotification, showNotice } from 'actions';
import { goTo, goToab, auth, initForm } from './actions';
import jQuery from 'jquery';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: 'Bienvenido',
            loading: false,
            requested: false,
            username: '',
            password: ''
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.hasError) {
            const fadeOutTime = 500;
            setTimeout(() => {
                this.setState({
                    text: 'Bienvenido',
                    loading: false,
                    requested: false
                }, () => {
                    jQuery('form').fadeIn(fadeOutTime);
                    jQuery('.wrapper').removeClass('form-success');
                    this.props.initForm();
                    this.showNotification({
                        message: nextProps.hasErrorText,
                        level: 'warning'
                    });
                });
            }, fadeOutTime + fadeOutTime);

        } else if (nextProps.loguedIn) {
            // logued in
        }
    }

    showAlert = () => {
        this.props.showAlert({
            color: 'success',
            text: 'Hola mundo',
            time: 3,
            alias: 'hola-mundo'
        });
    }

    showNotification = (payload) => {
        this.props.sendNotification({
            ...payload
        });
    }

    goToRegister = () => {
        this.props.goTo();
    }

    notice = () => {
        const notificationOpts = {
            // uid: 'once-please', // you can specify your own uid if required
            title: 'Hey!',
            message: 'Ahora ya sabes usar notificaciones!',
            position: 'br',
            autoDismiss: 0,
            action: {
                label: 'Click me!!',
                callback: () => alert('clicked!')
            }
        };

        this.props.showNotice(notificationOpts);
    }

    goToAbout = () => {
        this.props.goToab();
    }

    onChange = (field, e) => {
        const state = this.state;

        state[field] = e.target.value;

        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.checkRegex();
    }

    checkRegex = () => {
        /* eslint-disable */
        var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        /* eslint-enable */
        if (!emailRegex.test(this.state.username)) {
            this.showNotification({
                message: 'No es un correo valido',
                level: 'error'
            });
            return;
        }

        this.setState({
            requested: true,
            loading: true,
            text: 'Cargando...'
        }, () => {
            const fadeOutTime = 500;
            jQuery('form').fadeOut(fadeOutTime);
            jQuery('.wrapper').addClass('form-success');

            this.props.auth(this.state.username, this.state.password);
        });
    }

    filedsRequired = () => [this.state.username, this.state.password]

    allFieldsRequiredFilled = () => this.filedsRequired().every(f => f != '')

    canSubmit = () => !this.allFieldsRequiredFilled()

    render() {
        return (
            <div className="wrapper">
                <div className="container">
                    <div
                        className="__welcome_text"
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <img src="/assets/images/logo.png" width="50" />
                        <h1 className="__welcome">{this.state.text}</h1>
                    </div>
                    <small className="__title_app">
                        DAI ALARMAS  {this.state.loading && (
                            <span className="fa fa-spinner fa-spin" />
                        )}
                    </small>

                    <form className="form" onSubmit={this.onSubmit}>
                        <input
                            type="text"
                            onChange={this.onChange.bind(this, 'username')}
                            value={this.state.username}
                            placeholder="Username"
                        />
                        <input
                            type="password"
                            onChange={this.onChange.bind(this, 'password')}
                            value={this.state.password}
                            placeholder="Password"
                        />
                        <button
                            type="submit"
                            disabled={this.canSubmit()}
                        >
                            Login
                        </button>
                    </form>
                </div>

                <ul className="bg-bubbles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        );
    }
}

Login.propTypes = {
    showAlert: PropTypes.func,
    sendNotification: PropTypes.func,
    goTo: PropTypes.func,
    goToab: PropTypes.func,
    auth: PropTypes.func,
    initForm: PropTypes.func,
    showNotice: PropTypes.func
};

const mapStateToProps = ({ login }) => ({
    ...login
});

const mapDispatchToProps = {
    showAlert,
    sendNotification,
    goTo,
    goToab,
    showNotice,
    initForm,
    auth
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
