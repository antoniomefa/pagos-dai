import { ON_LOGIN_SUCCESS } from 'action-types/auth';
import { ON_RESET_FORM, ON_LOGIN_FAILED } from './types';
import { routerActions, push } from 'react-router-redux';
import Service from 'helpers/service';
import { createSession } from 'helpers';

export function goTo() {
    return dispatch => {
        dispatch(routerActions.push('/sign-up'));
    };
}

export function goToab() {
    return dispatch => {
        dispatch(push('/about-us'));
    };
}

export const auth = (username, password) =>  dispatch => {
    const data = JSON.stringify({ username, password: btoa(password) });
    Service(data, 'auth/login', 'post')
        .then(data => {
            //console.log(data.devicetoken_id)
            createSession(data);
            dispatch({ type: ON_LOGIN_SUCCESS, payload: data });
        })
        .catch(error => {
            dispatch({
                type: ON_LOGIN_FAILED,
                hasErrorText: error || 'Error inesperado del sistema'
            });
        });
};

export const initForm = () => ({ type: ON_RESET_FORM });
