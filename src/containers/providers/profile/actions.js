import { browserHistory } from 'react-router';
import { ON_GET_PROVIDER } from './types';
import { push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/providers/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PROVIDER, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Proveedor no encontrado',
                color: 'danger',
                alias: 'get-provider',
                time: 3
            }));
            dispatch(push('/providers'));
        });
};
