import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import Contacts from 'containers/contacts';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onDelete = () => {

    }

    onUpdate = () => {
        this.props.goEdit();
    }

    _getContactsText = () => {
        const ONE = 1;
        const { provider } = this.props;

        if (provider.contacts.length == ONE) return '1 contacto';

        return `${provider.contacts.length} contactos`;
    }

    render() {
        const { provider } = this.props;

        if (!provider) return null;
        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card_provider">
                    <div className="__card__header">
                        <h5>{provider.bussiness_name}</h5>
                    </div>
                    <div className="__card__body">
                        <p><strong>Codigo:</strong>  {provider.code}</p>
                        <p><strong>RFC:</strong>  {provider.rfc}</p>
                        {
                            !provider.user ? (
                                <p>
                                    <strong>Usuario:</strong>  {'Sin usuario'}
                                </p>
                            ) : (
                                <p>
                                    <strong>Usuario:</strong>
                                    {`   ${provider.user.name}(${provider.user.email})`}
                                </p>
                            )
                        }
                        <p><strong>Contactos:</strong>  {this._getContactsText()}</p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Eliminar"
                            icon="trash-o"
                            isIcon
                            type="danger"
                            outline
                            onButtonPress={() => {}}
                            permission={'delete'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                    </div>
                    <Contacts
                        {...this.props}
                        contacts={provider.contacts} />
                </div>
            </div>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    getProfile: PropTypes.func,
    routeParams: PropTypes.object,
    provider: PropTypes.object,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ providerProfile }) => ({
    ...providerProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
