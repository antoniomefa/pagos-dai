import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import { getData, addQuery, setFilters, goProfile, sync } from './actions';

class Providers extends Component {
    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.isSync) {
            this.props.getData(this.props.query);
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Código', field: 'code'},
            {name: 'Razon social', field: 'bussiness_name'},
            {name: 'RFC', field: 'rfc'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        return [];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __syncProviders = () => {
        this.props.sync();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'sync', label: 'sincronizar', style: 'outline-secondary',
                permission: 'sync',
                action: this.__syncProviders, tooltip: 'sincronizar proveedores'
            }
        ];
    }

    __getMasiveActions = () => {
        return [];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

Providers.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    sync: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

Providers.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ providers }) => ({
    ...providers
});

const mapDispatchToProps = {
    getData,
    addQuery,
    setFilters,
    goProfile,
    sync
};

export default connect(mapStateToProps, mapDispatchToProps)(Providers);
