import {
    ON_GET_PROVIDERS_SUCCESS,
    ON_FETCH_PROVIDERS,
    ON_SET_FILTERS,
    PROVIDERS_SYNC_SUCCESS
} from './types';
import Service from 'helpers/service';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import { sendNotification } from 'actions';

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_PROVIDERS });

        Service(query, 'api/providers', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_PROVIDERS_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });
    };
}

export function sync() {
    return dispatch => {
        Service({}, 'api/providers/sync', 'put')
            .then(() => {
                dispatch({ type: PROVIDERS_SYNC_SUCCESS });
                dispatch(sendNotification({
                    message: 'proveedores sincronizados',
                    level: 'success'
                }));
            })
            .catch(() => {
                //
            });
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/providers/${id}`));
    };
}
