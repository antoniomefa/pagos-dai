
export const ON_FETCH_PROVIDERS = 'App/Providers/ON_FETCH_PROVIDERS';
export const ON_GET_PROVIDERS_SUCCESS = 'App/Providers/ON_GET_PROVIDERS_SUCCESS';
export const ON_GET_PROVIDERS_FAIL = 'App/Providers/ON_GET_PROVIDERS_FAIL';
export const ON_SET_FILTERS = 'App/Providers/ON_SET_FILTERS';
export const PROVIDERS_SYNC_SUCCESS = 'App/Providers/PROVIDERS_SYNC_SUCCESS';
