import { ON_GET_PROVIDER } from './types';

const initialState = {
    provider: {}
};

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_GET_PROVIDER:
            return {
                ...state,
                provider: action.data
            };
        default:
            return { ...state };
    }
}
