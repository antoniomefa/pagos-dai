import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert } from 'actions';
import { ON_GET_PROVIDER } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const getProfile = id => dispatch => {
    Service({}, `api/providers/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_PROVIDER, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'Usuario no encontrado',
                color: 'danger',
                alias: 'get-user',
                time: 3
            }));
            dispatch(push('/users'));
        });
};

export const saveProvider = data => dispatch => {
    Service(JSON.stringify(data), 'api/providers', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};
