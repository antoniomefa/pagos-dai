import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from 'components/input';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile, saveProvider } from './actions';

import Contacts from 'containers/contacts';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            formValues: {
                contacts: []
            }
        };
    }

    componentDidMount() {

        if (this.props.routeParams.id) {
            this.props.getProfile(this.props.routeParams.id);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        var formValues = this.state.formValues;
        if (nextProps.provider.id) {

            formValues.user_name = nextProps.provider.user ? nextProps.provider.user.name : '';
            formValues.user_email = nextProps.provider.user ? nextProps.provider.user.username : '';
            formValues.user_ext = nextProps.provider.user ? nextProps.provider.user.ext : '';
            formValues.contacts = nextProps.provider.contacts;
            formValues.user_password = '';

            this.setState({
                formValues
            });
        }
    }

    __isEditing = () => !!this.props.routeParams.id && this.props.provider.user;

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            const value = select.value;

            formValues[field] = value;
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    onBlur = (input) => {
        const formValues = this.state.formValues;
        const errorMessages = this.state.errorMessages;

        if (formValues[input] && formValues[input] != '') {
            delete errorMessages[input];
        } else if (!formValues[input] || formValues[input] == '') {
            errorMessages[input] = `El campo ${input} no puede estar vacio`;
        }

        const index = this.inputsRequired().findIndex(i => i == input);
        const NOT_FOUND = -1;

        if (!errorMessages[input]) {
            if (index > NOT_FOUND && !this.inputsRegex()[index].test(formValues[input])) {
                errorMessages[input] = `El campo ${input} no tiene un formato valido`;
            }
        }

        this.setState({ errorMessages });
    }

    onSave = () => {
        this.setState({ formRequested: true }, this.verifyForm);
    }

    inputsRequired = () => {
        if (this.__isEditing()) {
            return ['user_name', 'user_email'];
        }
        return ['user_name', 'user_email', 'user_password'];
    }

    /* eslint-disable */
    inputsRegex = () => {
        if (this.__isEditing()) {
            return [
                /^[a-zA-Z ]+$/,
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            ];
        }

        return [
            /^[a-zA-Z ]+$/,
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            /^.*$/
        ];
    }
    /* eslint-enable */

    inputsRequiredFilled = () => {
        const errorMessages = this.state.errorMessages;
        const formValues = this.state.formValues;
        var invalidForm = false;

        this.inputsRequired().forEach(input => {
            if (!formValues[input] || formValues[input] == '') {
                errorMessages[input] = `El campo ${input} no puede estar vacio`;
                invalidForm = true;
            }
        });

        if (!invalidForm) {
            this.inputsRegex().forEach((regex, index) => {
                const input = this.inputsRequired()[index];

                if (!regex.test(formValues[input])) {
                    errorMessages[input] = `El campo ${input} no tiene un formato valido`;
                    invalidForm = true;
                }
            });
        }

        this.setState({ errorMessages, invalidForm });

        return !invalidForm;
    }

    verifyForm = () => {
        if (!this.inputsRequiredFilled()) {
            return this.setState({ invalidForm: true });
        }

        const { formValues } = this.state;
        const { provider } = this.props;
        const NOT_FOUND = -1;

        const form = {
            id: provider.id,
            user_id: this.__isEditing() ? provider.user.id : NOT_FOUND,
            user_email: formValues.user_email,
            user_password: btoa(formValues.user_password),
            user_ext: formValues.user_ext,
            user_name: formValues.user_name,
            contacts: formValues.contacts
        };

        this.props.saveProvider(form);
    }

    onChangeContacts = contacts => {
        const formValues = this.state.formValues;

        formValues.contacts = contacts;

        this.setState({ formValues });
    }

    getOptionsRol = () => {
        return [
            {value: 'Admin', label: 'admin'}
        ];
    }

    onAddNewContact = contact => {
        const ZERO = 0;
        const formValues = this.state.formValues;
        const newContact = contact.id > ZERO ? contact : { ...contact, id: -1 };

        formValues.contacts = [
            ...formValues.contacts,
            ...[{ ...newContact }]
        ];

        this.setState({ formValues });
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <form className="container">
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Nombre de usuario"
                        htmlFor="user_name"
                        icon="user"
                        id="user_name"
                        hasError={!!errorMessages['user_name']}
                        invalidText={errorMessages['user_name']}
                        requested={formRequested}
                        placeholder="nombre de usuario..."
                        aria="user_name"
                        value={formValues['user_name']}
                        onChange={this.onChange.bind(this, 'user_name')}
                        onBlur={this.onBlur.bind(this, 'user_name')}
                        required
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        type="email"
                        label="Correo electronico"
                        htmlFor="user_email"
                        icon="envelope-o"
                        requested={formRequested}
                        onChange={this.onChange.bind(this, 'user_email')}
                        onBlur={this.onBlur.bind(this, 'user_email')}
                        id="user_email"
                        value={formValues['user_email']}
                        placeholder="correo electoronico..."
                        aria="user_email"
                        hasError={!!errorMessages['user_email']}
                        invalidText={errorMessages['user_email']}
                        required
                        isGroup
                        hasIcon
                    />
                </div>
                <div className="form-row">
                    <Input
                        hasLabel
                        label="Contraseña"
                        htmlFor="user_password"
                        icon="key"
                        id="user_password"
                        hasError={!!errorMessages['user_password']}
                        invalidText={errorMessages['user_password']}
                        requested={formRequested}
                        placeholder="contraseña..."
                        aria="user_password"
                        type="password"
                        value={formValues['user_password']}
                        onChange={this.onChange.bind(this, 'user_password')}
                        onBlur={this.onBlur.bind(this, 'user_password')}
                        required
                        isGroup
                        hasIcon
                    />
                    <Input
                        hasLabel
                        label="Extensión telefónica"
                        htmlFor="user_ext"
                        icon="phone"
                        type="number"
                        requested={formRequested}
                        id="user_ext"
                        onChange={this.onChange.bind(this, 'user_ext')}
                        onBlur={this.onBlur.bind(this, 'user_ext')}
                        value={formValues['user_ext']}
                        placeholder="extensión..."
                        aria="user_ext"
                        isGroup
                        hasIcon
                    />
                </div>
                <Contacts
                    isEditable
                    {...this.props}
                    onAddNewContact={this.onAddNewContact}
                    onChange={this.onChangeContacts}
                    contacts={this.state.formValues.contacts} />
                <div className="pull-right">
                    <Button
                        text="Guardar"
                        type="primary"
                        outline
                        onButtonPress={this.onSave}
                        permission={'update'}
                        permissions={this.props.permisos}
                    />
                </div>
            </form>
        );
    }
}

ProfileUser.propTypes = {
    permisos: PropTypes.array,
    provider: PropTypes.object,
    getProfile: PropTypes.func,
    saveProvider: PropTypes.func,
    routeParams: PropTypes.object
};

const mapStateToProps = ({ providerForm }) => ({
    ...providerForm
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    saveProvider
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
