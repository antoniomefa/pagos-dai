import {
    ON_GET_REFERRAL_NOTES_TO_PAY_SUCCESS,
    ON_FETCH_REFERRAL_NOTES_TO_PAY,
    ON_SET_FILTERS
} from './types';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import { sendNotification } from 'actions';
import Service from 'helpers/service';
import { downloadB64 } from 'helpers';

export const notification = message => dispatch => {
    dispatch(sendNotification({ message, level: 'warning' }));
};

export function downloadFile(url, type) {
    return dispatch => {
        const ONE = 1;
        const name = url.split('/')[ONE];
        const mime = name.split('.')[ONE];
        const query = { url };

        Service(query, 'api/files', 'get')
            .then(({ data }) => {
                downloadB64(mime, name, data);
            })
            .catch(() => {
                dispatch(notification(`Error al descargar el ${type}`));
            });
    };
}

export function getData(query) {
    return dispatch => {
        dispatch({ type: ON_FETCH_REFERRAL_NOTES_TO_PAY });

        Service(query, 'api/referralnotes/treasury', 'get')
            .then(({ data }) => {
                dispatch({
                    type: ON_GET_REFERRAL_NOTES_TO_PAY_SUCCESS,
                    payload: {
                        data: data.data,
                        page: data.current_page,
                        total: data.total
                    }
                });
            })
            .catch(() => {
                //
            });
    };
}

export const addQuery = (query) => {
    const location = Object.assign({}, browserHistory.getCurrentLocation());
    Object.assign(location.query, query);
    browserHistory.push(location);
};

export function setFilters(filters) {
    return {
        type: ON_SET_FILTERS,
        filters
    };
}

export function goProfile(id) {
    return dispatch => {
        dispatch(push(`/referral-notes-to-pay/${id}`));
    };
}
