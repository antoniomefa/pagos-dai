import { browserHistory } from 'react-router';
import { goBack, push } from 'react-router-redux';
import Service from 'helpers/service';
import { showAlert, sendNotification } from 'actions';
import { ON_GET_REFERRAL_NOTE } from './types';

export function goEdit() {
    return dispatch => {
        dispatch(push(`${browserHistory.getCurrentLocation().pathname}/edit`));
    };
}

export const createBill = data => dispatch => {
    Service(JSON.stringify(data), 'api/referralnotes', 'POST')
        .then(() => {
            dispatch(showAlert({
                text: 'Nota de remisión creada correctamente',
                color: 'primary',
                alias: 'created-referralnotes',
                time: 5
            }));
            dispatch(goBack());
        })
        .catch(() => {
            dispatch(sendNotification({
                message: 'No fue posible crear la nota',
                level: 'warning'
            }));
        });
};

export const getProfile = id => dispatch => {
    Service({}, `api/referralnotes/${id}`, 'get')
        .then(({ data }) => {
            dispatch({ type: ON_GET_REFERRAL_NOTE, data });
        })
        .catch(() => {
            // error
            dispatch(showAlert({
                text: 'NOta de remision no encontrada',
                color: 'danger',
                alias: 'get-referralnotes',
                time: 3
            }));
            dispatch(push('/referral-notes'));
        });
};

export const updateBill = data => dispatch => {
    Service(JSON.stringify(data), 'api/referralnotes', 'put')
        .then(() => {
            dispatch(goBack());
        })
        .catch(() => {
            // error
        });
};

export const cancelForm = () => dispatch => dispatch(goBack());
