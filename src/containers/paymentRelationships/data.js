
export const data = [
    {
        id: 1, customer: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 1,
        folio: 'F-S00212', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 2, customer: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 12,
        folio: 'F-S002567', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Luis Angel Sa de CV', status: 'iPendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 3, customer: '209176456', emisor: 'Barragan', receptor: 'Luis', oc: 14,
        folio: 'F-S00234', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Marco SA de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 4, customer: '209897111', emisor: 'Barragan', receptor: 'Luis', oc: 15,
        folio: 'F-S00243', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 5, customer: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 21,
        folio: 'F-S002424', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 6, customer: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 19,
        folio: 'F-S0022345', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 7, customer: '209176456', emisor: 'Barragan', receptor: 'Luis', oc: 11,
        folio: 'F-S002', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Marco SA de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 8, customer: '209897111', emisor: 'Barragan', receptor: 'Luis', oc: 122,
        folio: 'F-S002', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 9, customer: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 144,
        folio: 'F-S002', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 10, customer: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 122,
        folio: 'F-S002', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Luis Angel Sa de CV'
    }
];

export const data2 = [
    {
        id: 11, customer: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 212,
        folio: 'F-S002343', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 12, customer: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 221,
        folio: 'F-S0025456', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 13, customer: 'Bosques acueducto', emisor: 'Barragan', receptor: 'Luis', oc: 115,
        folio: 'F-S002234', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Marco SA de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 14, customer: 'Colinas del bosque', emisor: 'Barragan', receptor: 'Luis', oc: 191,
        folio: 'F-S00247', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 15, customer: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 241,
        folio: 'F-S002678', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 16, customer: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 341,
        folio: 'F-S002768', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 17, customer: 'Bosques acueducto', emisor: 'Barragan', receptor: 'Luis', oc: 391,
        folio: 'F-S00223', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Marco SA de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 18, customer: 'Colinas del bosque', emisor: 'Barragan', receptor: 'Luis', oc: 971,
        folio: 'F-S00298765', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Edmundo SA DE CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 19, customer: 'San isidro', emisor: 'Barragan', receptor: 'Luis', oc: 541,
        folio: 'F-S00224', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Jonathan Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    },
    {
        id: 20, customer: 'San rafael', emisor: 'Barragan', receptor: 'Luis', oc: 111,
        folio: 'F-S002', serie: '1', dateSend: '10/10/2018', statusDownload: 'descargado',
        statusSend: 'enviado',
        provider: 'Luis Angel Sa de CV', status: 'Pendiente', comprador: '5M Construcciones',
        customerType: 'interno'
    }
];
