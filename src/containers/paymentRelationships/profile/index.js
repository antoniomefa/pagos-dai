import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/button';
import PropTypes from 'prop-types';
import { goEdit, getProfile } from './actions';
import { notification, downloadFile, notify } from '../actions';
import ModalMessage from 'components/modalMessage';

class ProfileWarehouse extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            id: null
        };
    }

    componentDidMount() {
        const id = this.props.routeParams.id;

        this.props.getProfile(id);
        this.setState({ id });
    }

    onUpdate = () => {
        this.props.goEdit();
    }

    onDownload = () => {
        const { payment, downloadFile, notification } = this.props;
        if (payment.pdf_url !== '') downloadFile(payment.pdf_url);
        else notification('No existe archivo para descargar.');
    }

    onResend = () => {
        this.setState({
            modalIsOpen: true
        });
    }

    toggle = () => {
        this.setState({
            modalIsOpen: !this.state.modalIsOpen
        });
    }

    renderModal = () => {
        if (!this.props.routeParams.id) return null;

        return (
            <ModalMessage
                isOpen={this.state.modalIsOpen}
                toggle={this.toggle}
                notification={this.props.notification}
                title="Reenviar orden de compra"
                onBtnAction={this.send}
                purchaseOrderId={parseInt(this.props.routeParams.id)}
            />
        );
    }

    send = (ids, description) => {
        this.props.notify(this.props.routeParams.id, ids.map(c => c.value), description);

        this.setState({
            modalIsOpen: false
        });
    }

    render() {
        const { payment } = this.props;

        if (!payment) return null;

        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="__card">
                    <div className="__card__header">
                        <h5>ID de la relación de pago: {payment.id}</h5>
                    </div>
                    <div className="__card__body">
                        <p>
                            <strong>Cliente:</strong>
                            <small> {payment.client.bussiness_name}</small>
                        </p>
                        <p>
                            <strong>Proveedor:</strong>
                            <small> {payment.provider.bussiness_name}</small>
                        </p>
                    </div>
                    <div className="__card__footer">
                        <Button
                            text="Editar"
                            type="primary"
                            icon="edit"
                            isIcon
                            outline
                            onButtonPress={this.onUpdate}
                            permission={'update'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Descargar"
                            type="primary"
                            icon="download"
                            isIcon
                            onButtonPress={this.onDownload}
                            permission={'download'}
                            permissions={this.props.permisos}
                        />
                        <Button
                            text="Reenviar"
                            type="dark"
                            icon="envelope-o"
                            isIcon
                            outline
                            onButtonPress={this.onResend}
                            permission={'emailing'}
                            permissions={this.props.permisos}
                        />
                    </div>
                </div>
                {this.renderModal()}
            </div>
        );
    }
}

ProfileWarehouse.propTypes = {
    permisos: PropTypes.array,
    routeParams: PropTypes.object,
    auth: PropTypes.object,
    getProfile: PropTypes.func,
    downloadFile: PropTypes.func,
    notify: PropTypes.func,
    payment: PropTypes.object,
    notification: PropTypes.func,
    deleteWarehouses: PropTypes.func,
    goEdit: PropTypes.func
};

const mapStateToProps = ({ paymentRelationshipProfile }) => ({
    ...paymentRelationshipProfile
});

const mapDispatchToProps = {
    goEdit,
    getProfile,
    downloadFile,
    notify,
    notification
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileWarehouse);
