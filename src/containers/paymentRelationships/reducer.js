import {
    ON_GET_PAYMENT_RELATIONSHIPS_SUCCESS,
    ON_GET_PAYMENT_RELATIONSHIPS_FAIL,
    ON_FETCH_PAYMENT_RELATIONSHIPS,
    ON_SET_FILTERS,
    ON_REFRESH,
    ON_DELETE_PAYMENT
} from './types';

const initialState = {
    total: 0,
    query: {
        page: 1,
        q: '',
        filters: '{}'
    },
    message: null,
    rows: [],
    refresh: false,
    loading: false
};

const ZERO = 0;
const ONE = 1;

export default function providersReducer(state = initialState, action) {
    var index = state.rows.findIndex(r => r.id === action.id);
    switch (action.type) {
        case ON_FETCH_PAYMENT_RELATIONSHIPS:
            return {
                ...state,
                refresh: false,
                loading: true
            };
        case ON_REFRESH:
            return {
                ...state,
                refresh: true
            };
        case ON_DELETE_PAYMENT:
            return {
                ...state,
                rows: [
                    ...state.rows.slice(ZERO, index),
                    ...state.rows.slice(index + ONE)
                ]
            };
        case ON_SET_FILTERS:
            return {
                ...state,
                query: action.filters
            };
        case ON_GET_PAYMENT_RELATIONSHIPS_SUCCESS:
            return {
                ...state,
                loading: false,
                message: null,
                rows: action.payload.data,
                total: action.payload.total
            };
        case ON_GET_PAYMENT_RELATIONSHIPS_FAIL:
            return {
                ...state,
                loading: false,
                message: action.payload
            };
        default:
            return {
                ...state
            };
    }
}
