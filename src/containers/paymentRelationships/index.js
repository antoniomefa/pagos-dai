import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

/* COMPONENTS */
import Table from 'components/table';

/* ACTOONS */
import {
    getData,
    addQuery,
    setFilters,
    goProfile,
    downloadFile,
    notification,
    send,
    deletePayment,
    goCreate
} from './actions';

/* CATALOGS */
import {
    getProviders,
    getCustomers,
    getPaymentRelationshipStatus
} from 'utils/catalogs';

class PaymentRelationships extends Component {
    constructor(props) {
        super(props);

        this.state = {
            providersCatalog: [],
            customersCatalog: [],
            statusCatalog: []
        };
    }

    componentDidMount() {
        const ZERO = 0;

        if (location.search != '') {
            const parsed = queryString.parse(location.search);

            // parsed.filters = JSON.parse(parsed.filters);
            parsed.page = parseInt(parsed.page) || ZERO;

            this.props.setFilters(parsed);
            this.props.getData(parsed);

            addQuery(parsed);
        } else {
            this.props.getData(this.props.query);
            addQuery({ ...this.props.query, filters: this.props.query.filters });
        }

        this.getStatusCatalog();
        this.getProvidersCatalog();
        this.getCustomersCatalog();
    }

    getStatusCatalog = () => {
        getPaymentRelationshipStatus().then(statusCatalog => this.setState({ statusCatalog }));
    }

    getProvidersCatalog = () => {
        getProviders().then(providersCatalog => this.setState({ providersCatalog }));
    }

    getCustomersCatalog = () => {
        getCustomers().then(customersCatalog => this.setState({ customersCatalog }));
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.refresh) {
            this.props.getData(this.props.query);
        }
    }

    __getDataColumns() {
        return [
            {name: '#', field: 'id'},
            {name: 'Proveedor', field: 'provider.bussiness_name'},
            {name: 'Cliente', field: 'client.bussiness_name'},
            {name: 'Fecha de envio', field: 'sent_at', isDate: true},
            {name: 'Estatus', field: 'status'}
        ];
    }

    __getDataRows = () => this.props.rows;

    __getFilters() {
        const { statusCatalog } = this.state;
        const { providersCatalog } = this.state;
        const { customersCatalog } = this.state;

        var statusOptions = statusCatalog.map(s => (
            { label: s, value: s}
        ));
        var providersOptions = providersCatalog.map(p => (
            { label: p.bussiness_name, value: p.id }
        ));
        var customersOptions = customersCatalog.map(c => (
            { label: c.bussiness_name, value: c.id }
        ));
        return [
            { name: 'proveedor', field: 'provider_id', options: providersOptions },
            { name: 'cliente', field: 'client_id', options: customersOptions },
            { name: 'estatus', field: 'payment_relationship_status', options: statusOptions }
        ];
    }

    __filtersHandler = filters => {
        const newOffset = 1;
        const newFilters = {
            ...filters,
            page: newOffset,
            filters: JSON.stringify(filters.filters)
        };

        addQuery({
            ...this.props.query,
            filters: JSON.stringify(filters.filters),
            q: filters.q,
            page: newOffset
        });

        this.props.getData(newFilters);
        this.props.setFilters(newFilters);
    }

    download = id => {
        const found = this.__getDataRows().find(d => d.id == id);

        if (found && found.pdf_url !== '') this.props.downloadFile(found.pdf_url);
        else this.props.notification('No existe archivo para descargar.');
    }

    __paginatedChange = page => {
        const { query } = this.props;

        this.props.setFilters({ ...query, page });
        addQuery({ ...query, page, filters: query.filters});
        this.props.getData({ ...query, page, filters: query.filters});
    }

    __addRelationship = () => {
        this.props.goCreate();
    }

    __getHeadActions = () => {
        return [
            {
                icon: 'plus', label: 'agregar', style: 'outline-secondary',
                permission: 'create',
                action: this.__addRelationship, tooltip: 'agregar nuevo usuario'
            }
        ];
    }

    onDelete = ids => {
        this.props.deletePayment(ids);
    }

    __getMasiveActions = () => {
        return [
            {
                icon: 'trash-o', label: 'eliminar', style: 'outline-secondary',
                permission: 'delete',
                action: this.onDelete, tooltip: 'eliminar'
            }
        ];
    }

    __getSingleActions = () => {
        return [
            {
                icon: 'trash-o',
                style: 'btn btn-danger',
                tooltip: 'Eliminar',
                permission: 'delete',
                action: id => this.props.deletePayment([id])
            },
            {
                icon: 'eye',
                style: 'btn btn-info',
                tooltip: 'Ver',
                permission: 'review',
                action: id => {
                    this.props.goProfile(id);
                }
            },
            {
                icon: 'download',
                style: 'btn btn-primary',
                tooltip: 'Descargar',
                permission: 'download',
                action: id => this.download(id)
            },
            {
                icon: 'envelope-o',
                style: 'btn btn-secondary',
                tooltip: 'Enviar',
                permission: 'emailing',
                action: id => this.props.send(id)
            },
            {
                icon: 'edit',
                style: 'btn btn-success',
                tooltip: 'Cambiar estatus',
                permission: 'change-status',
                action: () => {}
             }
        ];
    }

    render() {
        const { query, total } = this.props;
        return (
            <div>
                <Table
                    columns={this.__getDataColumns()}
                    data={this.__getDataRows()}
                    filters={this.__getFilters()}
                    query={query}
                    filtersHandler={this.__filtersHandler}
                    paginatedChange={this.__paginatedChange}
                    buttons={this.__getHeadActions()}
                    massiveActions={this.__getMasiveActions()}
                    singleActions={this.__getSingleActions()}
                    total={total}
                    isMultiselect
                    offset={query.page}
                />
             </div>
        );
    }
}

PaymentRelationships.propTypes = {
    query: PropTypes.object,
    rows: PropTypes.array,
    total: PropTypes.number,
    getData: PropTypes.func,
    addQuery: PropTypes.func,
    goCreate: PropTypes.func,
    downloadFile: PropTypes.func,
    notification: PropTypes.func,
    send: PropTypes.func,
    deletePayment: PropTypes.func,
    setFilters: PropTypes.func,
    goProfile: PropTypes.func
};

PaymentRelationships.defaultProps = {
    getData: () => {},
    addQuery: () => {},
    setFilters: () => {},
    goProfile: () => {}
};

const mapStateToProps = ({ paymentRelationships }) => ({
    ...paymentRelationships
});

const mapDispatchToProps = {
    getData,
    addQuery,
    goCreate,
    downloadFile,
    send,
    deletePayment,
    notification,
    setFilters,
    goProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentRelationships);
