import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { showAlert, sendNotification } from 'actions';

class Login extends Component {
    constructor(props) {
        super(props);
    }

    showAlert = () => {
        this.props.showAlert({
            color: 'success',
            text: 'Hola mundo',
            time: 3,
            alias: 'hola-mundo'
        });
    }

    showNotification = () => {
        this.props.sendNotification({
            hola: 1
        });
    }

    render() {
        return (
            <div>
                Register

                <button onClick={this.showAlert}>show alert</button>
                <button onClick={this.showNotification}>show notification</button>
            </div>
        );
    }
}

Login.propTypes = {
    showAlert: PropTypes.func,
    sendNotification: PropTypes.func
};

const mapStateToProps = () => ({

});

const mapDispatchToProps = {
    showAlert,
    sendNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
