import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Sidebar from 'containers/sidebar';
import Header from 'containers/header';
import NotificationSystem from 'react-notification-system';
import {
    clearNotification,
    clearAlert,
    redirectTo,
    setModule,
    setPermissions,
    renewSession
} from './actions';
import { showNotice } from 'actions';
import { Alert } from 'reactstrap';
import { MIL, ONE } from 'utils/constants';
import sidebarItems from 'utils/sidebar_items';
import { havePermission } from 'utils';
import Notifications from 'react-notification-system-redux';
import Breadcrumb from 'components/breadcrumb';

class Wrapper extends Component {
    constructor(props) {
        super(props);

        this.__notificationSystem = React.createRef();
        this.__addNotification = this.__addNotification.bind(this);

        this.state = {
            currentMenu: props.currentMenu,
            side: props.side,
            module: props.route.module
        };

        const data = this.__getRootPath(window.location.pathname);

        this.props.setModule(data.module, data.submodule, 'path');

        this.props.notifications.map(notif => {
            this.props.showNotice(Notifications.show(notif, notif.level));
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const { redirectTo, module, submodule } = nextProps;
        const permissions = havePermission({ alias: module });

        if (module !== this.props.module) {
            // revisar session
            this.props.renewSession(this.props.auth.user);
        }

        if (module && !this.props.permissions[module]) {
            this.props.setPermissions(module, permissions);
        }


        if (nextProps.notification) {
            this.__addNotification(nextProps.notification);
        }

        if (module && !permissions) {
            redirectTo('/');
        } else if (module && submodule && !permissions.permisos.some(p => p.key == submodule)) {
            redirectTo(`/${module}`);
        }
    }

    getItems() {
        return sidebarItems.map((item) => {
            let permissions = havePermission(item);
            if (!permissions) return false;
            if (!permissions.children && permissions.permisos.length) return { ...item };

            const filtered = item.children.filter((side) => {
                return permissions.children.some(c => c.module == side.alias && c.permisos.length);
            });

            return { ...item, children: filtered};
        });
    }

    __getRootPath(__path) {
        const paths = __path.split('/');
        const MODULE_PROFILE = 3;
        const MODULE_EDIT = 4;
        const ONLY_MODULE = 2;

        if (paths.length == MODULE_PROFILE) {
            const sm = paths[paths.length - ONE] == 'edit' ? 'update' : 'review';
            const module = `/${paths[ONE]}`;
            const submodule = paths[MODULE_PROFILE - ONE] == 'create' ? 'create' : sm;

            return { module, submodule };
        } else if (paths.length == MODULE_EDIT) {
            const module = `/${paths[ONE]}`;
            const submodule = paths[MODULE_EDIT - ONE] == 'edit' ? 'update' : 'review';

            return { module, submodule };
        } else if (paths.length == ONLY_MODULE) {
            const module = `/${paths[ONE]}`;

            return { module };
        }

        return {};
    }

    __sidebar = () => {
        //debugger
        const { auth, submodule, module } = this.props;

        if (!auth.user) return null;

        return <Sidebar items={this.getItems()} module={module} submodule={submodule} />;
    }

    __addNotification(notification) {
        this.__notificationSystem.current.addNotification(notification);
        this.props.clearNotification();
    }

    render() {
        const { alerts, route, children, permissions, module, auth, routeParams } = this.props;

        const childrenWithPermissions = React.Children.map(children, child => {
            return React.cloneElement(child, { ...permissions[module], routeParams });
        });

        const extraClass = auth.user ? 'withPadding' : '';
        return (
            <div
                id="outer-container"
                className={`page-wrapper toggled chiller-theme sidebar-bg bg1 ${extraClass}`}
                style={{height: '100%'}}
            >
                <a id="show-sidebar" className="btn btn-sm btn-dark" href="#">
                    <i className="fas fa-bars"></i>
                </a>
                <div className={this.state.side}>
                    {this.__sidebar()}
                </div>
                <main id="page-wrap">
                    {/* <Header /> */}
                    <Breadcrumb route={route}/>
                    {
                        alerts.map((alert, index) => {

                            if (alert.time !== 'never') {
                                setTimeout(() => {
                                    this.props.clearAlert(alert.alias);
                                }, (alert.time * MIL));
                            }

                            return (
                                <Alert color={alert.color} key={index}>
                                    <span
                                        className={'fa fa-bullhorn'}
                                        style={{ marginRight: 10 }}
                                    />
                                    {alert.text}
                                </Alert>
                            );
                        })
                    }

                    <NotificationSystem  ref={this.__notificationSystem} />
                    <Notifications
                        notifications={this.props.notifications}
                    />
                    <div className="__content_padding">
                        {childrenWithPermissions}
                    </div>
                </main>
            </div>
        );
    }
}

Wrapper.propTypes = {
    children: PropTypes.element,
    currentMenu: PropTypes.string,
    side: PropTypes.string,
    auth: PropTypes.object,
    alerts: PropTypes.array,
    clearNotification: PropTypes.func,
    clearAlert: PropTypes.func,
    redirectTo: PropTypes.func,
    module: PropTypes.string,
    submodule: PropTypes.string,
    initialModule: PropTypes.string,
    setModule: PropTypes.func,
    showNotice: PropTypes.func,
    notifications: PropTypes.array,
    permissions: PropTypes.object,
    routeParams: PropTypes.object,
    setPermissions: PropTypes.func,
    renewSession: PropTypes.func,
    route: PropTypes.object
};

Wrapper.defaultOptions = {
    currentMenu: 'slide',
    side: 'left'
};

const mapStateToProps = ({ wrapper, notifications, auth }) => ({
    ...wrapper,
    auth,
    notifications
});

const mapDispatchToProps = {
    clearNotification,
    clearAlert,
    redirectTo,
    setModule,
    showNotice,
    setPermissions,
    renewSession
};

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper);
