import { routerActions } from 'react-router-redux';
import routes from 'utils/routes';
import {
    ON_ERASE_NOTIFICATION,
    ON_ERASE_ALERT
} from 'action-types';
import { ON_SET_MODULE, ON_SET_PERMISSIONS } from './types';
import { showAlert } from 'actions';
import { ON_RENEW_SESSION } from 'action-types/auth';
import { getSession, getAccessToken, getPhorter, logout } from 'helpers';
import { sendNotification } from 'actions';
import Service from 'helpers/service';
import { createSession } from 'helpers';

export function me() {
    return dispatch => {
        Service({}, 'auth/me', 'get')
            .then((data) => {
                createSession(data);
                dispatch(sendNotification({
                    message: 'Tu cuenta se ha actualizada correctamente',
                    level: 'success'
                }));
            })
            .catch(() => {
                dispatch(sendNotification({
                    message: 'No fe posible descargar el archivo',
                    level: 'warning'
                }));
            });


    };
}

export const renewSession = (auth) => dispatch => {
    var message = 'Tu sesión ha expirado.';
    const session = getSession();
    const accessToken = getAccessToken();
    const phorther = getPhorter();

    if (auth && (!session || !accessToken || !phorther)) {
        if (!accessToken || !phorther) {
            message = 'Hubo un error en tu sesión, vuelve a iniciar.';
        }

        logout();
        dispatch(sendNotification({ message, level: 'warning' }));
        dispatch({ type: ON_RENEW_SESSION, payload: null });
        return;
    }

    dispatch({ type: ON_RENEW_SESSION, payload: session });
};

export function clearNotification() {
    return {
        type: ON_ERASE_NOTIFICATION
    };
}

export function clearAlert(alias) {
    return {
        type: ON_ERASE_ALERT,
        alias
    };
}

export function redirectTo(path) {
    return dispatch => {
        dispatch(setModule(path, 'path'));
        dispatch(routerActions.replace(path));
        dispatch(showAlert({
            text: 'No tienes permisos para acceder',
            color: 'danger',
            alias: 'error-auth',
            time: 3
        }));
    };
}

export function goBack() {
    return dispatch => {
        dispatch(routerActions.goBack());
    };
}

export function setModule(path, submodule, by) {
    const route = routes.find(m => m[by] == path);
    var moduleSelected;

    if (!path || !by) return {
        type: ON_SET_MODULE
    };

    if (!route) return {
        type: ON_SET_MODULE,
        module: moduleSelected
    };

    moduleSelected = route.module;

    return {
        type: ON_SET_MODULE,
        module: moduleSelected,
        submodule
    };
}

export function setPermissions(module, permissions) {
    const data = {};

    data[module] = permissions;

    return {
        type: ON_SET_PERMISSIONS,
        module,
        permissions: data
    };
}
