import {
    ON_ERASE_NOTIFICATION,
    ON_SEND_NOTIFICATION,
    ON_SHOW_ALERT,
    ON_ERASE_ALERT
} from 'action-types';
import { NOT_FOUND, ZERO, ONE } from 'utils/constants';
import { ON_SET_MODULE, ON_SET_PERMISSIONS } from './types';

const initialState = {
    notification: null,
    module: null,
    submodule: null,
    alerts: [],
    permissions: {}
};

const wrapperReducer = (state = initialState, action) => {
    var index = NOT_FOUND;

    if (action.alias || action.alert) {
        index = state.alerts.findIndex(a => a.alias === action.alias || action.alert.alias);
    }

    switch (action.type) {
        case ON_ERASE_NOTIFICATION:
            return {
                ...state,
                notification: null
            };
        case ON_SEND_NOTIFICATION:
            return {
                ...state,
                notification: action.notification
            };
        case ON_SET_MODULE:
            return {
                ...state,
                module: action.module,
                submodule: action.submodule
            };
        case ON_SHOW_ALERT:
            if (index >= ZERO) {
                return {
                    ...state
                };
            }

            return {
                ...state,
                alerts: [
                    ...state.alerts,
                    action.alert
                ]
            };
        case ON_SET_PERMISSIONS:
            return {
                ...state,
                permissions: {
                    ...state.permissions,
                    ...action.permissions
                }
            };
        case ON_ERASE_ALERT:
            if (index == NOT_FOUND) return { ...state };
            return {
                ...state,
                alerts: [
                    ...state.alerts.slice(ZERO, index),
                    ...state.alerts.slice(index + ONE)
                ]
            };
        default:
            return {
                ...state
            };
    }
};

export default wrapperReducer;
