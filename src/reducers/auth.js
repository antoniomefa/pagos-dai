import { ACTION_DEFAULT, ON_LOGIN_SUCCESS, ON_RENEW_SESSION } from 'action-types/auth';
import { getSession } from 'helpers';

const initialState = {
    user: typeof getSession == 'function' ? getSession() : null
};

export default function authReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_DEFAULT:
            return { ...initialState };
        case ON_LOGIN_SUCCESS:
            return {
                ...state,
                user: action.payload
            };
        case ON_RENEW_SESSION:
            return {
                ...state,
                user: action.payload
            };
        default:
            return state;
    }
}
