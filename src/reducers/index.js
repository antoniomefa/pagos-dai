import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as notifications } from 'react-notification-system-redux';

import accountStatus from 'containers/accountStatus/reducer'
import authReducer from './auth';
import wrapperReducer from 'containers/wrapper/reducer';
import users from 'containers/users/reducer';
import providers from 'containers/providers/reducer';
import projects from 'containers/projects/reducer';
import customers from 'containers/customers/reducer';
import memories from 'containers/memories/reducer';
import purchaseOrders from 'containers/purchaseOrders/reducer';
import bills from 'containers/bills/reducer';
import referralNotes from 'containers/referralNotes/reducer';
import referralNotesToPay from 'containers/referralNotesToPay/reducer';
import billsToPay from 'containers/billsToPay/reducer';
import warehouses from 'containers/warehouses/reducer';
import paymentVouchers from 'containers/paymentVouchers/reducer';
import paymentRelationships from 'containers/paymentRelationships/reducer';
import againstReceipts from 'containers/againstReceipts/reducer';
import advisories from 'containers/advisories/reducer';
import configuration from 'containers/configuration/reducer';
import promotions from 'containers/promotions/reducer';
import aboutUs from 'containers/aboutUs/reducer';
import AboutFormReducer from 'containers/aboutUs/form/reducer';
import LoginReducer from 'containers/login/reducer';
import UserProfileReducer from 'containers/users/profile/reducer';
import UserFormReducer from 'containers/users/form/reducer';
import ProviderProfileReducer from 'containers/providers/profile/reducer';
import ProviderFormReducer from 'containers/providers/form/reducer';
import payForm from 'containers/accountStatus/reducer';
import registeredCards from 'containers/registeredCards/reducer';
// customer
import CustomerProfileReducer from 'containers/customers/profile/reducer';
import CustomerFormReducer from 'containers/customers/form/reducer';
import ProjectProfileReducer from 'containers/projects/profile/reducer';
import ProjectFormReducer from 'containers/projects/form/reducer';
import WarehouseProfileReducer from 'containers/warehouses/profile/reducer';
import WarehouseFormReducer from 'containers/warehouses/form/reducer';
import AdvisoryProfileReducer from 'containers/advisories/profile/reducer';
import AdvisoryFormReducer from 'containers/advisories/form/reducer';
import PromotionProfileReducer from 'containers/promotions/profile/reducer';
import PromotionFormReducer from 'containers/promotions/form/reducer';
import PurchaseOrderProfileReducer from 'containers/purchaseOrders/profile/reducer';
import PurchaseOrderFormReducer from 'containers/purchaseOrders/form/reducer';
import BillFormReducer from 'containers/bills/form/reducer';
import BillProfileReducer from 'containers/bills/profile/reducer';
import ReferralNoteFormReducer from 'containers/referralNotes/form/reducer';
import ReferralNoteProfileReducer from 'containers/referralNotes/profile/reducer';
import ReferralNoteToPayFormReducer from 'containers/referralNotesToPay/form/reducer';
import ReferralNoteToPayProfileReducer from 'containers/referralNotesToPay/profile/reducer';
import MemoryProfileReducer from 'containers/memories/profile/reducer';
import MemoryFormReducer from 'containers/memories/form/reducer';
import PaymentRelationshipProfileReducer from 'containers/paymentRelationships/profile/reducer';
import PaymentRelationshipFormReducer from 'containers/paymentRelationships/form/reducer';
import RegisteredCardsProfileReducer from 'containers/registeredCards/profile/reducer';
import RegisteredCardsFormReducer from 'containers/registeredCards/form/reducer';

import AgainstReceiptProfileReducer from 'containers/againstReceipts/profile/reducer';
import AgainstReceiptFormReducer from 'containers/againstReceipts/form/reducer';

// reports
import reportMessages from 'containers/reports/messages/reducer';
// dai
// events
import EventsReducer from 'containers/events/reducer';

export default combineReducers({
    routing: routerReducer,
    auth: authReducer,
    wrapper: wrapperReducer,
    notifications,

    users,
    providers,
    projects,
    customers,
    memories,
    purchaseOrders,
    bills,
    referralNotes,
    referralNotesToPay,
    billsToPay,
    warehouses,
    paymentVouchers,
    paymentRelationships,
    againstReceipts,
    advisories,
    configuration,
    promotions,
    aboutUs,
    reportMessages,
    accountStatus,
    registeredCards,
    payForm,
    login: LoginReducer,
    userProfile: UserProfileReducer,
    userFormReducer: UserFormReducer,
    providerProfile: ProviderProfileReducer,
    providerForm: ProviderFormReducer,
    customerProfile: CustomerProfileReducer,
    customerForm: CustomerFormReducer,
    providerFormReducer: ProviderFormReducer,
    projectProfile: ProjectProfileReducer,
    projectForm: ProjectFormReducer,
    warehouseProfile: WarehouseProfileReducer,
    warehouseForm: WarehouseFormReducer,
    advisoryProfile: AdvisoryProfileReducer,
    advisoryForm: AdvisoryFormReducer,
    aboutForm: AboutFormReducer,
    promotionProfile: PromotionProfileReducer,
    promotionForm: PromotionFormReducer,
    purchaseOrderProfile: PurchaseOrderProfileReducer,
    purchaseOrderForm: PurchaseOrderFormReducer,
    billForm: BillFormReducer,
    billProfile: BillProfileReducer,
    referralNoteForm: ReferralNoteFormReducer,
    referralNoteProfile: ReferralNoteProfileReducer,
    referralNoteToPayForm: ReferralNoteToPayFormReducer,
    referralNoteToPayProfile: ReferralNoteToPayProfileReducer,
    memoryForm: MemoryFormReducer,
    memoryProfile: MemoryProfileReducer,
    paymentRelationshipProfile: PaymentRelationshipProfileReducer,
    paymentRelationshipForm: PaymentRelationshipFormReducer,
    RegisteredCardsProfile: RegisteredCardsProfileReducer,
    RegisteredCardsForm: RegisteredCardsFormReducer,
    againstReceiptProfile: AgainstReceiptProfileReducer,
    againstReceiptForm: AgainstReceiptFormReducer,

    eventsReducer: EventsReducer
});
