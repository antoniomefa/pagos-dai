import Service from 'helpers/service';

export const uploadFile = (file) => {
    return new Promise((res, rej) => {
        let formData:FormData = new FormData();
        let headers = new Headers();

        formData.append(file.name, file, file.name);
        headers.append('Accept', 'application/json, text/plain, */*');

        Service(formData, 'api/files', 'post', false)
            .then(({ data }) => {
                res(data);
            })
            .catch(err => {
                rej(err);
            });
    });
};
