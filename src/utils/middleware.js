import { connectedRouterRedirect } from 'redux-auth-wrapper/history3/redirect';
import { routerActions } from 'react-router-redux';

export const userIsAuthenticated = connectedRouterRedirect({
    redirectPath: '/sign-in',
    authenticatedSelector: state => state.auth.user !== null, // compare with ===
    redirectAction: routerActions.replace,
    allowRedirectBack: false
});

export const userIsntAuthenticated = connectedRouterRedirect({
    redirectPath: '/',
    authenticatedSelector: state => state.auth.user === null, // compare with !==
    redirectAction: routerActions.replace,
    allowRedirectBack: false

});
