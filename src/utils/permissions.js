import { ACTIONS, BINARY_BASE } from './actions';

const modules = [
    {
        name: 'events', permissions: [
            { key: 'review', value: Math.pow(BINARY_BASE, ACTIONS['review']) }
        ]
    },
    { name: 'users', permissions: [
        {
            key: 'review',
            value: Math.pow(BINARY_BASE, ACTIONS['review'])
        },
        {
            key: 'update',
            value: Math.pow(BINARY_BASE, ACTIONS['update'])
        },
        {
            key: 'create',
            value: Math.pow(BINARY_BASE, ACTIONS['create'])
        },
        {
            key: 'delete',
            value: Math.pow(BINARY_BASE, ACTIONS['delete'])
        }
    ] },
    { name: 'account-status', permissions: [
        {
            key: 'review',
            value: Math.pow(BINARY_BASE, ACTIONS['review'])
        },
        {
            key: 'update',
            value: Math.pow(BINARY_BASE, ACTIONS['update'])
        },
        {
            key: 'create',
            value: Math.pow(BINARY_BASE, ACTIONS['create'])
        },
        {
            key: 'delete',
            value: Math.pow(BINARY_BASE, ACTIONS['delete'])
        }
    ] },
    { name: 'registered-cards', permissions: [
        {
            key: 'review',
            value: Math.pow(BINARY_BASE, ACTIONS['review'])
        },
        {
            key: 'update',
            value: Math.pow(BINARY_BASE, ACTIONS['update'])
        },
        {
            key: 'create',
            value: Math.pow(BINARY_BASE, ACTIONS['create'])
        },
        {
            key: 'delete',
            value: Math.pow(BINARY_BASE, ACTIONS['delete'])
        }
    ] },
    // { name: 'providers', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'sync',
    //         value: Math.pow(BINARY_BASE, ACTIONS['sync'])
    //     }
    // ] },
    // { name: 'projects', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'sync',
    //         value: Math.pow(BINARY_BASE, ACTIONS['sync'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     }
    // ] },
    // { name: 'messages', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     }
    // ] },
    // { name: 'customers', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'sync',
    //         value: Math.pow(BINARY_BASE, ACTIONS['sync'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     }
    // ] },
    // { name: 'bills', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     },
    //     {
    //         key: 'change-status',
    //         value: Math.pow(BINARY_BASE, ACTIONS['change-status'])
    //     },
    //     {
    //         key: 'upload-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-receipt'])
    //     },
    //     {
    //         key: 'download-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download-receipt'])
    //     },
    //     {
    //         key: 'upload-against-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-against-receipt'])
    //     },
    //     {
    //         key: 'upload-proof-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-proof-receipt'])
    //     },
    //     {
    //         key: 'pass-treasurer',
    //         value: Math.pow(BINARY_BASE, ACTIONS['pass-treasurer'])
    //     }
    // ] },
    // { name: 'referral-notes', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     },
    //     {
    //         key: 'change-status',
    //         value: Math.pow(BINARY_BASE, ACTIONS['change-status'])
    //     },
    //     {
    //         key: 'upload-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-receipt'])
    //     },
    //     {
    //         key: 'download-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download-receipt'])
    //     },
    //     {
    //         key: 'upload-against-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-against-receipt'])
    //     },
    //     {
    //         key: 'upload-proof-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-proof-receipt'])
    //     },
    //     {
    //         key: 'pass-treasurer',
    //         value: Math.pow(BINARY_BASE, ACTIONS['pass-treasurer'])
    //     }
    // ] },
    // { name: 'memories', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'change-status',
    //         value: Math.pow(BINARY_BASE, ACTIONS['change-status'])
    //     },
    //     {
    //         key: 'distribute',
    //         value: Math.pow(BINARY_BASE, ACTIONS['distribute'])
    //     },
    //     {
    //         key: 'add-workforce',
    //         value: Math.pow(BINARY_BASE, ACTIONS['add-workforce'])
    //     },
    //     {
    //         key: 'add-fee',
    //         value: Math.pow(BINARY_BASE, ACTIONS['add-fee'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] },
    // { name: 'about-us', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'upload',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] },
    // { name: 'purchase-orders', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'sync',
    //         value: Math.pow(BINARY_BASE, ACTIONS['sync'])
    //     },
    //     {
    //         key: 'upload',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     },
    //     {
    //         key: 'emailing',
    //         value: Math.pow(BINARY_BASE, ACTIONS['emailing'])
    //     },

    //     {
    //         key: 'upload-bill',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-bill'])
    //     },
    //     {
    //         key: 'upload-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-receipt'])
    //     },
    //     {
    //         key: 'delete-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-receipt'])
    //     },

    //     {
    //         key: 'upload-against-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-against-receipt'])
    //     },
    //     {
    //         key: 'delete-against-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-against-receipt'])
    //     },

    //     {
    //         key: 'upload-proof-payment',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-proof-payment'])
    //     },
    //     {
    //         key: 'delete-proof-payment',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-proof-payment'])
    //     }
    // ] },
    // { name: 'warehouses', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'upload',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] },
    // { name: 'payment-vouchers', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'upload',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] },
    // { name: 'payment-relationships', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'upload',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     },
    //     {
    //         key: 'sync',
    //         value: Math.pow(BINARY_BASE, ACTIONS['sync'])
    //     },
    //     {
    //         key: 'emailing',
    //         value: Math.pow(BINARY_BASE, ACTIONS['emailing'])
    //     }
    // ] },
    // { name: 'advisories', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'emailing',
    //         value: Math.pow(BINARY_BASE, ACTIONS['emailing'])
    //     }
    // ] },
    // { name: 'against-receipts', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'upload',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] },
    // { name: 'bills-to-pay', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'upload-recepit',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-recepit'])
    //     },
    //     {
    //         key: 'download-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download-receipt'])
    //     },
    //     {
    //         key: 'delete-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-receipt'])
    //     }
    // ] },
    // { name: 'referral-notes-to-pay', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'upload-recepit',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-recepit'])
    //     },
    //     {
    //         key: 'download-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download-receipt'])
    //     },
    //     {
    //         key: 'delete-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-receipt'])
    //     }
    // ] },
    // { name: 'mission', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'upload-recepit',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-recepit'])
    //     },
    //     {
    //         key: 'download-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download-receipt'])
    //     },
    //     {
    //         key: 'delete-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-receipt'])
    //     }
    // ] },
    // { name: 'promotions', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'create',
    //         value: Math.pow(BINARY_BASE, ACTIONS['create'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'delete',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] },
    // { name: 'configuration', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'update',
    //         value: Math.pow(BINARY_BASE, ACTIONS['update'])
    //     },
    //     {
    //         key: 'upload-recepit',
    //         value: Math.pow(BINARY_BASE, ACTIONS['upload-recepit'])
    //     },
    //     {
    //         key: 'download-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download-receipt'])
    //     },
    //     {
    //         key: 'delete-receipt',
    //         value: Math.pow(BINARY_BASE, ACTIONS['delete-receipt'])
    //     }
    // ] },
    // reportes
    // { name: 'report-messages', permissions: [
    //     {
    //         key: 'review',
    //         value: Math.pow(BINARY_BASE, ACTIONS['review'])
    //     },
    //     {
    //         key: 'download',
    //         value: Math.pow(BINARY_BASE, ACTIONS['download'])
    //     }
    // ] }
];

export default modules;
