
const items = [
    {
        name: 'Estado de Cuenta',
        permisos: [],
        alias: '',
        children: [
            {
                name: 'Pago en línea',
                alias: 'account-status',
                permisos: ['users'],
                path: '/account-status'
            },
            {
                name: 'Historial de facturas',
                alias: 'account-status',
                permisos: ['users'],
                path: '/account-status/invoices-history'
            },
            {
                name: 'Tarjetas registradas',
                alias: 'account-status',
                permisos: ['users'],
                path: '/account-status/registered-cards'
            }
        ]
    },
    // {
    //     name: 'Administración',
    //     permisos: [],
    //     alias: '',
    //     children: [
    //         {
    //             name: 'Usuarios',
    //             alias: 'users',
    //             permisos: ['users'],
    //             path: '/users'
    //         },
    //         {
    //             name: 'Proveedores',
    //             alias: 'providers',
    //             permisos: ['providers'],
    //             path: '/providers'
    //         },
    //         {
    //             name: 'Proyectos',
    //             alias: 'projects',
    //             permisos: ['projects'],
    //             path: '/projects'
    //         },
    //         {
    //             name: 'Clientes',
    //             alias: 'customers',
    //             permisos: ['customers'],
    //             path: '/customers'
    //         },
    //         {
    //             name: 'Almacenes',
    //             alias: 'warehouses',
    //             permisos: ['warehouses'],
    //             path: '/warehouses'
    //         },
    //         {
    //             name: 'Memorias',
    //             alias: 'memories',
    //             permisos: ['memories'],
    //             path: '/memories'
    //         },
    //         {
    //             name: 'Comunicados',
    //             alias: 'advisories',
    //             permisos: ['advisories'],
    //             path: '/advisories'
    //         }
    //     ]
    // },
    // {
    //     name: 'Ordenes de compra',
    //     alias: '',
    //     permisos: [],
    //     children: [
    //         {
    //             name: 'Ordenes de compra',
    //             alias: 'purchase-orders',
    //             permisos: ['purchase-orders'],
    //             path: '/purchase-orders'
    //         },
    //         {
    //             name: 'Contrarecibos',
    //             alias: 'against-receipts',
    //             permisos: ['against-receipts'],
    //             path: '/against-receipts'
    //         }
    //     ]
    // },
    // {
    //     name: 'Reportes',
    //     permisos: [],
    //     alias: '',
    //     children: [
    //         {
    //             name: 'Mensajes',
    //             alias: 'report-messages',
    //             permisos: ['report-messages'],
    //             path: '/report-messages'
    //         }
    //     ]
    // },
    // {
    //     name: 'Acerca de',
    //     permisos: [],
    //     alias: '',
    //     children: [
    //         {
    //             name: 'Misión y visión',
    //             alias: 'about-us',
    //             permisos: ['about-us'],
    //             path: '/about-us'
    //         },
    //         {
    //             name: 'Promociones en proceso',
    //             alias: 'promotions',
    //             permisos: ['promotions'],
    //             path: '/promotions'
    //         }
    //     ]
    // },
    // {
    //     name: 'Pagos',
    //     permisos: [],
    //     alias: '',
    //     children: [
    //         {
    //             name: 'Facturas',
    //             alias: 'bills',
    //             permisos: ['bills'],
    //             path: '/bills'
    //         },
    //         {
    //             name: 'Facturas por pagar',
    //             alias: 'bills-to-pay',
    //             permisos: ['bills-to-pay'],
    //             path: '/bills-to-pay'
    //         },
    //         {
    //             name: 'Notas de remisión',
    //             alias: 'referral-notes',
    //             permisos: ['referral-notes'],
    //             path: '/referral-notes'
    //         },
    //         {
    //             name: 'Notas de remisión por pagar',
    //             alias: 'referral-notes-to-pay',
    //             permisos: ['referral-notes-to-pay'],
    //             path: '/referral-notes-to-pay'
    //         },
    //         // {
    //         //     name: 'Comprobantes de pagos',
    //         //     alias: 'payment-vouchers',
    //         //     permisos: ['payment-vouchers'],
    //         //     path: '/payment-vouchers'
    //         // },
    //         {
    //             name: 'Relaciones de pago',
    //             alias: 'payment-relationships',
    //             permisos: ['payment-relationships'],
    //             path: '/payment-relationships'
    //         }
    //     ]
    // },
    // {
    //     name: 'Eventos',
    //     alias: 'events',
    //     path: '/events',
    //     permisos: ['events']
    // },
    // {
    //     name: 'Promociones',
    //     alias: 'promotions',
    //     path: '/promotions',
    //     permisos: ['promotions']
    // }
];

export default items;
