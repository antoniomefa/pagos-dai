export const ACTIONS = {
    'report': 7,
    'review': 8,
    'update': 10,
    'create': 9,
    'delete': 11,
    'sync': 7
};

export const BINARY_BASE = 2;
