import Service from 'helpers/service';

export const getMemoryStatus = () => getStatus('api/memories/status');

export const getPaymentRelationshipStatus = () => getStatus('api/paymentrelationships/status');

export const getPaymentVoucherStatus = () => getStatus('api/paymentvouchers/status');

export const getProjectStatus = () => getStatus('api/projects/status');

export const getProviderStatus = () => getStatus('api/providers/status');

export const getPurchaseOrderStatus = () => getStatus('api/purchaseorders/status');

export const getReferralNoteStatus = () => getStatus('api/referralnotes/status');

export const getAdvisoriesStatus = () => getStatus('api/advisories/status');

export const getBillsStatus = () => getStatus('api/taxdocuments/status');

const getStatus = (url) => {
    return new Promise((res, rej) => {
        return Service({}, url, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};


export const getRoles = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/roles', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};

export const getProjects = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/projects', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};

export const getOrders = id => {
    return new Promise((res, rej) => {
        return Service({}, `api/projects/${id}/purchaseorders`, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};

export const getOrdersCatalog = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/purchaseorders', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};

export const getWarehouses = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/warehouses', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};

export const getCustomerTypes = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/clienttypes', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });
};

export const getContacts = (purchaseOrderId) => {
    return new Promise((res, rej) => {
        return Service({}, `api/purchaseorders/${purchaseOrderId}/contacts`, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getResidents = (purchaseOrderId) => {
    return new Promise((res, rej) => {
        return Service({}, `api/purchaseorders/${purchaseOrderId}/residents`, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getBuyers = (purchaseOrderId) => {
    return new Promise((res, rej) => {
        return Service({}, `api/purchaseorders/${purchaseOrderId}/buyers`, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getTaxDocuments = (projectId) => {
    return new Promise((res, rej) => {
        return Service({ projectId }, 'api/taxdocuments/tomemory', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getReferralNotes = (projectId) => {
    return new Promise((res, rej) => {
        return Service({ projectId }, 'api/referralnotes/tomemory', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getProviders = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/providers', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getCustomers = () => {
    return new Promise((res, rej) => {
        return Service({}, 'api/clients', 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getPurchaseOrders = (providerId) => {
    return new Promise((res, rej) => {
        return Service({}, `api/providers/${providerId}/purchaseorders`, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};

export const getTaxDocumentsByOrder = (orderId) => {
    return new Promise((res, rej) => {
        return Service({}, `api/purchaseorders/${orderId}/taxdocuments`, 'get')
            .then(data => {
                res(data.data);
            })
            .catch(() => {
                rej();
            });
    });

};
