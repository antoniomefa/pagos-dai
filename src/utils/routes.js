// import Home from 'containers/home';
// import Promotions from 'containers/promotions';

import AboutUs from 'containers/aboutUs';
import AboutUsForm from 'containers/aboutUs/form';
import App from 'containers/app';
import Users from 'containers/users';
import UserProfile from 'containers/users/profile';
import UserForm from 'containers/users/form';
import Providers from 'containers/providers';
import ProviderForm from 'containers/providers/form';
import ProviderProfile from 'containers/providers/profile';
import Projects from 'containers/projects';
import ProjectForm from 'containers/projects/form';
import ProjectProfile from 'containers/projects/profile';
import Customers from 'containers/customers';
import CustomerProfile from 'containers/customers/profile';
import CustomerForm from 'containers/customers/form';
import Memories from 'containers/memories';
import Bills from 'containers/bills';
import BillsToPay from 'containers/billsToPay';
import ReferralNotes from 'containers/referralNotes';
import ReferralNotesToPay from 'containers/referralNotesToPay';
import Warehouses from 'containers/warehouses';
import WarehouseProfile from 'containers/warehouses/profile';
import WarehouseForm from 'containers/warehouses/form';
import PurchaseOrders from 'containers/purchaseOrders';
import PurchaseOrderProfile from 'containers/purchaseOrders/profile';
import PurchaseOrderForm from 'containers/purchaseOrders/form';
import PaymentVouchers from 'containers/paymentVouchers';
import PaymentRelationships from 'containers/paymentRelationships';
import AgainstReceipts from 'containers/againstReceipts';
import Login from 'containers/login';
import Register from 'containers/register';
import NotFound from 'containers/notfound';
import Advisories from 'containers/advisories';
import AdvisoryForm from 'containers/advisories/form';
import AdvisoryProfile from 'containers/advisories/profile';
import ReportMessages from 'containers/reports/messages';
import Configuration from 'containers/configuration';
import PromotionProfile from 'containers/promotions/profile';
import PromotionForm from 'containers/promotions/form';
import BillProfile from 'containers/bills/profile';
import BillForm from 'containers/bills/form';
import ReferralNoteProfile from 'containers/referralNotes/profile';
import ReferralNoteForm from 'containers/referralNotes/form';
import MemoryProfile from 'containers/memories/profile';
import MemoryForm from 'containers/memories/form';
import BillToPayProfile from 'containers/billsToPay/profile';
import BillToPayForm from 'containers/billsToPay/form';
import ReferralNoteToPayProfile from 'containers/referralNotesToPay/profile';
import ReferralNoteToPayForm from 'containers/referralNotesToPay/form';
import PaymentRelationshipProfile from 'containers/paymentRelationships/profile';
import PaymentRelationshipForm from 'containers/paymentRelationships/form';
import againstReceiptProfile from 'containers/againstReceipts/profile';
import againstReceiptForm from 'containers/againstReceipts/form';

// DAI
import AccountStatus from 'containers/accountStatus';
import RegisteredCards from 'containers/RegisteredCards';
import InvoicesHistory from 'containers/InvoicesHistory';

// events
import Events from 'containers/events';
import Promotions from 'containers/promotions';

const routes = [
    {
        path: '/sign-in',
        name: 'login',
        component: Login,
        authorizationRequired: false
    },
    {
        path: '/sign-up',
        name: 'register',
        component: Register,
        authorizationRequired: false
    },
    {
        path: '/',
        name: 'app',
        component: App,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio' }]
    },
    {
        path: '/events',
        name: 'events', component: Events,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/events', name: 'Eventos' }]
    },
    {
        path: '/promotions',
        name: 'promotions', component: Promotions,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/promotions', name: 'Promociones' }]
    },
    {
        path: '/users',
        module: 'users',
        name: 'users',
        component: Users,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/users', name: 'Usuarios' }],
        children: [
            {
                path: 'create',
                component: UserForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/users', name: 'Usuarios' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: UserProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/users', name: 'Usuarios' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: UserForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/users', name: 'Usuarios' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/customers',
        name: 'customers',
        module: 'customers',
        component: Customers,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/customers', name: 'Clientes' }],
        children: [
            {
                path: 'create',
                component: CustomerForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/customers', name: 'Clientes' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: CustomerProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/customers', name: 'Clientes' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: CustomerForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/customers', name: 'Clientes' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/providers',
        name: 'providers',
        module: 'providers',
        component: Providers,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/providers', name: 'Proveedores' }],
        children: [
            {
                path: ':id',
                component: ProviderProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/providers', name: 'Proveedores' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: ProviderForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/providers', name: 'Proveedores' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/memories',
        name: 'memories',
        module: 'memories',
        component: Memories,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/memories', name: 'Memorias' }],
        children: [
            {
                path: 'create',
                component: MemoryForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/memories', name: 'Memorias' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: MemoryProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/memories', name: 'Memorias' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: MemoryForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/memories', name: 'Memorias' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/purchase-orders',
        name: 'purchase_orders',
        module: 'purchase-orders',
        component: PurchaseOrders,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/purchase-orders', name: 'Ordenes de compras'}
        ],
        children: [
            {
                path: 'create',
                component: PurchaseOrderForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/purchase-orders', name: 'Ordenes de compras' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: PurchaseOrderProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/purchase-orders', name: 'Ordenes de compras' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: PurchaseOrderForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/purchase-orders', name: 'Ordenes de compras' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/projects',
        name: 'projects',
        module: 'projects',
        component: Projects,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/projects', name: 'Proyectos' }],
        children: [
            {
                path: 'create',
                component: ProjectForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/projects', name: 'Proyectos' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: ProjectProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/projects', name: 'Proyectos' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: ProjectForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/projects', name: 'Proyectos' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/bills',
        name: 'bills',
        module: 'bills',
        component: Bills,
        authorizationRequired: true,
        breadcrumb: [{ path: '/', name: 'Inicio'}, { path: '/bills', name: 'Facturas' }],
        children: [
            {
                path: 'create',
                component: BillForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/bills', name: 'Facturas' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: BillProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/bills', name: 'Facturas' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: BillForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/bills', name: 'Facturas' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/referral-notes',
        name: 'referral-notes',
        module: 'referral-notes',
        component: ReferralNotes,
        authorizationRequired: true,
        breadcrumb: [
            { path: '/', name: 'Inicio'},
            { path: '/referral-notes', name: 'Notas de remisión' }
        ],
        children: [
            {
                path: 'create',
                component: ReferralNoteForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/referral-notes', name: 'Notas de remisión' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: ReferralNoteProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/referral-notes', name: 'Notas de remisión' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: ReferralNoteForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/referral-notes', name: 'Notas de remisión' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/bills-to-pay',
        name: 'bills-to-pay',
        module: 'bills-to-pay',
        component: BillsToPay,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/bills-to-pay', name: 'Facturas por pagar' }
        ],
        children: [
            {
                path: 'create',
                component: BillToPayForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/bills-to-pay', name: 'Facturas por pagar' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: BillToPayProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/bills-to-pay', name: 'Facturas por pagar' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: BillToPayForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/bills-to-pay', name: 'Facturas por pagar' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/referral-notes-to-pay',
        name: 'referral-notes-to-pay',
        module: 'referral-notes-to-pay',
        component: ReferralNotesToPay,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/referral-notes-to-pay', name: 'Notas de remisión por pagar' }
        ],
        children: [
            {
                path: 'create',
                component: ReferralNoteToPayForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/referral-notes-to-pay', name: 'Notas de remisión por pagar' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: ReferralNoteToPayProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/referral-notes-to-pay', name: 'Notas de remisión por pagar' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: ReferralNoteToPayForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/referral-notes-to-pay', name: 'Notas de remisión por pagar' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/warehouses',
        name: 'warehouses',
        module: 'warehouses',
        component: Warehouses,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/warehouses', name: 'Almacenes' }
        ],
        children: [
            {
                path: 'create',
                component: WarehouseForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/warehouses', name: 'Almacenes' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: WarehouseProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/warehouses', name: 'Almacenes' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: WarehouseForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/warehouses', name: 'Almacenes' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/payment-vouchers',
        name: 'payment-vouchers',
        module: 'payment-vouchers',
        component: PaymentVouchers,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/payment-vouchers', name: 'Comprobantes de pagos' }
        ],
        children: [
            {
                path: 'create',
                component: NotFound,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/payment-vouchers', name: 'Comprobantes de pagos' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: NotFound,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/payment-vouchers', name: 'Comprobantes de pagos' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: NotFound,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/payment-vouchers', name: 'Comprobantes de pagos' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/payment-relationships',
        name: 'payment-relationships',
        module: 'payment-relationships',
        component: PaymentRelationships,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/payment-relationships', name: 'Relaciones de pagos' }
        ],
        children: [
            {
                path: 'create',
                component: PaymentRelationshipForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/payment-relationships', name: 'Relaciones de pagos' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: PaymentRelationshipProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/payment-relationships', name: 'Relaciones de pagos' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: PaymentRelationshipForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/payment-relationships', name: 'Relaciones de pagos' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/against-receipts',
        name: 'against-receipts',
        module: 'against-receipts',
        component: AgainstReceipts,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/against-receipts', name: 'Contrarecibos' }
        ],
        children: [
            {
                path: 'create',
                component: againstReceiptForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/against-receipts', name: 'Contrarecibos' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: againstReceiptProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/against-receipts', name: 'Contrarecibos' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: againstReceiptForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/against-receipts', name: 'Contrarecibos' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/advisories',
        name: 'advisories',
        module: 'advisories',
        component: Advisories,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/advisories', name: 'Comunicados' }
        ],
        children: [
            {
                path: 'create',
                component: AdvisoryForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/advisories', name: 'Comunicados' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: AdvisoryProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/advisories', name: 'Comunicados' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: AdvisoryForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/advisories', name: 'Comunicados' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/configuration',
        name: 'configuration',
        module: 'configuration',
        component: Configuration,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/configuration', name: 'Configuracion' }
        ],
        children: [
            {
                path: 'create',
                component: NotFound,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/configuration', name: 'Configuracion' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: NotFound,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/configuration', name: 'Configuracion' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: NotFound,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/configuration', name: 'Configuracion' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    { /////////********** Rutas para la pantalla de Estado de cuenta*************** */
        path: '/account-status',
        name: 'account-status',
        module: 'account-status',
        component: AccountStatus,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/account-status', name: 'Estado de cuenta' }
        ],
        children: [
            {
                path: 'invoices-history',
                component: InvoicesHistory,
                authorizationRequired: true,
                breadcrumb: [
                    {path: '/', name: 'Inicio'},
                    { path: '/account-status', name: 'Estado de cuenta' },
                    { path: '/invoices-history', name: 'Historial de facturas' }
                ]
            },
            {
                path: 'registered-cards',
                component: RegisteredCards,
                authorizationRequired: true,
                breadcrumb: [
                    {path: '/', name: 'Inicio'},
                    { path: '/account-status', name: 'Estado de cuenta' },
                    { path: '/registered-cards', name: 'Tarjetas registradas' }
                ]
            }
        ]
    }, //************************/*/*///*//***************************************** */
    {
        path: '/promotions',
        name: 'promotions',
        module: 'promotions',
        component: Promotions,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            { path: '/promotions', name: 'Promociones' }
        ],
        children: [
            {
                path: 'create',
                component: PromotionForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/promotions', name: 'Promociones' },
                    { path: 'create', name: 'Crear' }
                ]
            },
            {
                path: ':id',
                component: PromotionProfile,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/promotions', name: 'Promociones' },
                    { path: ':id', name: 'Perfil' }
                ]
            },
            {
                path: ':id/edit',
                component: PromotionForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/promotions', name: 'Promociones' },
                    { path: '/edit', name: 'Editar' }
                ]
            }
        ]
    },
    {
        path: '/about-us',
        name: 'about-us',
        module: 'about-us',
        component: AboutUs,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            {path: '/about-us', name: 'Acerca de nosotros'}
        ],
        children: [
            {
                path: 'edit',
                component: AboutUsForm,
                authorizationRequired: true,
                breadcrumb: [
                    { path: '/', name: 'Inicio'},
                    { path: '/about-us', name: 'Acerca de nosotros' },
                    { path: '/edit', name: 'Actualizar' }
                ]
            }
        ]
    },
    {
        path: '/report-messages',
        name: 'report-messages',
        isReport: true,
        module: 'report-messages',
        component: ReportMessages,
        authorizationRequired: true,
        breadcrumb: [
            {path: '/', name: 'Inicio'},
            {path: '/report-messages', name: 'Reportes de mensajes'}
        ]
    },
    {
        path: '/not-found',
        name: 'notFound',
        component: NotFound,
        authorizationRequired: true
    }
];

export default routes;
