import { NOT_FOUND, payloadIndex } from 'utils/constants';
import actions from './permissions';
import { getPhorter } from 'helpers';

const notificationFields = {
    message: {
        type: 'string',
        enum: false
    },
    level: {
        type: 'string',
        enum: ['success', 'error', 'warning', 'info']
    }
};

const noticeFields = {
    title: {
        type: 'string',
        enum: false
    },
    uid: {
        type: 'string',
        enum: false
    },
    type: {
        type: 'string',
        enum: false
    },
    message: {
        type: 'string',
        enum: false
    },
    autoDismiss: {
        type: 'number'
    },
    position: {
        type: 'string'
    },
    action: {
        type: 'object',
        enum: false
    },
    level: {
        type: 'string',
        enum: ['success', 'error', 'warning', 'info']
    }
};

const alertFields = {
    text: {
        type: 'string',
        enum: false
    },
    color: {
        type: 'string',
        enum: ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark']
    },
    alias: {
        type: 'string',
        enum: false
    },
    time: {
        type: 'number'
    }
};

export function defaultNotification(message) {
    return {
        level: 'warning',
        message
    };
}

export function checkNotification(notification) {
    var message = 'notification error: ';
    var success = true;

    Object.keys(notification).map(key => {
        const value = notification[key];
        const field = notificationFields[key];

        if (!field) {
            success = false;
            message += `- ${key} dont exists`;
        } else if (value.trim() == '' || (field.enum && field.enum.indexOf(value) == NOT_FOUND)) {
            success = false;
            message += `- ${key} is empty or is error value`;
        }
    });

    return {
        success,
        message
    };
}

export function defaultAlert(text) {
    return {
        color: 'warning',
        alias: 'error-1',
        time: 3,
        text
    };
}

export function checkAlert(alert) {
    var message = 'alert error: ';
    var success = true;

    Object.keys(alert).map(key => {
        const value = alert[key];
        const field = alertFields[key];

        if (!field) {
            success = false;
            message += `- ${key} dont exists`;
        } else if ((typeof value == 'string' && value.trim() == '') ||
            (field.enum && (field.enum.indexOf(value) == NOT_FOUND))) {
            success = false;
            message += `- ${key} is empty or is error value`;
        } else if (field.type == 'number' && (isNaN(parseInt(value)) && value != 'never')) {
            success = false;
            message += `- ${key} is empty or is error value`;
        }
    });

    return {
        success,
        message
    };
}

export function havePermission(item) {
    const payload = getPayloadPerModule(item);
    const permisos = payload.permisos;
    const children = payload.children;
    console.log(payload)

    if (permisos.length || (children && children.some(p => p.permisos.length))) {
        return payload;
    }

    return false;
}

export function getPayloadPerModule(module) {
    const ONE = 1;
    var permisos = [];
    const permissions = getMyPermissions();
    const permissionsSplitted = permissions.split('.');
    const payloadPosition = payloadIndex[module.alias];
    const payloadModule = permissionsSplitted[payloadPosition - ONE];
    const payloadNumeric = parseInt(payloadModule);

    if (!isNaN(payloadNumeric)) {
        actions.map(action => {
            if (action.name == module.alias) {
                permisos = action.permissions.filter(p => (p.value & payloadNumeric) == p.value);
            }
        });
    }

    return {
        module: module.alias,
        permisos,
        children: module.children && module.children.map(m => getPayloadPerModule(m))
    };
}

export function getMyPermissions() {
    return getPhorter() || '0';
}

export function defaultNotice(text) {
    return  {
      // uid: 'once-please', // you can specify your own uid if required
      title: 'Hey, algo esta mal',
      message: text,
      position: 'tr',
      autoDismiss: 0,
      action: {
        label: 'Click me!!',
        callback: () => alert('clicked!')
      }
    };
}

export function checkNotice(notice) {
    var message = 'notice error: ';
    var success = true;

    Object.keys(notice).map(key => {
        const value = notice[key];
        const field = noticeFields[key];

        if (!field) {
            success = false;
            message += `- ${key} dont exists`;
        } else if ((typeof value == 'string' && value.trim() == '') ||
            (field.enum && (field.enum.indexOf(value) == NOT_FOUND))) {
            success = false;
            message += `- ${key} is empty or is error value`;
        } else if (field.type == 'number' && (isNaN(parseInt(value)) && value != 'never')) {
            success = false;
            message += `- ${key} is empty or is error value`;
        }
    });

    return {
        success,
        message
    };
}

export function getData(object, keys, defaultVal = null) {
    const ZERO = 0;
    const ONE = 1;

    keys = Array.isArray(keys) ? keys : keys.replace(/(\[(\d)\])/g, '.$2').split('.');
    object = object[keys[ZERO]];
    if (object && keys.length > ONE) {
        return getData(object, keys.slice(ONE), defaultVal);
    }

    return !object ? defaultVal : object;

}
