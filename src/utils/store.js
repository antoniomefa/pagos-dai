import { createStore, applyMiddleware } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import reducers from 'reducers';
import thunk from 'redux-thunk';

const middleware = routerMiddleware(browserHistory);
const store = createStore(reducers, applyMiddleware(middleware, thunk));

export const  history = syncHistoryWithStore(browserHistory, store);
export default store;
