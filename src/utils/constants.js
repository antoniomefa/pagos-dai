
export const NOT_FOUND = -1;
export const ZERO = 0;
export const ONE = 1;
export const MIL = 1000;

// payload permissions
export const payloadIndex = {
    // dai
    //'events': 1,

    // example
    'account-status': 1,
    'registered-cards': 2
    // 'users': 11,
    // 'providers': 12,
    // 'projects': 13,
    // 'report-messages': 7,
    // 'customers': 14,
    // 'bills': 1,
    // 'referral-notes': 1,
    // 'memories': 3,
    // 'purchase-orders': 6,
    // 'bills-to-pay': 2,
    // 'referral-notes-to-pay': 2,
    // 'payment-vouchers': 4,
    // 'payment-relationships': 5,
    // 'against-receipts': 17,
    // 'configuration': 8,
    // 'about-us': 9,
    // 'promotions': 10,
    // 'warehouses': 15,
    // 'advisories': 16
};
