// import store from 'utils/store';
// import { goSignIn } from 'redux-actions/auth';
// import { ON_RENEW_SESSION } from 'action-types/auth';
import JSZip from 'jszip';
import { saveAs } from 'file-saver/FileSaver';

export const createSession = payload => {
    const localName = '__b_m_usr_d';
    const data = btoa(JSON.stringify(payload));

    localStorage.setItem(localName, data);
};

export const getSession = () => {
    const localName = '__b_m_usr_d';
    const data = localStorage.getItem(localName);

    if (!data) return null;

    return JSON.parse(atob(data));
};

export const removeSession = () => {
    const localName = '__b_m_usr_d';
    localStorage.removeItem(localName);
};

export const saveAccess = (accessToken, phorther) => {
    createAccessToken(accessToken);
    createPhorter(phorther);
};

export const createAccessToken = payload => {
    const localName = '__b_m_usr_ae';
    const data = btoa(JSON.stringify(payload));

    localStorage.setItem(localName, data);
};

export const getAccessToken = () => {
    const localName = '__b_m_usr_ae';
    const data = localStorage.getItem(localName);

    if (!data) return null;

    return JSON.parse(atob(data));
};

export const removeAccessToken = () => {
    const localName = '__b_m_usr_ae';
    localStorage.removeItem(localName);
};

export const createPhorter = payload => {
    const localName = '__b_m_usr_ph';
    const data = btoa(JSON.stringify(payload));

    localStorage.setItem(localName, data);
};

export const getPhorter = () => {
    //const localName = '__b_m_usr_ph';
    const data = 'IjEyNzIzMy40OTE1NC4xOTMyODMuMjYwLjMyMzU4OS44MzgxNTAuMTM1LjQ5MTYwLjQ5MTYxLjYxNDUwLjYxNDUxLjUxMjEyLjUxMjEzLjUxMjE0LjYxNDU1LjMyMzYwMC42MTQ1NyI=';
    if (!data) return null;

    return JSON.parse(atob(data));
};

export const removePhorter = () => {
    const localName = '__b_m_usr_ph';
    localStorage.removeItem(localName);
};

export const logout = () => {
    removePhorter();
    removeSession();
    removeAccessToken();
};

export const downloadB64 = (mime, name, base64) => {
    const ZERO = 0;
    const NOT_FOUND = -1;
    var contentType = `image/${mime}`;
    var sliceSize = 1024;
    var byteCharacters = atob(base64);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);
    var sliceIndex = ZERO;
    var begin, end, bytes, offset, i, blob, match;

    if (mime === 'pdf') contentType = 'application/pdf';
    else if (mime === 'xml') contentType = 'text/xml';

    for (sliceIndex = ZERO; sliceIndex < slicesCount; ++sliceIndex) {
        begin = sliceIndex * sliceSize;
        end = Math.min(begin + sliceSize, bytesLength);

        bytes = new Array(end - begin);
        for (offset = begin, i = ZERO; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(ZERO);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
    }

    blob = new Blob(byteArrays, {
        type: contentType
    });

    match = navigator.userAgent.search(/(?:Edge|MSIE|Trident\/.*; rv:)/);
    if (match !== NOT_FOUND) {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, name);
        }
    } else {

    const a = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';

    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = name;
    a.click();
    window.URL.revokeObjectURL(url);
  }

};

export const downloadB64AsZIP = (array, zipName) => {
    // array => mime, name, base64
    const zipArchive = new JSZip();
    const memoryName = zipName;
    const zip = zipArchive.folder(memoryName);
    const ZERO = 0;
    const NOT_FOUND = -1;

    array.forEach(({ mime, name, data }) => {
        var contentType = `image/${mime}`;
        var sliceSize = 1024;
        var byteCharacters = atob(data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);
        var sliceIndex = ZERO;
        var begin, end, bytes, offset, i, blob, match;

        if (mime === 'pdf') contentType = 'application/pdf';
        else if (mime === 'xml') contentType = 'text/xml';

        for (sliceIndex = ZERO; sliceIndex < slicesCount; ++sliceIndex) {
            begin = sliceIndex * sliceSize;
            end = Math.min(begin + sliceSize, bytesLength);

            bytes = new Array(end - begin);
            for (offset = begin, i = ZERO; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(ZERO);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }

        blob = new Blob(byteArrays, {
            type: contentType
        });

        match = navigator.userAgent.search(/(?:Edge|MSIE|Trident\/.*; rv:)/);
        if (match !== NOT_FOUND) {
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob, name);
            }
        } else {
            zip.file(name, blob);
        }
    });

    zipArchive.generateAsync({type:'blob'}).then(function(content) {
        // see FileSaver.js
        saveAs(content, `${memoryName}.zip`);
    });
};
