import { URL_BASE } from 'source/constants';
import { ON_RENEW_SESSION } from 'action-types/auth';
import { sendNotification } from 'actions';
import { saveAccess, logout, getAccessToken } from 'helpers';
import store from 'utils/store';
const STATUS_OK = 200;
const UNAUTHORIZED = 401;

const Service = (body, api, method = 'get', isJson = true) => {
    const accessToken = getAccessToken();
    let query = '';
    var objFetch = {};
    var headers = {
        'Accept': 'application/json'
    };

    if (isJson) {
        headers = {
            ...headers,
            'Content-Type': 'application/json'
        };
    }

    if (accessToken) {
        headers = {
            ...headers,
            Authorization: accessToken
        };
    }

    if (method === 'get') {
        let esc = encodeURIComponent;
        query = Object.keys(body)
             .map(k => esc(k) + '=' + esc(body[k]))
             .join('&');
        objFetch = { method, headers };
    } else {
        objFetch = { method, headers, body };
    }


    return fetch(`${URL_BASE}/${api}?${query}`, objFetch)
        .then(resp => {
            const accessToken = resp.headers.get('access-token');
            const phorther = resp.headers.get('phorther');

            if (accessToken && phorther) {
                saveAccess(accessToken, phorther);
            }

            return resp.json().then(d => {
                return ({
                    json: d, status: resp.status
                });
            });
        })
        .then(resp => {
            const message401 = 'No tienes acceso';

            if (resp.status === UNAUTHORIZED) {
                logout();
                store.dispatch({ type: ON_RENEW_SESSION, payload: null });
                store.dispatch(sendNotification({ message: message401, level: 'warning' }));
                location.reload(true);
            } else if (resp.status != STATUS_OK) {
                throw resp.json;
            }

            return resp.json;
        })
        .then(json => json)
        .catch(err => {
            if (typeof err === 'object') {
                const message500 = 'Hubo un error en el servidor';
                store.dispatch(sendNotification({ message: message500, level: 'warning' }));
            } else if (typeof err === 'string') {
                store.dispatch(sendNotification({ message: err, level: 'warning' }));
            }
            throw err;
        });
};

export default Service;
