
export const ACTION_DEFAULT = 'ACTION_DEFAULT';
export const ON_ERASE_NOTIFICATION = 'App/Default/ON_ERASE_NOTIFICATION';
export const ON_SEND_NOTIFICATION = 'App/Default/ON_SEND_NOTIFICATION';
export const ON_SHOW_ALERT = 'App/Default/ON_SHOW_ALERT';
export const ON_ERASE_ALERT = 'App/Default/ON_ERASE_ALERT';
