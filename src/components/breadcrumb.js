import React from 'react';
import PropTypes from 'prop-types';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router';
import { ONE } from 'utils/constants';

const BreadcrumbComponent = ({ route }) => (
    route.breadcrumb ? (
        <Breadcrumb >
            {
                route.breadcrumb.map((bread, index) => (
                    <BreadcrumbItem key={index} active={index == (route.breadcrumb.length - ONE)}>
                        {
                            index == route.breadcrumb.length - ONE ? ( <span>{bread.name}</span> ) : ( <Link to={bread.path}>{bread.name}</Link> )
                        }
                    </BreadcrumbItem>
                ))
            }
        </Breadcrumb>
    ) : null
);

BreadcrumbComponent.propTypes = {
    route: PropTypes.object
};

export default BreadcrumbComponent;
