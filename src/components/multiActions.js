import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class multiActions extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);

        this.state = {
            tooltipOpen: false,
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

    toggleDropdown() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        const { actions } = this.props;

        if (!actions.length) return <div />;

        return (
            <Dropdown
                isOpen={this.state.dropdownOpen}
                toggle={this.toggleDropdown}
                size="sm"
            >
                <DropdownToggle caret color={'btn btn-light'}>
                    <small>Acciones masivas</small>
                </DropdownToggle>
                <DropdownMenu>
                    {
                        actions.map((action, index) => (
                            <DropdownItem
                                key={index}
                                onClick={() => this.props.selectAction(action)}
                            >
                                {action.label}
                            </DropdownItem>
                        ))
                    }
                </DropdownMenu>
            </Dropdown>
        );
    }
}


multiActions.propTypes = {
    actions: PropTypes.array,
    selectAction: PropTypes.func
};

multiActions.defaultProps = {
    actions: []
};

export default multiActions;
