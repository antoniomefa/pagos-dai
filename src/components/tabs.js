import React from 'react';
import PropTypes from 'prop-types';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row } from 'reactstrap';
import classnames from 'classnames';

class Tabs extends React.Component {
    constructor(props) {
        super(props);
    }

    toggle = to => {
        this.props.toggle(to);
    }

    render() {
        const { activeTab } = this.props;
        return (
            <div>
                <Nav tabs>
                    {
                        this.props.tabs.map((tab, index) => (
                            <NavItem key={index}>
                                <NavLink
                                    className={classnames({ active: activeTab === tab.id })}
                                    onClick={() => { this.toggle(tab.id); }}
                                >
                                    {tab.title}
                                </NavLink>
                            </NavItem>
                        ))
                    }
                </Nav>
                <TabContent activeTab={activeTab}>
                    {
                        this.props.tabs.map((tab, index) => (
                            <TabPane key={index} tabId={tab.id}>
                                <Row>
                                    {tab.Component}
                                </Row>
                            </TabPane>
                        ))
                    }
                </TabContent>
            </div>
        );
    }
}

Tabs.propTypes = {
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    toggle: PropTypes.func
};

export default Tabs;
