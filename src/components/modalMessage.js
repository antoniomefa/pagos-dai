import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { getBuyers, getResidents, getContacts } from 'utils/catalogs';
import Select from 'components/select';
import Input from 'components/input';

class ModalMessage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            formRequested: false,
            invalidForm: false,
            errorMessages: {},
            formValues: {
                buyers: [],
                contacts: [],
                residents: []
            },
            buyers: [],
            residents: [],
            contacts: []
        };
    }

    componentDidMount() {
        const { purchaseOrderId } = this.props;

        getBuyers(purchaseOrderId).then(buyers => this.setState({ buyers }));
        getContacts(purchaseOrderId).then(contacts => this.setState({ contacts }));
        getResidents(purchaseOrderId).then(residents => this.setState({ residents }));
    }

    getOptionsBuyer = () => {
        return this.state.buyers.map(r => ({ value: r.id, label: r.name }));
    }

    getOptionsContact = () => {
        return this.state.contacts.map(r => ({ value: r.id, label: r.name }));
    }

    getOptionsResident = () => {
        return this.state.residents.map(r => ({ value: r.id, label: r.name }));
    }

    onChange = (field, e) => {
        const value = e.target.value;
        const formValues = this.state.formValues;

        formValues[field] = value;

        this.setState({ formValues });
    }

    onChangeSelect = (field, select) => {
        const formValues = this.state.formValues;
        if (select) {
            if (Array.isArray(select)) {
                formValues[field] = select;
            } else {
                formValues[field] = select.value;
            }
        } else {
            delete formValues[field];
        }
        this.setState({ formValues });
    }

    __isEditing = () => this.props.purchaseOrderId;

    onSendData = () => {

        const contacts = [
            ...this.state.formValues.buyers,
            ...this.state.formValues.contacts,
            ...this.state.formValues.residents
        ];

        if (!contacts.length) {
            this.props.notification('No se seleccionaron destinatarios');
            return;
        }

        this.props.onBtnAction(
            contacts,
            this.state.formValues.observations
        );
    }

    render() {
        const { formValues, formRequested, errorMessages } = this.state;

        return (
            <div>
                <Modal
                    isOpen={this.props.isOpen}
                    toggle={this.props.toggle}
                    className={this.props.className}
                >
                    <ModalHeader toggle={this.props.toggle}>
                        {this.props.title || 'Envio de mensaje'}
                    </ModalHeader>
                    <ModalBody>
                        <div className="form-row">
                            <Select
                                hasLabel
                                label="Contactos"
                                htmlFor="contacts"
                                requested={formRequested}
                                onChange={this.onChangeSelect.bind(this, 'contacts')}
                                onBlur={() => {}}
                                id="contacts"
                                multi
                                value={formValues['contacts']}
                                placeholder="selecciona contactos..."
                                options={this.getOptionsContact()}
                                hasError={!!errorMessages['contacts']}
                                invalidText={errorMessages['contacts']}
                            />
                            <Select
                                hasLabel
                                label="Compradores"
                                multi
                                htmlFor="buyers"
                                requested={formRequested}
                                onChange={this.onChangeSelect.bind(this, 'buyers')}
                                onBlur={() => {}}
                                id="buyers"
                                value={formValues['buyers']}
                                placeholder="selecciona compradores..."
                                options={this.getOptionsBuyer()}
                                hasError={!!errorMessages['buyers']}
                                invalidText={errorMessages['buyers']}
                            />
                        </div>
                        <div className="form-row">
                            <Select
                                hasLabel
                                label="Residentes"
                                htmlFor="residents"
                                requested={formRequested}
                                onChange={this.onChangeSelect.bind(this, 'residents')}
                                onBlur={() => {}}
                                id="residents"
                                multi
                                value={formValues['residents']}
                                placeholder="selecciona residentes..."
                                options={this.getOptionsResident()}
                                hasError={!!errorMessages['residents']}
                                invalidText={errorMessages['residents']}
                            />
                            <Input
                                hasLabel
                                label="Observaciones"
                                htmlFor="observations"
                                requested={formRequested}
                                id="observations"
                                onChange={this.onChange.bind(this, 'observations')}
                                onBlur={() => {}}
                                value={formValues['observations']}
                                placeholder="escriba sus observaciones..."
                                aria="observations"
                                hasError={!!errorMessages['observations']}
                                invalidText={errorMessages['observations']}
                            />
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
                        <Button
                            color="primary"
                            onClick={this.onSendData}
                        >
                            {this.props.btnText || 'Enviar'}
                        </Button>{' '}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

ModalMessage.propTypes = {
    onBtnAction: PropTypes.func,
    notification: PropTypes.func,
    isOpen: PropTypes.bool.isRequired,
    title: PropTypes.string,
    btnText: PropTypes.string,
    toggle: PropTypes.func,
    className: PropTypes.string,
    purchaseOrderId: PropTypes.number.isRequired
};

export default ModalMessage;
