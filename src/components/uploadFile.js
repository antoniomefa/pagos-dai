import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/button';
import { uploadFile } from 'utils/files';

class UploadFile extends Component {

    constructor(props) {
        super(props);

        this.state ={
            loading: false
        };
    }

    handleClickButton = () => {
        this.file.click();
    }

    onFileUploaded = e => {
        const ZERO = 0;
        const file = e.target.files[ZERO];

        e.preventDefault();

        this.setState({
            loading: true
        }, () => {
            uploadFile(file)
                .then(data => {
                    this.setState({
                        loading: false
                    }, () => this.props.onFileUploaded(data.url));
                })
                .catch(() => {
                    this.setState({
                        loading: false
                    });
                });
        });
    }

    render() {
        return (
            <div className="col-xs-12 col-sm-6">
                <div>
                    <label>{this.props.label || 'Archivo'}</label>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Button
                            text="Cancelar"
                            type="dark"
                            outline
                            isIcon
                            icon={this.state.loading ? 'spinner fa-spin' : 'upload'}
                            extraStyle={{ margin: 0 }}
                            extraStyleParent={{ marginRight: 10 }}
                            onButtonPress={this.handleClickButton}
                            needPermission={false}
                        />
                        <small>{this.props.previousName}</small>
                    </div>
                </div>
                <input
                    type="file"
                    className="invisible"
                    accept={this.props.types}
                    ref={(ref) => { this.file = ref; }}
                    onChange={this.onFileUploaded}
                />
            </div>
        );
    }

}

UploadFile.defaultProps = {
    types: 'application/pdf'
};

UploadFile.propTypes = {
    types: PropTypes.string,
    label: PropTypes.string,
    onFileUploaded: PropTypes.func,
    previousName: PropTypes.string
};

export default UploadFile;
