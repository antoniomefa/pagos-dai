import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
    hasError,
    htmlFor,
    label,
    requested,
    parentClass,
    hasLabel,
    isGroup,
    icon,
    iconText,
    type,
    id,
    placeholder,
    required,
    aria,
    invalidText,
    validText,
    hasIcon,
    value,
    onChange,
    onFocus,
    onBlur
}) => (
    <div className={parentClass}>
        <div className="form-group">
            {
                hasLabel && (
                    <label htmlFor={htmlFor}>
                        {label} { required && <i className="isRequired">*</i>}
                    </label>
                )
            }
            {
                isGroup ? (
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text">
                                {
                                    hasIcon ? (
                                        <i className={`fa fa-${icon}`}></i>
                                    ) : (
                                        <i>{iconText}</i>
                                    )
                                }
                            </span>
                        </div>
                        {
                            requested && required ? (
                                <input
                                    type={type}
                                    className={`form-control ${hasError ? 'is-invalid':'is-valid'}`}
                                    id={id}
                                    placeholder={placeholder}
                                    value={value}
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    aria-describedby={aria}
                                />
                            ) : (
                                <input
                                    type={type}
                                    className='form-control'
                                    id={id}
                                    value={value}
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    placeholder={placeholder}
                                    aria-describedby={aria}
                                />

                            )
                        }
                        {
                            requested && required && (
                                hasError ? (
                                    <div className="invalid-feedback">
                                        {invalidText}
                                    </div>
                                ) : (
                                    <div className="valid-feedback">
                                        {validText}
                                    </div>
                                )
                            )
                        }
                    </div>
                ) : (
                    <div>
                        {
                            requested ? (
                                <input
                                    type={type}
                                    className={`form-control ${hasError ? 'is-invalid':'is-valid'}`}
                                    id={id}
                                    value={value}
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    placeholder={placeholder}
                                    aria-describedby={aria}
                                />
                            ) : (
                                <input
                                    type={type}
                                    className='form-control'
                                    id={id}
                                    value={value}
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    placeholder={placeholder}
                                    aria-describedby={aria}
                                />

                            )
                        }
                        {
                            requested && (
                                hasError ? (
                                    <div className="invalid-feedback">
                                        {invalidText}
                                    </div>
                                ) : (
                                    <div className="valid-feedback">
                                        {validText}
                                    </div>
                                )
                            )
                        }
                    </div>
                )
            }
        </div>
    </div>
);

Input.propTypes = {
    hasLabel: PropTypes.bool,
    label: PropTypes.string,
    parentClass: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    htmlFor: PropTypes.string,
    isGroup: PropTypes.bool,
    icon: PropTypes.string,
    iconText: PropTypes.string,
    hasError: PropTypes.bool,
    requested: PropTypes.bool,
    type: PropTypes.string,
    id: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    aria: PropTypes.string,
    invalidText: PropTypes.string,
    validText: PropTypes.string,
    required: PropTypes.bool,
    hasIcon: PropTypes.bool
};

Input.defaultProps = {
    hasLabel: false,
    parentClass: 'col-xs-12 col-sm-6',
    isGroup: false,
    hasIcon: false,
    requested: false,
    onChange: () => {},
    onFocus: () => {},
    onBlur: () => {},
    value: '',
    type: 'text',
    placeholder: '',
    aria: '',
    invalidText: '',
    validText: '',
    required: false
};

export default Input;
