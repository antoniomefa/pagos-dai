import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

class Filters extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showFilters: false,
            filtered: {
                ...this.props.filtered,
                filters: JSON.parse(this.props.filtered.filters)
            }
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.filtered != this.props.filtered) {
            this.setState({
                filtered: {
                    ...nextProps.filtered,
                    filters: JSON.parse(nextProps.filtered.filters)
                }
            });
        }
    }

    __getIcon = () => this.state.showFilters ? 'caret-up' : 'caret-down';

    __changeStateFromFilters = () => {
        this.setState({
            showFilters: !this.state.showFilters
        });
    }

    onChangeFilter = (field, selected) => {
        const { filtered } = this.state;

        filtered.filters[field] = selected ? selected.value : selected;
        if (!selected) delete filtered.filters[field];

        this.setState({ filtered });
    }

    __applyFilters = () => {
        this.props.applyFilters(this.state.filtered);
        this.setState({
            showFilters: false
        });
    }

    __clearFilters = () => {
        this.setState({
            showFilters: false,
            filtered: { ...this.state.filtered, filters: {}, q: '' }
        });

        this.props.applyFilters({ ...this.state.filtered, filters: {}, q: '' });
    }

    __changeSearch = e => {
        this.setState({
            filtered: {
                ...this.state.filtered,
                q: e.target.value
            }
        });
    }

    __showFilters = () => {
        const { filters } = this.props;
        const { filtered } = this.state;

        if (!this.state.showFilters) return null;
        return (
            <div style={{ paddingBottom: 10 }}>
                <div style={{ display: 'flex' }}>
                    <div className="col-md-2 col-sm-4 col-xs-12">
                        <input
                            placeholder="Buscar"
                            className="form-control"
                            value={filtered.q}
                            onChange={this.__changeSearch}
                        />
                    </div>
                    {
                        filters.map((filter, index) => (
                            <div className="col-md-2 col-sm-4 col-xs-12" key={index}>
                                <Select
                                    name={`form-field-${filter.field}`}
                                    value={filtered.filters[filter.field]}
                                    placeholder={filter.name}
                                    onChange={this.onChangeFilter.bind(this, filter.field)}
                                    options={filter.options}
                                />
                            </div>
                        ))
                    }
                </div>
                <div style={{ display: 'flex', justifyContent: 'flex-end', padding: 5 }}>
                    <button className="btn btn-outline-dark" onClick={this.__applyFilters}>
                        <small><span className="fa fa-filter" /> Aplicar filtros</small>
                    </button>
                </div>
            </div>
        );
    }

    __haveAnyFilter = () => {
        const { filtered } = this.state;

        return filtered.q != '' || Object.keys(filtered.filters).some(f => filtered.filters[f]);
    }

    __showButtonCancelFilters = () => {
        if (!this.__haveAnyFilter()) return null;

        return (
            <button
                className="btn btn-light"
                title={'Limiar'}
                style={{ position: 'relative' }}
                onClick={this.__clearFilters}
            >
                <span className="fa fa-filter" />
                <span
                    className="fa fa-times-circle"
                    style={{
                        position: 'absolute',
                        fontSize: 11,
                        top: 5,
                        right: 3
                    }}
                />
            </button>
        );
    }

    __showButtonFilters = () => {
        return (
            <div>
                {this.__showButtonCancelFilters()}
                <button className="btn btn-light" onClick={this.__changeStateFromFilters}>
                    <small>
                        mostrar filtros <span className={`fa fa-${this.__getIcon()}`}/>
                    </small>
                </button>
            </div>
        );
    }

    __getMultiActions = () => {
        if (!this.props.selected) return <div />;
        return this.props.multiActions;
    }

    render() {
        return (
            <div>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: 5,
                        alignItems: 'center'
                    }}
                >
                    {this.__getMultiActions()}
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        {this.__showButtonFilters()}
                        {this.props.actions}
                    </div>
                </div>
                {this.__showFilters()}
            </div>
        );
    }
}

Filters.defaultProps = {
    filtered: {}
};

Filters.propTypes = {
    filters: PropTypes.array,
    applyFilters: PropTypes.func,
    filtered: PropTypes.object,
    children: PropTypes.element,
    actions: PropTypes.element,
    multiActions: PropTypes.element,
    selected: PropTypes.number
};

export default Filters;
