import React, { Component } from 'react';
import CKEditor from 'react-ckeditor-component';
import PropTypes from 'prop-types';

class DraftEditor extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(evt){
        var newContent = evt.editor.getData();

        this.props.changeEditorContent(newContent);
    }

    render() {
        const {
            requested,
            required,
            invalidText,
            hasError,
            validText,
            label,
            className,
            isEditing,
            content
        } = this.props;

        if (isEditing && content == null) return null;
        return (
            <div className={className} style={this.props.parentStyle}>
                <label>{label} { required && <i className="isRequired">*</i>}</label>
                <CKEditor
                    activeClass="p10"
                    content={this.props.content}
                    events={{
                        'blur': this.props.onBlur,
                        'change': this.onChange
                    }}
                />
                <input type="hidden" className={`form-control ${hasError ? 'is-invalid' : ''}`} />
                {
                    requested && required && (
                        hasError ? (
                            <div className="invalid-feedback">
                                {invalidText}
                            </div>
                        ) : (
                            <div className="valid-feedback">
                                {validText}
                            </div>
                        )
                    )
                }
            </div>
        );
    }
}

DraftEditor.defaultProps = {
    content: '',
    className: '',
    parentStyle: {},
    onBlur: () => {},
    changeEditorContent: () => {}
};

DraftEditor.propTypes = {
    content: PropTypes.string,
    parentStyle: PropTypes.object,
    className: PropTypes.string,
    requested: PropTypes.bool,
    required: PropTypes.bool,
    invalidText: PropTypes.string,
    onBlur: PropTypes.func,
    hasError: PropTypes.bool,
    validText: PropTypes.string,
    label: PropTypes.string,
    isEditing: PropTypes.bool,
    changeEditorContent: PropTypes.func
};

export default DraftEditor;
