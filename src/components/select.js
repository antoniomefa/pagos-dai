import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

const SelectComponent = ({
    hasError,
    htmlFor,
    label,
    requested,
    parentClass,
    hasLabel,
    isGroup,
    icon,
    iconText,
    options,
    id,
    placeholder,
    required,
    invalidText,
    validText,
    hasIcon,
    value,
    onChange,
    onFocus,
    onBlur,
    multi
}) => (
    <div className={parentClass}>
        <div className="form-group">
            {
                hasLabel && (
                    <label htmlFor={htmlFor}>
                        {label} { required && <i className="isRequired">*</i> }
                    </label>
                )
            }
            {
                isGroup ? (
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text">
                                {
                                    hasIcon ? (
                                        <i className={`fa fa-${icon}`}></i>
                                    ) : (
                                        <i>{iconText}</i>
                                    )
                                }
                            </span>
                        </div>
                        {
                            requested && required ? (
                                <Select
                                    className={`form-control ${hasError ? 'is-invalid':'is-valid'}`}
                                    id={id}
                                    placeholder={placeholder}
                                    value={value}
                                    onChange={onChange}
                                    multi={multi}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    options={options}
                                />
                            ) : (
                                <Select
                                    className='form-control'
                                    id={id}
                                    value={value}
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    multi={multi}
                                    onBlur={onBlur}
                                    placeholder={placeholder}
                                    options={options}
                                />
                            )
                        }
                        {
                            requested && required && (
                                hasError ? (
                                    <div className="invalid-feedback">
                                        {invalidText}
                                    </div>
                                ) : (
                                    <div className="valid-feedback">
                                        {validText}
                                    </div>
                                )
                            )
                        }
                    </div>
                ) : (
                    <div>
                        {
                            requested ? (
                                <Select
                                    className={`form-control ${hasError ? 'is-invalid':'is-valid'}`}
                                    id={id}
                                    value={value}
                                    onChange={onChange}
                                    multi={multi}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    placeholder={placeholder}
                                    options={options}
                                />
                            ) : (
                                <Select
                                    className='form-control'
                                    id={id}
                                    value={value}
                                    onChange={onChange}
                                    multi={multi}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    placeholder={placeholder}
                                    options={options}
                                />

                            )
                        }
                        {
                            requested && (
                                hasError ? (
                                    <div className="invalid-feedback">
                                        {invalidText}
                                    </div>
                                ) : (
                                    <div className="valid-feedback">
                                        {validText}
                                    </div>
                                )
                            )
                        }
                    </div>
                )
            }
        </div>
    </div>
);

SelectComponent.propTypes = {
    hasLabel: PropTypes.bool,
    label: PropTypes.string,
    parentClass: PropTypes.string,
    value: PropTypes.PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.array
    ]),
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    htmlFor: PropTypes.string,
    isGroup: PropTypes.bool,
    icon: PropTypes.string,
    iconText: PropTypes.string,
    hasError: PropTypes.bool,
    requested: PropTypes.bool,
    id: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    options: PropTypes.array,
    invalidText: PropTypes.string,
    validText: PropTypes.string,
    required: PropTypes.bool,
    hasIcon: PropTypes.bool,
    multi: PropTypes.bool
};

SelectComponent.defaultProps = {
    hasLabel: false,
    parentClass: 'col-xs-12 col-sm-6',
    isGroup: false,
    hasIcon: false,
    requested: false,
    onChange: () => {},
    onFocus: () => {},
    onBlur: () => {},
    options: [],
    multi: false,
    value: '',
    placeholder: '',
    invalidText: '',
    validText: '',
    required: false
};

export default SelectComponent;
