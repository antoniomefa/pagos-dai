import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const specialEvent = props => (
    <div style={{ display: 'flex' }}>

        <div className="__row_center">
            <div className="__tube_timeline" />
            <div className="__circle_timeline" style={{ backgroundColor: props.background }}>
                <span className={`fa fa-${props.icon}`} />
            </div>
            <div className="__tube_timeline" />
        </div>

        <div className="__row_center" style={{ marginLeft: 10, fontSize: 12 }}>
            <span>{moment(props.event.created_at).format('DD-MM-YYYY HH:mm')}</span>
            <div><small><strong>{props.event.codigo}</strong></small></div>
        </div>

        <div className="__card_event">
            <div className="__card_inner_event">
                <div
                    className="__card_line_vertical"
                    style={{ backgroundColor: props.background }}
                />
                <div style={{ width: 220, height: '100%' }}>
                    <div><span style={{ textOverflow: 'elipssis' }}>
                        {props.event.nombre}
                    </span></div>
                </div>
            </div>
        </div>
    </div>
);

specialEvent.propTypes = {
    event: PropTypes.object,
    background: PropTypes.string,
    icon: PropTypes.string
};

export default specialEvent;
