import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ONE, ZERO } from 'utils/constants';
import { Tooltip } from 'reactstrap';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class headButtons extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);

        this.state = {
            tooltipOpen: false,
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

    toggleDropdown() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        const { buttons } = this.props;

        return (
            <div>
                {
                    buttons.length == ONE ? (
                        <div>
                            <button
                                id={'tooltip'}
                                onClick={buttons[ZERO].action}
                                className='btn btn-light'
                            >
                                <small>{buttons[ZERO].label}</small>
                            </button>
                            <Tooltip
                                placement="right"
                                isOpen={this.state.tooltipOpen}
                                target="tooltip"
                                toggle={this.toggle}
                            >
                                {buttons[ZERO].tooltip}
                            </Tooltip>
                        </div>
                    ) : buttons.length > ONE && (
                        <Dropdown
                            isOpen={this.state.dropdownOpen}
                            toggle={this.toggleDropdown}
                            size="sm"
                        >
                            <DropdownToggle caret color={'btn btn-light'}>
                                <small>Acciones</small>
                            </DropdownToggle>
                            <DropdownMenu>
                                {
                                    buttons.map((button, index) => (
                                        <DropdownItem key={index} onClick={button.action}>
                                            {button.label}
                                        </DropdownItem>
                                    ))
                                }
                            </DropdownMenu>
                        </Dropdown>
                    )
                }
            </div>
        );
    }
}


headButtons.propTypes = {
    buttons: PropTypes.array
};

headButtons.defaultProps = {
    buttons: []
};

export default headButtons;
