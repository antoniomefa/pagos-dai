import React from 'react';
import PropTypes from 'prop-types';
import { ACTIONS } from 'utils/actions';

const permissionsKeys = Object.keys(ACTIONS);

const Button = ({
    text,
    type,
    outline,
    permission,
    permissions,
    onButtonPress,
    isIcon,
    icon,
    needPermission,
    extraStyle,
    extraStyleParent
}) => (
    <div style={extraStyleParent}>
        {
            needPermission ? (
                !isIcon ? (
                    permissions.some(p => p.key == permission) && (
                        <button
                            className={`btn btn-${outline ? 'outline-' : ''}${type}`}
                            type='button'
                            style={[{ margin: 2 }, extraStyle]}
                            onClick={onButtonPress}
                        >
                            {text}
                        </button>
                    )
                ) : (
                    permissions.some(p => p.key == permission) && (
                        <button
                            className={`btn btn-${outline ? 'outline-' : ''}${type}`}
                            type='button'
                            style={[{ margin: 2 }, extraStyle]}
                            onClick={onButtonPress}
                        >
                            <span className={`fa fa-${icon}`} />
                        </button>
                    )
                )
            ) : (
                !isIcon ? (
                    <button
                        className={`btn btn-${outline ? 'outline-' : ''}${type}`}
                        type='button'
                        style={[{ margin: 2 }, extraStyle]}
                        onClick={onButtonPress}
                    >
                        {text}
                    </button>
                ) : (
                    <button
                        className={`btn btn-${outline ? 'outline-' : ''}${type}`}
                        type='button'
                        style={[{ margin: 2 }, extraStyle]}
                        onClick={onButtonPress}
                    >
                        <span className={`fa fa-${icon}`} />
                    </button>
                )
            )
        }
    </div>
);

Button.propTypes = {
    type: PropTypes.oneOf([
        'primary',
        'secondary',
        'success',
        'danger',
        'warning',
        'info',
        'light',
        'dark'
    ]),
    outline: PropTypes.bool,
    text: PropTypes.string.isRequired,
    onButtonPress: PropTypes.func.isRequired,
    permission: PropTypes.oneOf(permissionsKeys),
    permissions: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired
    })),
    isIcon: PropTypes.bool,
    extraStyle: PropTypes.object,
    needPermission: PropTypes.bool,
    extraStyleParent: PropTypes.object,
    icon: PropTypes.string
};

Button.defaultProps = {
    type: 'primary',
    outline: false,
    needPermission: true,
    extraStyle: {},
    extraStyleParent: {},
    text: 'button',
    isIcon: false,
    permissions: []
};

export default Button;
