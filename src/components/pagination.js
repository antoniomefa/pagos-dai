import React, { Component} from 'react';
import PropTypes from 'prop-types';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

const FIRST = 1;
const TWO = 2;
const ZERO = 0;
const TEN = 10;
const MAX_PAGES_PAGINATION = 5;

class PaginationComponent extends Component {
    constructor(props) {
        super(props);
    }

    isOnFirstOrLastPositions = () => {
        const { total, offset } = this.props;
        const TOTAL_PAGES = Math.ceil(total / TEN);
        const isTooLarge = TOTAL_PAGES > MAX_PAGES_PAGINATION;

        if (!isTooLarge) return false;

        const curr = offset + FIRST;
        return curr > ZERO && curr <= TWO && curr <= TOTAL_PAGES && curr >= TOTAL_PAGES - FIRST;
    }

    render() {
        const { total, offset, paginatedChange } = this.props;
        const TOTAL_PAGES = Math.ceil(total / TEN);
        const isTooLarge = TOTAL_PAGES > MAX_PAGES_PAGINATION;
        var numberOfPages = TOTAL_PAGES;
        var i;

        if (isTooLarge) {
            numberOfPages = MAX_PAGES_PAGINATION;
        }

        const pages = Array(numberOfPages);

        if (pages.length < TWO) return null;

        const firstNumber = offset - TWO;
        for (i = firstNumber; i <= offset + TWO; i++) {
            pages.push(i);
        }

        return (
            <Pagination aria-label="pagination">
                <PaginationItem disabled={offset == FIRST}>
                    <PaginationLink previous onClick={() => paginatedChange(offset - FIRST)} />
                </PaginationItem>
                {
                    pages.map((page, index) => {
                        if (page <= ZERO || page > TOTAL_PAGES) return null;
                        return (
                            <div key={index} style={{ display: 'flex', flexDirection: 'row' }}>
                                <PaginationItem active={offset == page}>
                                    <PaginationLink
                                        onClick={
                                            () => paginatedChange(page)
                                        }
                                    >
                                        {page}
                                    </PaginationLink>
                                </PaginationItem>
                            </div>
                        );
                    })
                }
                <PaginationItem disabled={offset == TOTAL_PAGES}>
                    <PaginationLink next onClick={() => paginatedChange(offset + FIRST)} />
                </PaginationItem>
            </Pagination>
        );
    }
}

PaginationComponent.propTypes = {
    offset: PropTypes.number,
    total: PropTypes.number,
    paginatedChange: PropTypes.func
};

export default PaginationComponent;
