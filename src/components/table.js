import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';
import Pagination from 'components/pagination';
import Filters from 'components/filters';
import Actions from 'components/headButtons';
import MultiActions from 'components/multiActions';
import { connect } from 'react-redux';
import { getData } from 'utils';
import moment from 'moment';

const ZERO = 0;

class TableComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: []
        };
    }

    __selectAllFromPage = e => {
        const value = e.target.value;
        var selected = this.state.selected;
        const NOT_FOUND = -1;
        const ONE = 1;

        this.props.data.forEach((item) => {
            if (value == 'false') {
                if (selected.indexOf(item.id) == NOT_FOUND) {
                    selected.push(item.id);
                }
            } else {
                const index = selected.findIndex(s => s == item.id);

                if (index > NOT_FOUND) {
                    selected = [
                        ...selected.slice(ZERO, index),
                        ...selected.slice(index + ONE)
                    ];
                }
            }
        });

        this.setState({ selected });
    }

    __selectOne = (id, e) => {
        const selected = e.target.value;

        if (selected == 'false') {
            this.addAt(id);
        } else {
            this.removeAt(id);
        }
    }

    removeAt = id => {
        const NOT_FOUND = -1;
        const ZERO = 0;
        const ONE = 1;
        const index = this.state.selected.findIndex(s => s == id);

        if (index == NOT_FOUND) return;

        const selected = [
            ...this.state.selected.slice(ZERO, index),
            ...this.state.selected.slice(index + ONE)
        ];

        this.setState({ selected });
    }

    addAt = id => {
        const NOT_FOUND = -1;
        const index = this.state.selected.findIndex(s => s == id);

        if (index != NOT_FOUND) return;

        const selected = [
            ...this.state.selected,
            ...[id]
        ];

        this.setState({ selected });
    }

    thereAreSelectedAll = () => {
        const NOT_FOUND = -1;

        return !this.props.data.some(d => this.state.selected.indexOf(d.id) == NOT_FOUND);
    }

    __filterActions = data => {
        return data.filter(action =>
            this.props.wrapper.permissions[this.props.wrapper.module] &&
            this.props.wrapper.permissions[this.props.wrapper.module].permisos.some(p =>
                p.key == action.permission)
        );
    }

    onSelectMultiAction = data => {
        data.action(this.state.selected);
        this.setState({ selected: [] });
    }

    render() {
        const {
            columns,
            data,
            offset,
            filters,
            filtersHandler,
            total,
            isMultiselect,
            buttons,
            massiveActions,
            singleActions,
            paginatedChange,
            query,
            auth
        } = this.props;
        const ZERO = 0;
        const { selected } = this.state;
        const NOT_FOUND = -1;
        const sActions = singleActions;
        const multiselect = isMultiselect && this.__filterActions(massiveActions).length > ZERO;

        return (
            <div>
                <Filters
                    filters={filters}
                    filtered={query}
                    applyFilters={filtersHandler}
                    actions={<Actions buttons={this.__filterActions(buttons)} />}
                    multiActions={
                        <MultiActions
                        selectAction={this.onSelectMultiAction}
                        actions={this.__filterActions(massiveActions)}
                        />
                    }
                    selected={selected.length}
                />
                <Table striped responsive hover>
                    <thead>
                        <tr>
                            {
                                multiselect && (
                                    <th>
                                        <input
                                            type="checkbox"
                                            value={this.thereAreSelectedAll()}
                                            checked={this.thereAreSelectedAll()}
                                            onChange={this.__selectAllFromPage}
                                        />
                                    </th>
                                )
                            }
                            {
                                columns.map((column, index) => (
                                    <th key={index}>{column.name}</th>
                                ))
                            }
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.map((item, index) => (
                                <tr key={index}>
                                    {
                                        multiselect && (
                                            <th>
                                                <input
                                                    type="checkbox"
                                                    value={selected.indexOf(item.id) > NOT_FOUND}
                                                    checked={selected.indexOf(item.id) > NOT_FOUND}
                                                    onChange={this.__selectOne.bind(this, item.id)}
                                                />
                                            </th>
                                        )
                                    }
                                    {
                                        columns.map((column, indx) => (
                                            <td key={`data-${indx}`}>
                                                {
                                                    column.isDate && getData(item, column.field) ? (
                                                        moment(getData(item, column.field))
                                                            .format(
                                                                column.format ||
                                                                'DD [de] MMMM [de] YYYY'
                                                            )
                                                    ) : (
                                                        getData(item, column.field)
                                                    )
                                                }

                                            </td>
                                        ))
                                    }
                                    <td
                                        className="pull-right"
                                        style={{ display: 'flex', justifyContent: 'center' }}
                                    >
                                        {
                                            this.__filterActions(sActions).map((action, index) => (
                                                !action.validation || auth.user.id !== item.id ? (
                                                    <button
                                                        key={index}
                                                        title={action.tooltip}
                                                        onClick={() => action.action(item.id)}
                                                        className={action.style}
                                                        style={{ margin: 5 }}
                                                    >
                                                        <span className={`fa fa-${action.icon}`} />
                                                    </button>
                                                ) : null
                                            ))
                                        }
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
                {
                    data.length === ZERO && (
                        <h5 className="text-center">No hay información a mostrar</h5>
                    )
                }
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Pagination total={total} offset={offset} paginatedChange={paginatedChange} />
                </div>
            </div>
        );
    }
}

TableComponent.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    filters: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    buttons: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string,
        label: PropTypes.string,
        action: PropTypes.func,
        permission: PropTypes.string.isRequired
    })),
    massiveActions: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string,
        label: PropTypes.string,
        action: PropTypes.func,
        permission: PropTypes.string.isRequired
    })),
    singleActions: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string,
        label: PropTypes.string,
        action: PropTypes.func,
        permission: PropTypes.string.isRequired
    })),
    data: PropTypes.array.isRequired,
    offset: PropTypes.number,
    total: PropTypes.number,
    filtersHandler: PropTypes.func,
    isMultiselect: PropTypes.bool,
    wrapper: PropTypes.object,
    auth: PropTypes.object,
    query: PropTypes.object,
    paginatedChange: PropTypes.func
};

TableComponent.defaultProps = {
    columns: [],
    data: [],
    filters: [],
    buttons: [],
    massiveActions: [],
    singleActions: []
};

const mapStateToProps = ({ wrapper, auth }) => ({
    wrapper,
    auth
});

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(TableComponent);
