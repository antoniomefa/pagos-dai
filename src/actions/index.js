import {
    ON_SEND_NOTIFICATION,
    ON_SHOW_ALERT
} from 'action-types';
import {
    checkNotification,
    defaultNotification,
    defaultAlert,
    checkAlert,
    checkNotice,
    defaultNotice
} from 'utils';
import { success, warning } from 'react-notification-system-redux';

export function showNotice(opts) {
    return dispatch => {
        const data = checkNotice(opts);

        if (!data.success) {
            return dispatch(warning(defaultNotice(data.message)));
        }

        dispatch(success(opts));
    };
}

export function sendNotification(notification) {
    const data = checkNotification(notification);

    if (!data.success) {
        return {
            type: ON_SEND_NOTIFICATION,
            notification: defaultNotification(data.message)
        };
    }

    return {
        type: ON_SEND_NOTIFICATION,
        notification
    };
}

export function showAlert(alert) {
    const data = checkAlert(alert);

    if (!data.success) {
        return {
            type: ON_SHOW_ALERT,
            alert: defaultAlert(data.message)
        };
    }

    return {
        type: ON_SHOW_ALERT,
        alert
    };
}
