import { push } from 'react-router-redux';

export function goSignIn() {
    return dispatch => {
        dispatch(push('/sign-in'));
    };
}
