import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import RouterContainer from 'containers/router';
import store from 'utils/store';
import './index.css';
import './sidebar.css';
import './richEditor.css';
import './login.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-select/dist/react-select.css';
import 'malihu-custom-scrollbar-plugin';

ReactDOM.render(
    <Provider store={store}>
        <RouterContainer />
    </Provider>,
    document.getElementById('app')
);
